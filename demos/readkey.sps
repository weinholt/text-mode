#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (text-mode console)
  (text-mode console events))

(println "Text-mode interactive read-key test")
(println "Press keys. Quit with Alt-q.")
(cursor-off)

(with-window 3 3 45 15
  (lambda ()
    (text-color White)
    (text-background Blue)
    (clrscr)
    (redraw-screen)
    (let lp ()
      (let ((ev (read-event)))
        (cond
          ((key-press-event? ev)
           (println (list 'press (enum-set->list (keyboard-event-mods ev))
                          (keyboard-event-char ev) (keyboard-event-key ev)
                          (keyboard-event-location ev))))
          ((mouse-event? ev)
           (println (list (cond ((click-event? ev) 'click)
                                ((mouse-down-event? ev) 'mouse-down)
                                ((mouse-up-event? ev) 'mouse-up)
                                ((mouse-move-event? ev) 'mouse-move)
                                ((wheel-event? ev) 'wheel-event))
                          'm (enum-set->list (mouse-event-mods ev))
                          'b (mouse-event-buttons ev)
                          'x (mouse-event-x ev) 'y (mouse-event-y ev)
                          (and (wheel-event? ev) (wheel-event-delta-y ev)))))
          ((and (unknown-event? ev) (bytevector? (unknown-event-data ev)))
           (println (utf8->string (unknown-event-data ev))))
          (else
           (println ev)))
        (update-screen)
        (cond
          ((input-closed-event? ev)
           #f)
          ((and (key-press-event? ev)
                (enum-set=? (keyboard-event-mods ev) (modifier-set alt))
                (eqv? (keyboard-event-key ev) #\q))
           #f)
          ((and (key-press-event? ev)
                (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                (eqv? (keyboard-event-key ev) #\l))
           (resize-screen)
           (lp))
          (else
           (lp)))))))

(cursor-on)
(gotoxy 0 16)
(update-screen)
(restore-console)
