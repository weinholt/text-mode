#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (text-mode console)
  (text-mode console events)
  (text-mode termios))

(cursor-off)
(clrscr)

(define COLORS (vector Default Black Blue Green Cyan Red Magenta Brown Gray
                       DarkGray LightBlue LightGreen LightCyan LightRed LightMagenta
                       Yellow White Orange))

(define (draw-current-colors)
  (gotoxy (- (window-maxx) 2) 0)
  (print "XX"))

(define (draw-ui)
  (gotoxy 0 0)
  (clreol)
  (vector-for-each
   (lambda (color)
     (text-color color)
     (text-background color)
     (print "##"))
   COLORS)
  (text-color Default)
  (text-background Default)
  (print (list (window-maxx) (window-maxy)))
  (draw-current-colors))

(text-color Default)
(text-background Default)
(draw-ui)
(redraw-screen)
(let lp ()
  (update-screen)
  (let ((ev (read-event)))
    (unless (or (input-closed-event? ev)
                (and (key-press-event? ev) (eqv? (keyboard-event-key ev) #\q)))
      (cond
        ((resize-event? ev)
         (draw-ui))
        ((or (mouse-down-event? ev) (mouse-move-event? ev))
         (cond ((and (= (mouse-event-y ev) 0)
                     (< (div (mouse-event-x ev) 2) (vector-length COLORS)))
                (let ((color (vector-ref COLORS (div (mouse-event-x ev) 2))))
                  (cond ((= (mouse-event-buttons ev) #b1)
                         (text-color color))
                        ((= (mouse-event-buttons ev) #b100)
                         (text-background color)))
                  (draw-current-colors)))
               (else
                (gotoxy (mouse-event-x ev) (mouse-event-y ev))
                (print "#"))))
        #;
        ((wheel-event? ev)
         (if (negative? (wheel-event-delta-y ev))
             (text-color LightRed)
             (text-color White)))
        ((key-press-event? ev)
         (cond ((and (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                     (eqv? (keyboard-event-key ev) #\l))
                (resize-screen))
               ((eqv? (keyboard-event-key ev) #\c)
                (clrscr)
                (draw-ui)))))
      (lp))))

(restore-console)
