# Terminfo API

## (text-mode terminfo)

This library provides an interface to the terminfo database, which
contains descriptions of terminals. It can be used to generate escape
sequences to control terminals and recognize escape sequences sent
from terminals.

This is a low-level library that does not provide an abstraction for
consoles. If you just wish to use a terminal in full-screen mode then
have a look at `(text-mode console)` instead.

### (make-termstate *in* *out* [*baudrate*])

Create a termstate object, representing the state of a terminal with
the binary input port *in* and the binary output port *out*.
Optionally takes the *baudrate* of the serial line (default: 38400).
The baud rate is currently unused, but is meant to be used when
generating padding bytes for old terminals.

### (termstate? *obj*)

True if *obj* is a termstate.

### (termstate-in *ts*)

The binary input port of *ts*, which is used to read e.g. key presses.

### (termstate-in-set! *ts* *binary-input-port*)

Change the input port of *ts* to *binary-input-port*.

### (termstate-out *ts*)

The binary input port of *ts*, which is used to e.g. update the screen.

### (termstate-out-set! *ts* *binary-output-port*)

Change the output port of *ts* to *binary-output-port*.

### (termstate-baudrate *ts*)

The current baud rate of *ts*.

### (termstate-baudrate-set! *ts* *n*)

Change the baud rate of *ts* to *n*. This does not have any effect the
serial line.

### (termstate-dynamic-vars *ts*)

This is a hashtable of "dynamic" variables used in the execution of
terminfo procedures.

### (termstate-static-vars *ts*)

This is a hashtable of "static" variables used in the execution of
terminfo procedures.

### (termstate-ti *ts*)

The terminfo database of *ts*. This is an in-memory representation of
the terminfo database entry for the terminal. It is used by
`tigetnum`, `tigetflag`, `termstate-translate-input`, etc.

### (termstate-ti-set! *ts* *db*)

Change the terminfo database of *ts* to *db*. The terminfo database on
the file system is loaded late, which means that if the program calls
this procedure early then no file system lookup is done.

### (termstate-translate-input *ts* *bytevector*)

Appends *bytevector* to the pending input of *ts* and returns an
interpretation of the pending input.

This procedure returns three values: *partial?*, *key* and *code*. If
*partial?* is true then a partial match to a key was found and the
returned key might match a longer key if more data arrives soon. The
*code* value is a bytevector containing the raw bytes of the key
sequence.

The *key* value is one of these:

- An event object from `(text-mode console events)` for non-key
  events.
- A (tagged) character object. Characters may be tagged with the name
  of a modifier, `ctrl` or `alt`, e.g.: `(ctrl . #\g)`.
- A symbol describing the translated key, e.g. `KEY_SHOME`, matching
  the `Key:` lines from `bin/dump-terminfo.sps`.

### (get-terminfo-db [*str*])

Retrieve the terminfo database of the terminal named *str*. If *str*
is `#f` or omitted then the environment variable `TERM` is used.

If it cannot find the database in the file system then it either uses
the built-in entry for `dumb` if *term* is `dumb`, the entry for
`ansi` if *term* is `ansi`, not supplied or is empty, and otherwise
fails with an error.

### (terminfo-db->sexpr *db*)

Convert the terminfo *db* from its record form to an S-expression.

### (sexpr->terminfo-db *alist*)

Convert the terminfo db *alist* from its S-expression form to a
record.

### (tigetnum *cap* *ts*)

Retrieve the numeric [capability][ti5] *cap* from *ts*. If *cap* is
predefined then it must be provided as one of the constants exported
from `(text-mode terminfo constants)`. If it is an extension then it
must be provided as a symbol (e.g. `RGB` or `AX`).

Raises an error if *cap* has the wrong type. Returns -1 if the
capability is not defined.

### (tigetflag *cap* *ts*)

Retrieve the flag [capability][ti5] *cap* from *ts*. See above.

Raises an error if *cap* has the wrong type. Returns `#f` if the
capability is not defined.

### (tigetstr *cap* *ts*)

Retrieve the string [capability][ti5] *cap* from *ts*. See above.

Raises an error if *cap* has the wrong type. Returns `#f` if the
capability is not defined.

### (tigetproc *cap* *ts*)

Retrieve the string [capability][ti5] *cap* from *ts* as a procedure.
See above. The procedure is created by `terminfo-procedure`.

Example:

```scheme
(import (text-mode terminfo) (text-mode terminfo constants))
(define ts (make-termstate (standard-input-port) (standard-output-port)))
((tigetproc set_a_foreground ts) 2)
(put-bytevector (termstate-out ts) (string->utf8 "Hello, world!\n"))
((tigetproc exit_attribute_mode ts) 2)
(flush-output-port (termstate-out ts))
```

Raises an error if *cap* has the wrong type. Returns a dummy procedure
if the capability is not defined.

### (ticall *cap* *ts* [*p1* … *p9*])

Retrieve the string [capability][ti5] *cap* from *ts* and immediately
call it with arguments *p1* … *p9*. This is a shorthand for `tigetproc`.

### (terminfo-procedure *str* *ts*)

Compile the terminfo string *str* to a Scheme procedure sending its
output to *ts*. The procedure takes the arguments p1 through p9, all
of which are optional. Most commonly they are numbers, but some
procedures take strings. See the description of the capability either
in `terminfo/constants.sls` or in [*terminfo*(5)][ti5]. Changes to
*ts* that affect the output port or baud rate will affect future
invocations of the procedure.

 [ti5]: https://invisible-island.net/ncurses/man/terminfo.5.html#h3-Predefined-Capabilities

### (make-terminfo-backend state)

Creates a backend for use with the `(text-mode console)` library.

## (text-mode terminfo constants)

This library contains constants for the binary terminfo database
format. It has been automatically extracted from `Caps` in the ncurses
distribution and carries the license of ncurses (MIT + advertising
clause).

Four vectors are provided: `terminfo-booleans`, `terminfo-numbers`,
`terminfo-strings`, `terminfo-keys`. These are described elsewhere in
this document.

The binary format of terminfo database entries contains pre-defined
entries for booleans, numbers and strings and separately contains
extended capabilities. The pre-defined capabilities are implicitly
encoded by their position in the file while the extended capabilities
are encoded along with their names. The vectors above contain the
names of all pre-defined capabilities.

Most of this library is exports for the pre-defined entries, such as
`cursor_address`. These exported variables should be used as argument
to `tigetstr` etc (see their documentation).

## (text-mode terminfo parser)

This library contains a model and parser for terminfo database entries.

### (make-terminfo-db *str* *vec1* *vec2* *vec3* *ht1* *ht2* *ht3*)

Create a new terminfo database record with terminal names *str*,
booleans *vec1*, numbers *vec2*, strings *vec3*, extended booleans
*ht1*, extended numbers *ht2* and extended strings *ht3*. The *ht*
arguments are eq-hashtables with capability name symbols as keys.

### (terminfo-db? obj)

True if the object is a terminfo database.

### (terminfo-db-terminal-names *db*)

The names of the supported terminals of *db*, as a string. Multiple
terminals can be specified and separated with a pipe character.

### (terminfo-db-booleans *db*)

A vector of booleans for the flags in the terminfo database *db*.
Indices match `terminfo-booleans` in `(text-mode terminfo constants)`.

### (terminfo-db-numbers *db*)

A vector of numbers for the terminfo database *db*.
Indices match `terminfo-numbers` in `(text-mode terminfo constants)`.

### (terminfo-db-strings *db*)

A vector of strings for the terminfo database *db*.
Indices match `terminfo-strings` and `terminfo-keys` in
`(text-mode terminfo constants)`.

### (terminfo-db-ext-booleans *db*)

A hashtable of extended flags for the terminfo database *db*. These
are the flags which do not have a name in `terminfo-booleans`.

### (terminfo-db-ext-numbers *db*)

A hashtable of extended numbers for the terminfo database *db*. These
are the numbers which do not have a name in `terminfo-numbers`.

### (terminfo-db-ext-strings *db*)

A hashtable of extended strings for the terminfo database *db*. These
are the strings which do not have a name in `terminfo-strings`.

### (get-compiled-ncurses-terminfo *p*)

Read a binary compiled terminfo database in ncurses format from the
binary input port *p*. Supports the new 32-bit format.

Raises an error if the port does not start with the right magic bytes.

### (parse-term-string *bv*)

Terminfo string tokenizer. This turns the bytevector *bv* into a list
of tokens.

More about the language can be found in [Terminfo and its DSL][dsl]
and [*terminfo*(5)][str].

  [dsl]: https://weinholt.se/articles/terminfo-and-its-dsl/
  [str]: https://invisible-island.net/ncurses/man/terminfo.5.html#h3-Parameterized-Strings

### (terminfo-expression *bv*)

Terminfo string compiler. This turns the bytevector *bv* into a Scheme
expression. The expression should be evaluated in an environment with
the libraries `(rnrs)`, `(rnrs r5rs)` and `(text-mode terminfo builtins)`.
The code is designed to be optimized by cp0 and will not be very
efficient otherwise.

### (terminfo-raw-procedure *bv*)

Terminfo string compiler. This is like `terminfo-expression` but it
also evaluates the expression and returns the procedure. Please do not
rely on the stability of the API. Use `terminfo-procedure` instead.
