# Console API

The console API represents a computer console, such as a terminal
emulator like xterm or urxvt, or another character cell-based display.
Examples are available in the `demos` directory.

## (text-mode console)

### (console? *obj*)

True if *obj* is a console record.

### (make-console *backend*)

Create a new console record that interfaces with a console through
*backend*. The backend interface is currently undocumented. There is
one implementation in the terminfo library, `make-terminfo-backend`.

### (current-console)

The current console, which on library start-up is initialized to
`(default-console)` in `(text-mode platform)`. Any procedures which
take a console object default to the current console, much like the
normal I/O procedures in Scheme default to the current input and
output ports.

### (restore-console [*console*])

Restore *console* to the state it had before the console library
started messing with it. Should be called before exiting the program.

### (update-screen [*console*])

Update the screen of *console* to match the internal model.

### (redraw-screen [*console*])

Update the screen of *console* to match the internal model, not
assuming anything about its current contents.

### (resize-screen [*console*])

Probe for the size and capabilities of *console*, resizing if the size
has changed. Can be called as part of a user command that redraws the
screen.

### (set-window *x1* *y1* *x2* *y2* [*console*]

Change the window of *console* to the rectangle (*x1*, *y1*) - (*x2*,
*y2*). When a window is in effect, all coordinates are relative to the
window and the screen area outside of the window cannot be changed.
The window affects all operations except window operations.

The window may not be larger than the full dimensions of the console
(although that restriction may be lifted later, since there are some
strange interactions with screen resizing). The window must contain
at least one character cell.

### (reset-window [*console*])

Reset the window of *console* to match the full size of the screen,
which is the default.

### (with-window *x1* *y1* *x2* *y2* *thunk* [*console*])

Change the window of *console* to the rectangle (*x1*, *y1*) - (*x2*,
*y2*) during the dynamic extent of *thunk*, which is a procedure
taking no arguments. See `set-window`.

### (window-minx [*console*])

The minimum x coordinate of *console*, corresponding to *x1* of the
window.

### (window-miny [*console*])

The minimum y coordinate of *console*, corresponding to *y1* of the
window.

### (window-maxx [*console*])

The maximum x coordinate of *console*, corresponding to *x2* of the
window.

### (window-maxy [*console*])

The maximum y coordinate of *console*, corresponding to *y2* of the
window.

### (window-width [*console*])

The width of *console* measured in character cells, according to the
current window.

### (window-height [*console*])

The height of *console* measured in character cells, according to the
current window.

### (gotoxy *x* *y* [*console*])

Move the cursor of *console* to coordinates (*x*, *y*), where (0, 0)
is the top-left cell of the window.

### (wherex [*console*])

The current column of *console*, where new output will be written.
Relative to the top-left cell of the window.

### (wherey [*console*])

The current row of *console*, where new output will be written.
Relative to the top-left cell of the window.

### (cursor-on [*console*])

Unhide the cursor on the screen of *console*. Does not take effect
until the screen is updated.

### (cursor-off [*console*])

Hide the cursor on the screen of *console*. Does not take effect until
the screen is updated.

### (clreol [*console*])

Clear the current line of *console*, from the current *x* coordinate
to the end of the line, using the current window, colors and attributes.

### (clrscr [*console*])

Clear the screen of *console* using the current window, colors and
attributes.

### (print *obj* [*console*])

Print *obj* on the screen of *console* at the current coordinates
using the current colors and attributes. If *obj* is not a string then
it is printed as if printed by `display`. If the cursor reaches the
end of the line it will wrap around to the next line, scrolling the
window if necessary.

```scheme
(import (text-mode console))
(print "Hello, world!")
(update-screen)
(read-key)
(restore-console)
```

### (println [*console*])

Same as `print` followed by a newline.

```scheme
(import (text-mode console))
(println "Hello, ")
(print "world!")
(update-screen)
(read-key)
(restore-console)
```

### (read-event [*console*])

Read an event from *console*, blocking if no data is available.
See `(text-mode console events)` for definitions of events.

### (read-key [*console*])

Read a key from *console*, blocking if no data is available, and
ignoring any events other than key presses. Returns a character, if
the key can be represented as a character; ignoring other keys. If you
wish to handle all keys, even function keys, then use `read-event`.

### (text-color *color* [*console*])

Set the current foreground color of *console* to *color*, which should
be either a predefined named color or an object created by *rgb-color*
or *hsv-color*.

Most terminals in use have restrictions on colors. Often there are
only 16 or eight color pairs and they are often hardwired. This
library will try to make the best of the situation.

### (text-background *color* [*console*])

Set the current background color of *console* to *color*, which should
be either a predefined named color or an object created by *rgb-color*
or *hsv-color*.

Most terminals in use have restrictions on colors, which are often
even worse for background colors.

### (text-attribute *symbol* *obj* [*console*])

Set the current attribute value *symbol* of *console* to *obj*. Used
for text styles. The *symbol* should be one of: standout, underline,
reverse, blink, dim, bold, invisible. The value *obj* should be a
boolean. Not all consoles support all attributes.

### (rgb-color rgb)

An RGB color object that represents the 24-bit color value *rgb*.
Example: `(rgb-color #xFF0000)` returns red at maximum value.

### (rgb-color r g b [d])

A 24-bit RGB color object that represents red *r*, green *g* and blue
*b*. Example: `(rgb-color #xFF 0 0)` returns red at maximum value. The
arguments are byte values.

The optional *d* parameter is used to encode the default color.

### (hsv-color h s v)

A 24-bit RGB color object that represents the color given by hue *h*,
saturation *s* and value *v*. The ranges of the arguments are 0 ≤ *h*
≤ 360, 0 ≤ *s* ≤ 1, 0 ≤ *v* ≤ 1.

### Default

This represents the default color, which depends on the settings of
the terminal. Many terminals support a transparent background when the
background color is the default. The default foreground color is often
only useful to monochrome programs and tends to be some gray or green
color.

### Black Blue Green Cyan Red Magenta Brown Gray

Predefined colors that are almost always available as both foreground
and background colors. If the terminal supports 24-bit colors then the
standard VGA colors are used; otherwise a close match is used.

### DarkGray LightBlue LightGreen LightCyan LightRed LightMagenta Yellow White

Predefined colors that are almost always available as foreground
colors. If the terminal supports 24-bit colors then the standard VGA
colors are used; otherwise a close match is used.

### Orange

Orange is a good color. Almost never available unless the terminal
supports 24-bit colors.
