;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (text-mode console model)
  (export
    make-console console?
    console-backend console-backend-set!
    console-full-cols console-full-cols-set!
    console-full-rows console-full-rows-set!
    console-pixel-width console-pixel-width-set!
    console-pixel-height console-pixel-height-set!
    console-x console-x-set!
    console-y console-y-set!
    console-cursor-attr console-cursor-attr-set!
    console-fg console-fg-set!
    console-bg console-bg-set!
    console-attr console-attr-set!
    console-x1 console-x1-set!
    console-y1 console-y1-set!
    console-x2 console-x2-set!
    console-y2 console-y2-set!
    console-cols console-cols-set!
    console-rows console-rows-set!
    rgb-color
    hsv-color

    console-resize!
    set-console-cursor-attr!

    %index %index/nowin
    set-console-dirty!
    clear-console-dirty!
    console-row-dirty?

    textcell-unused?

    text-ref text-set! text-clear!
    fg-ref fg-set!
    bg-ref bg-set!
    attr-ref attr-set!
    mark-ref mark-set!

    color-d
    color-r color-g color-b
    color-rgb

    Default
    Black Blue Green Cyan Red Magenta Brown Gray
    DarkGray LightBlue LightGreen LightCyan LightRed LightMagenta
    Yellow White
    Orange)
  (import
    (rnrs (6))
    (rnrs mutable-pairs (6))
    (rnrs mutable-strings (6)))

(define rgb-color
  (case-lambda
    ((rgb)
     (bitwise-and rgb #xffffff))
    ((r g b)
     (rgb-color r g b 0))
    ((r g b d)
     ;; "DRGB", with 1-bit default-flag and 8-bit RGB
     (bitwise-ior (bitwise-arithmetic-shift-left (fxand d 1) 24)
                  (bitwise-arithmetic-shift-left r 16)
                  (fxarithmetic-shift-left g 8)
                  b))))

(define (hsv-color H S V)
  (assert (<= 0 H 360))
  (assert (<= 0 S 1))
  (assert (<= 0 V 1))
  (let* ((C (* V S))
         (X (* C (- 1 (abs (- (mod (/ H 60) 2) 1)))))
         (m (- V C)))
    (let-values (((R* G* B*)
                  (cond ((<= H 60) (values C X 0))
                        ((<= H 120) (values X C 0))
                        ((<= H 180) (values 0 C X))
                        ((<= H 240) (values 0 X C))
                        ((<= H 300) (values X 0 C))
                        (else (values C 0 X)))))
      (rgb-color (exact (round (* 255 (+ R* m))))
                 (exact (round (* 255 (+ G* m))))
                 (exact (round (* 255 (+ B* m))))))))

;; VGA colors
(define Black        (rgb-color #x000000))
(define Blue         (rgb-color #x0000aa))
(define Green        (rgb-color #x00aa00))
(define Cyan         (rgb-color #x00aaaa))
(define Red          (rgb-color #xaa0000))
(define Magenta      (rgb-color #xaa00aa))
(define Brown        (rgb-color #xaa5500))
(define Gray         (rgb-color #xaaaaaa))
(define DarkGray     (rgb-color #x555555))
(define LightBlue    (rgb-color #x5555ff))
(define LightGreen   (rgb-color #x55ff55))
(define LightCyan    (rgb-color #x55ffff))
(define LightRed     (rgb-color #xff5555))
(define LightMagenta (rgb-color #xff55ff))
(define Yellow       (rgb-color #xffff55))
(define White        (rgb-color #xffffff))
(define Orange       (rgb-color #xff6400))
(define Default      (rgb-color 0 0 0 1))

;; The console keeps track of the current state only. Backends keep
;; track of the previous state if necessary.
(define-record-type console
  (sealed #t)
  (fields
   (mutable backend)
   ;; Size in characters
   (mutable full-cols) (mutable full-rows)
   ;; Size in pixels (not used)
   (mutable pixel-width) (mutable pixel-height)
   ;; Cursor (inside the window)
   (mutable x) (mutable y) (mutable cursor-attr)
   ;; Current color and attributes
   (mutable fg) (mutable bg) (mutable attr)
   ;; Window
   (mutable x1) (mutable y1) (mutable x2) (mutable y2)
   (mutable cols) (mutable rows)
   ;; Text to render (flat strings). This does not contain control
   ;; characters. The second buffer is for combining marks, etc.
   (mutable textbuf)
   (mutable markbuf)
   ;; Foreground colors (flat vector)
   (mutable fgbuf)
   ;; Background colors (flat vector)
   (mutable bgbuf)
   ;; Attributes (flat vector of alists)
   (mutable attrbuf)
   ;; Mark rows as dirty
   (mutable dirty-marks))
  (protocol
   (lambda (p)
     (lambda (backend)
       (let-values ([(cols rows width height) (backend 'get-size #f)])
         (let* ((colorbuf (make-bytevector (* cols rows 2 4)))
                (c (p backend
                      cols rows
                      ;; TODO: assumes 8x13 cells and square pixels
                      (if (zero? width) (* cols 8) width)
                      (if (zero? height) (* rows 13) height)
                      0 0 (list (cons 'visible #t))
                      ;; Colors
                      Gray Default '()
                      ;; Window (inclusive lower, exclusive upper)
                      0 0 cols rows
                      cols rows               ;window size
                      ;; Text
                      (make-string (* cols rows) #\space)
                      (make-vector (* cols rows) "")
                      ;; Colors
                      (make-vector (* cols rows) Default)
                      (make-vector (* cols rows) Default)
                      ;; Attributes
                      (make-vector (* cols rows) '())
                      ;; Dirty marks
                      (make-bytevector rows 0))))
           c))))))

(define (fxclamp low v high)
  (fxmin (fxmax low v) high))

(define console-resize!
  (case-lambda
    ((c cols rows pixel-width pixel-height)
     (console-pixel-width-set! c (if (fxpositive? pixel-width) pixel-width (* cols 8)))
     (console-pixel-height-set! c (if (fxpositive? pixel-height) pixel-height (* rows 13)))
     (unless (and (fx=? cols (console-full-cols c))
                  (fx=? rows (console-full-rows c)))
       (when (and (fx<=? 1 cols 32768) (fx<=? 1 rows 32767))
         (let ((old-width (console-full-cols c))
               (old-height (console-full-rows c)))
           (console-full-cols-set! c cols)
           (console-full-rows-set! c rows)
           ;; Constrain the cursor
           (console-x-set! c (fxclamp 0 (console-x c) (fx- cols 1)))
           (console-y-set! c (fxclamp 0 (console-y c) (fx- rows 1)))
           (cond
             ((and (fx=? (console-cols c) old-width) (fx=? (console-rows c) old-height))
              ;; No window
              (console-x1-set! c 0)
              (console-y1-set! c 0)
              (console-x2-set! c cols)
              (console-y2-set! c rows)
              (console-cols-set! c cols)
              (console-rows-set! c rows))
             (else
              ;; Shrink the window
              (console-x1-set! c (fxclamp 0 (console-x1 c) cols))
              (console-y1-set! c (fxclamp 0 (console-y1 c) rows))
              (console-x2-set! c (fxclamp 0 (console-x2 c) cols))
              (console-y2-set! c (fxclamp 0 (console-y2 c) rows))
              (console-cols-set! c (fx- (console-x2 c) (console-x1 c)))
              (console-rows-set! c (fx- (console-y2 c) (console-y1 c)))))

           ;; Resize the buffers
           (let ((tb (make-string (* cols rows) #\space))
                 (mb (make-vector (* cols rows) ""))
                 (fb (make-vector (* cols rows) Default))
                 (bb (make-vector (* cols rows) Default))
                 (ab (make-vector (* cols rows) '()))
                 (otb (console-textbuf c))
                 (omb (console-markbuf c))
                 (ofb (console-fgbuf c))
                 (obb (console-bgbuf c))
                 (oab (console-attrbuf c)))
             (console-textbuf-set! c tb)
             (console-markbuf-set! c mb)
             (console-fgbuf-set! c fb)
             (console-bgbuf-set! c bb)
             (console-attrbuf-set! c ab)
             (console-dirty-marks-set! c (make-bytevector rows 1))

             ;; Keep the previous contents
             (do ((y 0 (fx+ y 1))) ((fx=? y rows))
               (do ((x 0 (fx+ x 1))) ((fx=? x cols))
                 (let ((new-idx (fx+ x (fx* y cols)))
                       (old-idx (fx+ x (fx* y old-width))))
                   (when (and (fx<? x old-width) (fx<? y old-height))
                     (text-set! tb new-idx (text-ref otb old-idx))
                     (mark-set! mb new-idx (mark-ref omb old-idx))
                     (fg-set! fb new-idx (fg-ref ofb old-idx))
                     (bg-set! bb new-idx (bg-ref obb old-idx))
                     (attr-set! ab new-idx (attr-ref oab old-idx)))))))))))))

;; An internal abstraction for addressing the buffers above. The
;; characters in a line inside the window are stored consecutively in
;; the same buffer this returns.
(define (%index/nowin c x y)
  (values (console-textbuf c)
          (console-markbuf c)
          (console-fgbuf c)
          (console-bgbuf c)
          (console-attrbuf c)
          (fx+ x (fx* y (console-full-cols c)))))

(define (%index c x y)
  (values (console-textbuf c)
          (console-markbuf c)
          (console-fgbuf c)
          (console-bgbuf c)
          (console-attrbuf c)
          (fx+ (fx+ x (console-x1 c))
               (fx* (fx+ y (console-y1 c))
                    (console-full-cols c)))
          ;; Cols available inside the window
          (fx- (console-cols c) x)))

(define set-console-dirty!
  (case-lambda
    ((c y)
     (bytevector-u8-set! (console-dirty-marks c) (fx+ y (console-y1 c)) 1))
    ((c)
     (bytevector-fill! (console-dirty-marks c) 1))))

(define (clear-console-dirty! c)
  (bytevector-fill! (console-dirty-marks c) 0))

(define (console-row-dirty? c y absolute?)
  (let ((row (if absolute? y (fx+ y (console-y1 c))))
        (marks (console-dirty-marks c)))
    (and (fx<? row (bytevector-length marks))
         (eqv? 1 (bytevector-u8-ref marks row)))))

(define (set-console-cursor-attr! c attr val)
  (cond ((assq attr (console-cursor-attr c))
         => (lambda (p) (set-cdr! p val)))))

(define TEXTCELL-UNUSED #\x10FFFD)
(define (textcell-unused? x)
  (eqv? x TEXTCELL-UNUSED))

(define text-ref string-ref)
(define text-set! string-set!)
(define mark-ref vector-ref)
(define mark-set! vector-set!)
(define fg-ref vector-ref)
(define fg-set! vector-set!)
(define bg-ref vector-ref)
(define bg-set! vector-set!)
(define attr-ref vector-ref)
(define attr-set! vector-set!)

(define (text-clear! buf idx)
  (text-set! buf idx TEXTCELL-UNUSED))

(define (color-d color) (fx=? (bitwise-bit-field color 24 25) 1))
(define (color-r color) (bitwise-bit-field color 16 24))
(define (color-g color) (bitwise-bit-field color 8 16))
(define (color-b color) (bitwise-bit-field color 0 8))
(define (color-rgb color) (bitwise-bit-field color 0 24)))
