;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; All manner of events for the application to handle

;; Loosely modelled on https://www.w3.org/TR/uievents/ but with
;; additions and simplifications for terminals.

(library (text-mode console events)
  (export
    modifier modifier-set

    make-event event?

    make-input-closed-event input-closed-event?

    make-resize-event resize-event? resize-event-cols resize-event-rows
    resize-event-width resize-event-height

    make-device-attributes-event device-attributes-event?
    device-attributes-event-architecture device-attributes-event-list

    make-cursor-report-event cursor-report-event?
    cursor-report-event-x cursor-report-event-y

    make-unknown-event unknown-event? unknown-event-source unknown-event-data

    make-keyboard-event keyboard-event?
    keyboard-event-mods keyboard-event-char keyboard-event-key
    keyboard-event-location
    keyboard-event-has-modifiers?

    make-key-press-event key-press-event?

    make-mouse-event mouse-event?
    mouse-event-mods mouse-event-buttons mouse-event-x mouse-event-y

    make-aux-click-event aux-click-event?
    make-click-event click-event?

    make-mouse-down-event mouse-down-event?
    make-mouse-up-event mouse-up-event?
    make-mouse-move-event mouse-move-event?

    make-wheel-event wheel-event?
    wheel-event-delta-x wheel-event-delta-y wheel-event-delta-z
    wheel-event-delta-mode)
  (import
    (rnrs (6)))

(define-enumeration modifier
  (ctrl                                 ;Ctrl key
   shift                                ;Shift key
   alt                                  ;Alt key
   meta                                 ;Logo or ◆/⌘ key

   alt-graph                            ;Alt Gr
   caps-lock                            ;Caps lock
   fn                                   ;Fn key
   fn-lock                              ;F-Lock key
   num-lock                             ;Num lock
   hyper                                ;Hyper key
   scroll-lock                          ;Scroll lock
   super                                ;Super key
   symbol                               ;Symbol key
   symbol-lock)                         ;SymbolLock key
  modifier-set)

(define no-mods (modifier-set))

(define-record-type event)

(define-record-type input-closed-event
  (parent event))

(define-record-type resize-event
  (parent event)
  (fields cols rows
          width height))                ;0 if not known

(define-record-type device-attributes-event
  (parent event)
  (fields architecture list))

(define-record-type cursor-report-event
  (parent event)
  (fields x y))

(define-record-type unknown-event
  (parent event)
  (fields source data))

;;; Keyboard

(define-record-type keyboard-event
  (parent event)
  (fields mods                          ;a modifier-set
          char                          ;character interpretation or #f
          key                           ;char or symbol
          location))                    ;one of: 'standard 'left 'right 'numpad

(define (keyboard-event-has-modifiers? e)
  (not (enum-set=? no-mods (keyboard-event-mods e))))

(define-record-type key-press-event
  (parent keyboard-event))

;;; Mouse

(define-record-type mouse-event
  (parent event)
  (fields mods                          ;a modifier-set
          buttons                       ;bitmask: 1=primary, 2=secondary, 4=middle
          x
          y))

(define-record-type aux-click-event
  (parent mouse-event))

(define-record-type click-event
  (parent mouse-event))

(define-record-type double-click-event
  (parent mouse-event))

(define-record-type mouse-down-event
  (parent mouse-event))

(define-record-type mouse-up-event
  (parent mouse-event))

(define-record-type mouse-move-event
  (parent mouse-event))

;;; Mouse scroll wheel

(define-record-type wheel-event
  (parent mouse-event)
  (fields delta-x
          delta-y
          delta-z
          delta-mode)))                 ;one of: 'pixel 'line 'page
