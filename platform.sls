;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Platform-specific code

(library (text-mode platform)
  (export
    default-console)
  (import
    (rnrs (6))
    (text-mode console model)
    (text-mode terminfo))

(define (default-console)
  (make-console
   (let ((out (standard-output-port))
         (in (standard-input-port)))
     (let ((state (make-termstate in out 38400)))
       (make-terminfo-backend state))))))
