;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Text-based console abstraction

;; Upper left corner is (0,0).

;; A good reference for 24-bit true color:
;; https://gist.github.com/XVilka/8346728

;; Character widths:
;; https://github.com/JuliaStrings/utf8proc/blob/master/data/charwidths.jl

;; Emojis (not implemented yet):
;; https://www.unicode.org/reports/tr51/

(library (text-mode console)
  (export
    console?
    make-console
    current-console
    update-screen
    redraw-screen
    resize-screen
    restore-console

    with-window
    set-window
    reset-window
    window-maxx window-maxy window-width
    window-minx window-miny window-height

    gotoxy wherex wherey
    cursor-on cursor-off

    clreol
    clrscr

    print
    println

    read-event
    read-key

    text-color
    text-background
    text-attribute

    rgb-color hsv-color
    Default
    Black
    Blue
    Green
    Cyan
    Red
    Magenta
    Brown
    Gray
    DarkGray
    LightBlue
    LightGreen
    LightCyan
    LightRed
    LightMagenta
    Yellow
    White
    Orange)
  (import
    (rnrs (6))
    (text-mode console events)
    (text-mode console model)
    (text-mode platform)
    (text-mode unicode))

(define (fxclamp low v high)
  (fxmin (fxmax low v) high))

;;; Drawing

(define text-background
  (case-lambda
    ((v) (text-background v (current-console)))
    ((v c)
     (assert (<= 0 v #xffffffff))
     (console-bg-set! c v))))

(define text-color
  (case-lambda
    ((v) (text-color v (current-console)))
    ((v c)
     (assert (<= 0 v #xffffffff))
     (console-fg-set! c v))))

(define text-attribute
  (case-lambda
    ((key value) (text-attribute key value (current-console)))
    ((key value c)
     (console-attr-set! c (cons (cons key value)
                                (let lp ((attr* (console-attr c)))
                                  (cond ((null? attr*)
                                         '())
                                        ((eq? (caar attr*) key)
                                         (lp (cdr attr*)))
                                        (else
                                         (cons (car attr*)
                                               (lp (cdr attr*)))))))))))

;; Update the physical screen with the current state. Returns #f if
;; interrupted by user input.
(define update-screen
  (case-lambda
    (() (update-screen (current-console)))
    ((c)
     ((console-backend c) 'update c))))

;; Redraw the physical screen, not assuming anything about its current
;; state. Returns #f if interrupted by user input.
(define redraw-screen
  (case-lambda
    (() (redraw-screen (current-console)))
    ((c)
     (set-console-dirty! c)
     ((console-backend c) 'redraw c))))

;; Probe for the size of the screen.
(define resize-screen
  (case-lambda
    (() (resize-screen (current-console)))
    ((c)
     (call-with-values (lambda ()
                         ((console-backend c) 'resize c))
                       (case-lambda
                         ((ev) (%handle-resize c ev))
                         (_ #f))))))

;; Sets the window, outside which the contents are immutable.
(define set-window
  (case-lambda
    ((x1 y1 x2 y2) (set-window x1 y1 x2 y2 (current-console)))
    ((x1 y1 x2 y2 c)
     (let ((x2 (fxclamp 1 x2 (console-full-cols c)))
           (y2 (fxclamp 1 y2 (console-full-rows c))))
       (let ((x1 (fxclamp 0 x1 (fx- x2 1)))
             (y1 (fxclamp 0 y1 (fx- y2 1))))
         (unless (fx<? -1 x1 x2 (fx+ 1 (console-full-cols c)))
           (assertion-violation 'set-window "X coordinates out of range"
                                x1 x2 'limit: (console-full-cols c)))
         (unless (fx<? -1 y1 y2 (fx+ 1 (console-full-rows c)))
           (assertion-violation 'set-window "X coordinates out of range"
                                y1 y2 'limit: (console-full-rows c)))
         (console-x1-set! c x1)
         (console-y1-set! c y1)
         (console-x2-set! c x2)
         (console-y2-set! c y2)
         (console-cols-set! c (fx- x2 x1))
         (console-rows-set! c (fx- y2 y1))
         (console-x-set! c 0)
         (console-y-set! c 0))))))

;; Reset the window, setting it to the whole console.
(define reset-window
  (case-lambda
    (() (reset-window (current-console)))
    ((c)
     (console-x-set! c 0)
     (console-y-set! c 0)
     (console-x1-set! c 0)
     (console-y1-set! c 0)
     (console-x2-set! c (console-full-cols c))
     (console-y2-set! c (console-full-rows c))
     (console-cols-set! c (console-full-cols c))
     (console-rows-set! c (console-full-rows c)))))

(define with-window
  (case-lambda
    ((x1 y1 x2 y2 thunk)
     (with-window x1 y1 x2 y2 thunk (current-console)))
    ((x1 y1 x2 y2 thunk c)
     (define old-x1)
     (define old-y1)
     (define old-x2)
     (define old-y2)
     (define old-x)
     (define old-y)
     (dynamic-wind
       (lambda ()
         (set! old-x1 (console-x1 c))
         (set! old-y1 (console-y1 c))
         (set! old-x2 (console-x2 c))
         (set! old-y2 (console-y2 c))
         (set! old-x (console-x c))
         (set! old-y (console-y c))
         (set-window x1 y1 x2 y2 c))
       thunk
       (lambda ()
         (set-window old-x1 old-y1 old-x2 old-y2 c)
         (gotoxy old-x old-y c))))))

(define window-maxx
  (case-lambda
    (() (window-maxx (current-console)))
    ((c) (console-x2 c))))

(define window-maxy
  (case-lambda
    (() (window-maxy (current-console)))
    ((c) (console-y2 c))))

(define window-minx
  (case-lambda
    (() (window-minx (current-console)))
    ((c) (console-x1 c))))

(define window-miny
  (case-lambda
    (() (window-miny (current-console)))
    ((c) (console-y1 c))))

(define window-width
  (case-lambda
    (() (window-width (current-console)))
    ((c) (fx- (window-maxx c) (window-minx c)))))

(define window-height
  (case-lambda
    (() (window-height (current-console)))
    ((c) (fx- (window-maxy c) (window-miny c)))))

(define gotoxy
  (case-lambda
    ((x y) (gotoxy x y (current-console)))
    ((x y c)
     (console-x-set! c (fxclamp 0 x (fx- (console-cols c) 1)))
     (console-y-set! c (fxclamp 0 y (fx- (console-rows c) 1))))))

(define wherex
  (case-lambda
    (() (wherex (current-console)))
    ((c) (console-x c))))

(define wherey
  (case-lambda
    (() (wherey (current-console)))
    ((c) (console-y c))))

(define cursor-on
  (case-lambda
    (() (cursor-on (current-console)))
    ((c)
     (set-console-cursor-attr! c 'visible #t))))

(define cursor-off
  (case-lambda
    (() (cursor-off (current-console)))
    ((c)
     (set-console-cursor-attr! c 'visible #f))))

;; Clear the current line without moving the cursor.
(define clreol
  (case-lambda
    (() (clreol (current-console)))
    ((c)
     (set-console-dirty! c (console-y c))
     (let-values ([(buf mbuf fbuf bbuf abuf idx remx) (%index c (console-x c) (console-y c))])
       (do ((i 0 (fx+ i 1))
            (idx idx (fx+ idx 1)))
           ((fx=? i remx))
         (text-set! buf idx #\space)
         (mark-set! mbuf idx "")
         (fg-set! fbuf idx (console-fg c))
         (bg-set! bbuf idx (console-bg c))
         (attr-set! abuf idx (cons '(empty . #t) (console-attr c))))))))

;; Clear the screen without moving the cursor.
(define clrscr
  (case-lambda
    (() (clrscr (current-console)))
    ((c)
     (set-console-dirty! c)
     (do ((y 0 (fx+ y 1))) ((fx=? y (console-rows c)))
       (let-values ([(buf mbuf fbuf bbuf abuf idx remx) (%index c 0 y)])
         (do ((i 0 (fx+ i 1))
              (idx idx (fx+ idx 1)))
             ((fx=? i remx))
           (text-set! buf idx #\space)
           (mark-set! mbuf idx "")
           (fg-set! fbuf idx (console-fg c))
           (bg-set! bbuf idx (console-bg c))
           (attr-set! abuf idx (console-attr c))))))))

(define (%next-line c)
  (console-x-set! c 0)
  (cond
    ((fx=? (console-y c) (fx- (console-rows c) 1))
     ;; Scroll the window
     (do ((y 0 (fx+ y 1))) ((fx=? y (fx- (console-rows c) 1)))
       (set-console-dirty! c y)
       (let-values ([(buf0 mbuf0 fbuf0 bbuf0 abuf0 idx0 remx0) (%index c 0 y)]
                    [(buf1 mbuf1 fbuf1 bbuf1 abuf1 idx1 remx1) (%index c 0 (fx+ y 1))])
         (assert (eqv? remx0 remx1))
         (do ((i 0 (fx+ i 1)))
             ((fx=? i remx0))
           (let ((i0 (fx+ idx0 i))
                 (i1 (fx+ idx1 i)))
             (text-set! buf0 i0 (text-ref buf1 i1))
             (mark-set! mbuf0 i0 (mark-ref mbuf1 i1))
             (fg-set! fbuf0 i0 (fg-ref fbuf1 i1))
             (bg-set! bbuf0 i0 (bg-ref bbuf1 i1))
             (attr-set! abuf0 i0 (attr-ref abuf1 i1))))))
     (clreol c))
    (else
     (console-y-set! c (fx+ (console-y c) 1)))))

;; Get the graph cluster starting at the index. It is this procedure's
;; job to get the base character and all following characters that
;; will appear in the same character cell as the base character
;; (mods). Returns: base character, modifiers, cursor advance length,
;; length of characters used from str.
(define (string-cluster-ref str i)
  (let* ((ch (string-ref str i))
         (modlen (let lp ((i (fx+ i 1)) (len 0))
                   (cond ((fx=? i (string-length str))
                          len)
                         ((eqv? 0 (char-width (string-ref str i)))
                          (if (eq? 'Cc (char-general-category (string-ref str i)))
                              len
                              (lp (fx+ i 1) (fx+ len 1))))
                         (else len))))
         (mods (if (eqv? modlen 0)
                   ""
                   (substring str (fx+ i 1) (fx+ (fx+ i 1) modlen))))
         (len (fx+ modlen 1)))
    (case (char-width ch)
      ((1)
       (values ch mods 1 len))
      ((2)
       (values ch mods 2 len))
      ((0)
       (if (eq? 'Cc (char-general-category ch))
           (values ch mods 1 len)
           ;; The first char in a cluster is zero-width... skip it.
           (values ch mods 0 len))))))

(define (grapheme-nfc ch mods)
  (cond ((string=? mods "")
         (values ch mods))
        (else
         (let ((str (string-normalize-nfc (string-append (string ch) mods))))
           (values (string-ref str 0)
                   (substring str 1 (string-length str)))))))

;; Print a string. Control characters are converted to printable
;; characters. Combining marks cannot be split between two calls to
;; this procedure. Handles line wrapping and scrolling.
(define print
  (case-lambda
    ((str) (print str (current-console)))
    ((str c)
     (cond
       ((not (string? str))
        (print (call-with-string-output-port
                 (lambda (p) (display str p)))
               c))
       (else
        (let lp ((x (console-x c)) (y (console-y c)) (i 0))
          (cond
            ((eqv? i (string-length str))
             (console-x-set! c x)
             (console-y-set! c y))
            (else
             (let-values ([(buf mbuf fbuf bbuf abuf idx remx) (%index c x y)])
               (set-console-dirty! c y)
               (let lp* ((x x) (i i) (idx idx) (remx remx))
                 (cond
                   ((fx=? i (string-length str))
                    (lp x y i))
                   ((eqv? remx 0)
                    (%next-line c)
                    (lp (console-x c) (console-y c) i))
                   (else
                    (let-values ([(ch mods screenlen strlen) (string-cluster-ref str i)])
                      (let ((ch (cond
                                  ((char<=? #\space ch #\~) ch)
                                  ((eq? (char-general-category ch) 'Cc)
                                   ;; Sanitize control characters
                                   (cond
                                     ((char<? ch #\space)
                                      (integer->char (fx+ #x2400 (char->integer ch))))
                                     ((char=? ch #\delete)
                                      (integer->char #x2421))
                                     ;; Not nice: the replacement character
                                     (else #\xfffd)))
                                  (else ch))))
                        (fg-set! fbuf idx (console-fg c))
                        (bg-set! bbuf idx (console-bg c))
                        (attr-set! abuf idx (console-attr c))
                        (cond
                          ((fx>? screenlen remx)
                           ;; The character is too wide to fit the screen
                           (text-set! buf idx #\space)
                           (mark-set! mbuf idx "")
                           (lp* x i idx 0))
                          (else
                           ;; XXX: NFC is needed for the Linux console
                           (let-values ([(ch mods) (grapheme-nfc ch mods)])
                             (text-set! buf idx ch)
                             (mark-set! mbuf idx mods)
                             (when (eqv? screenlen 2)
                               (text-clear! buf (fx+ idx 1)))
                             (lp* (fx+ x screenlen)
                                  (fx+ i strlen)
                                  (fx+ idx screenlen)
                                  (fx- remx screenlen)))))))))))))))))))

(define println
  (case-lambda
    (()
     (println "" (current-console)))
    ((str) (println str (current-console)))
    ((str c)
     (print str c)
     (%next-line c))))

(define (%handle-resize c ev)
  (when (resize-event? ev)
    (console-resize! c (resize-event-cols ev) (resize-event-rows ev)
                     (resize-event-width ev) (resize-event-height ev))
    (redraw-screen c)))

(define read-event
  (case-lambda
    (()
     (read-event (current-console)))
    ((c)
     (let ((ev ((console-backend c) 'read-event #f)))
       (%handle-resize c ev)
       ev))))

(define read-key
  (case-lambda
    (()
     (read-key (current-console)))
    ((c)
     (let lp ()
       (let ((ev (read-event c)))
         ;; FIXME: Return something that makes sense when there are
         ;; modifiers.
         (cond ((and (key-press-event? ev)
                     (keyboard-event-char ev)))
               ((input-closed-event? ev) ev)
               (else (lp))))))))

(define event-ready?
  (case-lambda
    (()
     (event-ready? (current-console)))
    ((c)
     ((console-backend c) 'event-ready? #f))))

(define restore-console
  (case-lambda
    (()
     (restore-console (current-console)))
    ((c)
     ((console-backend c) 'restore #f))))

(define current-console                 ;FIXME: must be a parameter
  (let ((c (default-console)))
    (case-lambda
      (() c)
      ((c^) (set! c c^))))))
