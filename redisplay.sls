;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018, 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Screen update optimization for serial line terminals

;; Based on "A Redisplay Algorithm" by James Gosling (1981).
;; This implementation tries hard to not invoke Cthulhu, but be
;; warned.

(library (text-mode redisplay)
  (export
    #;terminal-diff

    ;; Internal but exposed for testing.
    compute-trace
    run-trace)
  (import
    (rnrs (6))
    (text-mode console model)
    (text-mode terminfo constants))

(define (string-length-left str)
  ;; XXX: should not be needed
  (let lp ((i (fx- (string-length str) 1)))
    (cond ((eqv? i -1)
           0)
          ((char=? (string-ref str i) #\space)
           (lp (fx- i 1)))
          (else (fx+ i 1)))))

(define-record-type matrix
  (fields m x y)
  (nongenerative matrix-v0-5f97f264-63b9-45b2-8903-58adb2a6a940)
  (protocol
   (lambda (p)
     (lambda (x y)
       (p (do ((i -1 (fx+ i 1))
               (v* '() (cons (make-vector (+ x 1) 0) v*)))
              ((fx=? i y) (list->vector v*)))
          x y)))))

(define (matrix-ref a x y)
  (vector-ref (vector-ref (matrix-m a) (fx+ y 1))
              (fx+ x 1)))

(define (matrix-set! a x y v)
  (vector-set! (vector-ref (matrix-m a) (fx+ y 1))
               (fx+ x 1) v))

(define (print-matrix D)
  (do ((y -1 (+ y 1))) ((= y (matrix-y D)))
    (do ((x -1 (+ x 1))) ((= x (matrix-x D)))
      (display (matrix-ref D x y))
      (display #\tab))
    (newline)))

;;; The algorithms

(define (compute-trace A-len B-len cost-transform cost-delete cost-insert)
  ;; "Algorithm 2"
  (define cost-min                      ;ignores inexactness
    (case-lambda
      ((x y z) (cost-min (cost-min x y) z))
      ((x y) (if (< x y) x y))))
  (define (Ct i j)
    (if (or (<= i 0) (<= j 0))
        0
        (cost-transform (- i 1) (- j 1))))
  (define (Cd i) (if (<= i 0) 0 (cost-delete (- i 1))))
  (define (Ci j) (if (<= j 0) 0 (cost-insert (- j 1))))
  (let ((D (make-matrix (+ A-len 1) (+ B-len 1)))
        (W (make-matrix (+ A-len 1) (+ B-len 1))))
    (matrix-set! D -1 -1 0)
    (do ((i 0 (fx+ i 1))) ((> i A-len))
      (matrix-set! D i -1 +inf.0))
    (do ((j 0 (fx+ j 1))) ((> j B-len))
      (matrix-set! D -1 j +inf.0))
    (do ((i 0 (fx+ i 1))) ((> i A-len))
      (do ((j 0 (fx+ j 1))) ((> j B-len))
        (let ((Tt (+ (matrix-ref D (fx- i 1) (fx- j 1)) (Ct i j)))
              (Td (+ (matrix-ref D (fx- i 1) j)         (Cd i)))
              (Ti (+ (matrix-ref D i         (fx- j 1)) (Ci j))))
          (let ((T (cost-min Tt Td Ti)))
            ;; (display (list 'Tt Tt 'Td Td 'Ti Ti '=> 'T T)) (newline)
            (matrix-set! D i j T)
            (cond ((= T Tt) (matrix-set! W i j 'redraw))
                  ((= T Td) (matrix-set! W i j 'delete))
                  (else     (matrix-set! W i j 'insert)))))))
    ;; (print-matrix D)
    ;; (newline)
    ;; (print-matrix W)
    (values W D)))

;; Run the redisplay algorithm using the output from compute-trace.
(define (run-trace A-len B-len W transform delete insert)
  ;; "Algorithm 4"
  ;; FIXME: isn't A-len / B-len in the matrix?
  (let redisplay ((i A-len) (j B-len))
    (when (or (fx>? i 0) (fx>? j 0))
      (case (matrix-ref W i j)
        ((redraw)
         (redisplay (fx- i 1) (fx- j 1))
         (transform (fx- i 1) (fx- j 1)))
        ((delete)
         (delete (fx- i 1))
         (redisplay (fx- i 1) j))
        ((insert)
         (redisplay i (fx- j 1))
         (insert (fx- j 1))
         (transform #f (fx- j 1)))
        (else
         (error 'run-trace "Bad value in W" W i j (matrix-ref W i j)))))))

;; Compute the trace for updating line A to look like line B.
(define (compute-trace/line-update A B)
  ;; Cost functions
  (define (Cd i)
    ;; Deleting the character at i
    0)
  (define (Ci j)
    ;; Inserting a character at j + typing in the cluster at j
    1)
  (define (Ct i j)
    ;; Transforming the cluster at i to the cluster at j
    (if (char=? (string-ref A i) (string-ref B j))
        0
        +inf.0))
  (compute-trace (string-length A)
                 (string-length B)
                 Ct Cd Ci))

;; Compute the trace for updating screen A to look like screen B.
(define (compute-trace/screen-update A B)
  ;; "Algorithm 6". A: old screen. B: new screen.
  (define (cost-delete i)
    ;; Deleting line at i
    (+ 2 (string-length (string-append (number->string i) "d" "1M"))))
  (define (cost-insert j)
    ;; Inserting a line at j and typing in line j
    (+ 2 (string-length (string-append (number->string j) "d" "1L"))
       (string-length-left (vector-ref B j))))
  (define (cost-transform i j)
    ;; Transforming the line A[i] to A[j]
    (let ((lineA (vector-ref A i))
          (lineB (vector-ref B j)))
      ;; TODO: Speed.
      (if (string=? lineA lineB)
          0
          (let-values ([(W _) (compute-trace/line-update lineA lineB)])
            (matrix-ref W (fx- (string-length lineA) 1)
                       (fx- (string-length lineB) 1))))))
  (compute-trace (vector-length A)
                 (vector-length B)
                 cost-transform cost-delete cost-insert))

(define (line-update p A B)
  (define (delete-cell i)
    ;; Delete a character at i
    (write (list 'D i)))
  (define (insert-cell j)
    ;; Insert a character at j and write B[j] there
    (write (list 'I j (and j (string-ref B j)))))
  (define (transform-cell i j)
    ;; Transform A[i] to B[j] at column j
    (let ((x (if i (string-ref A i) #\nul))
          (y (string-ref B j)))
      (write '(t))
      (unless (eqv? x y)
        (write (list 'T i j x y)))))
  (let-values ([(W _) (compute-trace/line-update A B)])
    (run-trace (string-length A)
               (string-length B) W
               transform-cell delete-cell insert-cell)))

(define (screen-update p A B)
  ;; "Algorithm 5"
  ;; (define write (lambda x #f))
  ;; (define newline (lambda x #f))
  (define (delete-line i)
    ;; Delete a line. Moves lines below i up one row.
    (write (list 'DeleteLine i))
    (newline)
    '(terminal-VPA p (+ 1 i))
    '(terminal-DL p 1))
  (define (insert-line j)
    ;; Insert a line. Moves lines at i down a row.
    (write (list 'InsertLine j))
    (newline)
    '(terminal-VPA p (+ 1 j))
    '(terminal-IL p 1))
  (define (transform-line i j)
    ;; Transform line index B-j so it looks like B[j] instead of A[i]
    (let ((old-e (if i (vector-ref A i) ""))  ;FIXME: maybe make-string here
          (new-e (vector-ref B j)))
      (unless (string=? old-e new-e)
        (write (list 'TransformLine j old-e new-e))
        (newline)
        (line-update p old-e new-e (compute-trace/line-update old-e new-e)))))
  (let-values ([(W _) (compute-trace/screen-update A B)])
    (run-trace (vector-length A)
               (vector-length B) W
               transform-line delete-line insert-line))))
