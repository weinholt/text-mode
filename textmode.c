/* -*- mode: c; coding: utf-8 -*-
 * Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
 * SPDX-License-Identifier: MIT
 */

#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>

int textmode_get_window_size(int fd, char *buf) {
    struct winsize ws;

    if (ioctl(fd, TIOCGWINSZ, &ws) != 0)
        return -errno;

    memcpy(buf, &ws, sizeof(short) * 4);

    return 0;
}

/* FIXME: should not hide the state */
static struct termios orig;

int textmode_raw_mode(int fd) {
    struct termios raw;

    if (tcgetattr(fd, &orig) == -1)
        return -1;

    memcpy(&raw, &orig, sizeof raw);
    raw.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag &= ~(CSIZE);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    raw.c_cc[VMIN] = 1;
    raw.c_cc[VTIME] = 0;

    return tcsetattr(fd, TCSAFLUSH, &raw);
}

int textmode_canonical_mode(int fd) {
    /* TODO: This does not actually set canonical mode, does it... */
    tcsetattr(fd, TCSAFLUSH, &orig);

    return 0;
}
