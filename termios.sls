;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Oversimplified termios interface

(library (text-mode termios)
  (export
    termios-get-window-size
    termios-raw-mode
    termios-canonical-mode)
  (import
    (rnrs (6))
    (pffi))

(define lib
  (open-shared-object "libtextmode.so"))

;; Returns cols, rows, width, height (order has been corrected)
(define termios-get-window-size
  (let ((f (foreign-procedure lib int textmode_get_window_size (int pointer))))
    (lambda (fd)
      (assert (fixnum? fd))
      (let ((bv (make-bytevector (* 4 2) 0)))
        (let ((ret (f fd (bytevector->pointer bv))))
          (unless (>= ret 0)
            (error 'termios-get-window-size "Unable to get the window size" fd ret)))
        (values (bytevector-u16-native-ref bv 2)
                (bytevector-u16-native-ref bv 0)
                (bytevector-u16-native-ref bv 4)
                (bytevector-u16-native-ref bv 6))))))

(define termios-raw-mode
  (let ((f (foreign-procedure lib int textmode_raw_mode (int))))
    (lambda (fd)
      (assert (fixnum? fd))
      (f fd))))

(define termios-canonical-mode
  (let ((f (foreign-procedure lib int textmode_canonical_mode (int))))
    (lambda (fd)
      (assert (fixnum? fd))
      (f fd)))))
