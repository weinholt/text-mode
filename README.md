# (text-mode)

This is an R6RS Scheme library for text-oriented consoles with
keyboard and mouse input. It is not a teletype abstraction. These
libraries take inspiration from ncurses, OpenSSH, VGA text mode, xterm
and Turbo Pascal's CRT unit.

- `(text-mode console)` - main library: representation of a
  console with a text-oriented screen.
- `(text-mode console events)` - event models.
- `(text-mode console model)` - console model.
- `(text-mode pixels)` - pixel drawing with Unicode characters.
- `(text-mode redisplay)` - delta update algorithm for serial-line
  terminal emulators. *Not currently used.*
- `(text-mode terminfo)` - interface to the terminfo database, which
  describes the commands for terminals.
- `(text-mode terminfo constants)` - constants that describe the
  capabilities in the terminfo database.
- `(text-mode terminfo parser)` - parsers for compiled terminfo
  database entries and parameterized strings.
- `(text-mode termios)` - Unix terminal raw mode, canonical mode and
  window size.
- `(text-mode unicode)` - helper for width of Unicode characters.

## Documentation

These libraries are documented separately:

- [console](docs/console.md) - high-level console API
- [terminfo](docs/terminfo.md) - low-level terminfo API

## Versioning

The parts documented above can be expected to follow SemVer, with
deviations counting as bugs.
