;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Terminfo API for terminal input and output

(library (text-mode terminfo)
  (export
    ;; Representation of a serial terminal
    make-termstate termstate?
    termstate-in termstate-in-set!
    termstate-out termstate-out-set!
    termstate-baudrate termstate-baudrate-set!
    termstate-dynamic-vars
    termstate-static-vars
    termstate-ti termstate-ti-set!      ;terminfo database
    termstate-translate-input
    termstate-consume-input

    get-terminfo-db
    terminfo-db->sexpr
    sexpr->terminfo-db

    tigetnum
    tigetflag
    tigetstr
    tigetproc
    ticall

    terminfo-procedure

    make-terminfo-backend)
  (import
    (rnrs (6))
    (srfi :98 os-environment-variables)
    (struct pack)
    (text-mode console events)
    (text-mode console model)
    (text-mode terminfo constants)
    (text-mode terminfo parser)
    (text-mode termios))

(define *builtin-dumb*
  '("dumb|80-column dumb tty" (am . #t) (cols . 80)
    (bel . "\a") (cr . "\r") (cud1 . "\n") (ind . "\n")))

(define *builtin-ansi*
  '("ansi|ansi (text-mode terminfo built-in)" (am . #t)
    (mir . #t) (msgr . #t) (mc5i . #t) (OTbs . #t) (cols . 80)
    (it . 8) (lines . 24) (colors . 8) (pairs . 64) (ncv . 3)
    (cbt . "\x1B;[Z") (bel . "\a") (cr . "\r")
    (tbc . "\x1B;[3g") (clear . "\x1B;[H\x1B;[J")
    (el . "\x1B;[K") (ed . "\x1B;[J") (hpa . "\x1B;[%i%p1%dG")
    (cup . "\x1B;[%i%p1%d;%p2%dH") (cud1 . "\x1B;[B")
    (home . "\x1B;[H") (cub1 . "\x1B;[D") (cuf1 . "\x1B;[C")
    (cuu1 . "\x1B;[A") (dch1 . "\x1B;[P") (dl1 . "\x1B;[M")
    (smacs . "\x1B;[11m") (blink . "\x1B;[5m")
    (bold . "\x1B;[1m") (invis . "\x1B;[8m") (rev . "\x1B;[7m")
    (smso . "\x1B;[7m") (smul . "\x1B;[4m")
    (ech . "\x1B;[%p1%dX") (rmacs . "\x1B;[10m")
    (sgr0 . "\x1B;[0;10m") (rmso . "\x1B;[m") (rmul . "\x1B;[m")
    (il1 . "\x1B;[L") (kbs . "\b") (kcud1 . "\x1B;[B")
    (khome . "\x1B;[H") (kich1 . "\x1B;[L") (kcub1 . "\x1B;[D")
    (kcuf1 . "\x1B;[C") (kcuu1 . "\x1B;[A") (nel . "\r\x1B;[S")
    (dch . "\x1B;[%p1%dP") (dl . "\x1B;[%p1%dM")
    (cud . "\x1B;[%p1%dB") (ich . "\x1B;[%p1%d@")
    (indn . "\x1B;[%p1%dS") (il . "\x1B;[%p1%dL")
    (cub . "\x1B;[%p1%dD") (cuf . "\x1B;[%p1%dC")
    (rin . "\x1B;[%p1%dT") (cuu . "\x1B;[%p1%dA")
    (mc4 . "\x1B;[4i") (mc5 . "\x1B;[5i")
    (rep . "%p1%c\x1B;[%p2%{1}%-%db") (vpa . "\x1B;[%i%p1%dd")
    (ind . "\n")
    (sgr . "\x1B;[0;10%?%p1%t;7%;%?%p2%t;4%;%?%p3%t;7%;%?%p4%t;5%;%?%p6%t;1%;%?%p7%t;8%;%?%p9%t;11%;m")
    (hts . "\x1B;H") (ht . "\x1B;[I")
    (acsc . #vu8(43 16 44 17 45 24 46 25 48 219 96 4 97 177 102 248 103 241
                    104 176 106 217 107 191 108 218 109 192 110 197 111 126 112
                    196 113 196 114 196 115 95 116 195 117 180 118 193 119 194
                    120 179 121 243 122 242 123 227 124 216 125 156 126 254))
    (kcbt . "\x1B;[Z") (el1 . "\x1B;[1K")
    (u6 . "\x1B;[%i%d;%dR") (u7 . "\x1B;[6n")
    (u8 . "\x1B;[?%[;0123456789]c") (u9 . "\x1B;[c")
    (op . "\x1B;[39;49m") (setaf . "\x1B;[3%p1%dm")
    (setab . "\x1B;[4%p1%dm") (s0ds . "\x1B;(B")
    (s1ds . "\x1B;)B") (s2ds . "\x1B;*B") (s3ds . "\x1B;+B")
    (smpch . "\x1B;[11m") (rmpch . "\x1B;[10m") (AX . #t)))

(define-record-type termstate
  (fields (mutable in)
          (mutable out)
          (mutable baudrate)
          dynamic-vars
          static-vars
          proc-cache
          (mutable ti-db)
          (mutable keys)
          (mutable keybuffer))
  (protocol
   (lambda (p)
     (letrec ((mkterm-state
               (case-lambda
                 ((in out)
                  (mkterm-state in out 38400))
                 ((in out baudrate)
                  (p in out
                     baudrate
                     (make-eq-hashtable)
                     (make-eq-hashtable)
                     (make-hashtable equal-hash equal?)
                     #f
                     #f
                     #vu8())))))
       mkterm-state))))

(define (termstate-ti-set! ts db)
  (termstate-keys-set! ts (terminfo-keys->list db))
  (termstate-ti-db-set! ts db))

(define (termstate-ti ts)
  (cond ((termstate-ti-db ts))
        (else
         ;; Late initialization, since this goes to the filesystem to
         ;; load the database
         (termstate-ti-set! ts (get-terminfo-db))
         (termstate-ti-db ts))))

(define subbytevector
  (case-lambda
    ((bv start end)
     (unless (fx<=? 0 start end (bytevector-length bv))
       (error 'subbytevector "Invalid indices" bv start end))
     (if (and (fxzero? start)
              (fx=? end (bytevector-length bv)))
         bv
         (let ((ret (make-bytevector (fx- end start))))
           (bytevector-copy! bv start
                             ret 0 (fx- end start))
           ret)))
    ((bv start)
     (subbytevector bv start (bytevector-length bv)))))

(define bytevector-u8-index
  (case-lambda
    ((bv c start end)
     (let lp ((i start))
       (cond ((fx=? end i) #f)
             ((fx=? (bytevector-u8-ref bv i) c) i)
             (else (lp (fx+ i 1))))))
    ((bv c start)
     (bytevector-u8-index bv c start (bytevector-length bv)))
    ((bv c)
     (bytevector-u8-index bv c 0 (bytevector-length bv)))))

(define bytevector-prefix-length
  (case-lambda
    ((bv1 bv2 s1)
     (bytevector-prefix-length bv1 bv2 s1 (bytevector-length bv1)
                               0 (bytevector-length bv2)))
    ((bv1 bv2 s1 e1 s2 e2)
     (let lp ((i 0) (s1 s1) (s2 s2))
       (cond ((or (fx=? s1 e1) (fx=? s2 e2))
              i)
             ((fx=? (bytevector-u8-ref bv1 s1) (bytevector-u8-ref bv2 s2))
              (lp (fx+ i 1) (fx+ s1 1) (fx+ s2 1)))
             (else i))))))

(define (string-contains-char? str chars)
  (let lp ((i 0))
    (cond ((fx=? i (string-length str)) #f)
          ((memv (string-ref str i) chars) #t)
          (else (lp (fx+ i 1))))))

(define get-terminfo-db
  (case-lambda
    (()
     (get-terminfo-db #f))
    ((term)
     ;; TODO: Use $TERMINFO, which can also encode the entry directly
     ;; with hex: or b64: (but only if its name matches term).
     ;; TODO: USE $TERMINFO_DIRS, which is split by #\: or #\;
     (let ((term (or term (get-environment-variable "TERM")))
           (home (get-environment-variable "HOME")))
       (cond
         ((and term
               (not (string=? term ""))
               (not (string-contains-char? term '(#\/ #\:)))
               (exists (lambda (dir)
                         (and dir
                              (let ((fn (string-append dir "/" (string (string-ref term 0))
                                                       "/" term)))
                                (guard (exn ((i/o-error? exn) #f))
                                  (call-with-port (open-file-input-port fn)
                                    get-compiled-ncurses-terminfo)))))
                       `(,(and home (string-append home "/.terminfo"))
                         "/etc/terminfo"
                         "/lib/terminfo"
                         "/usr/share/terminfo"))))
         ((equal? term "dumb")
          (sexpr->terminfo-db *builtin-dumb*))
         ((or (not term) (equal? term "") (equal? term "ansi"))
          (sexpr->terminfo-db *builtin-ansi*))
         (else
          (error 'get-terminfo-db "Could not find terminfo file" term)))))))

(define (terminfo-db->sexpr db)
  (define (convert-boolean name value)
    (and (not (eqv? value #f)) (cons name value)))
  (define (convert-number name value)
    (and (not (eqv? value -1)) (cons name value)))
  (define (convert-string name value)
    (and (not (eqv? value #f))
         (let ((str (utf8->string value)))
           (if (equal? value (string->utf8 str))
               (cons name str)
               (cons name value)))))
  (define (vector-map* proc v0 v1)
    (let lp ((i 0))
      (if (or (fx=? i (vector-length v0)) (fx=? i (vector-length v1)))
          '()
          (let ((v (proc (vector-ref v0 i) (vector-ref v1 i))))
            (if v
                (cons v (lp (fx+ i 1)))
                (lp (fx+ i 1)))))))
  (cons (terminfo-db-terminal-names db)
        (append
         (vector-map* convert-boolean terminfo-booleans (terminfo-db-booleans db))
         (vector-map* convert-number terminfo-numbers (terminfo-db-numbers db))
         (vector-map* convert-string terminfo-strings (terminfo-db-strings db))
         (let-values (((ks vs) (hashtable-entries (terminfo-db-ext-booleans db))))
           (vector-map* convert-boolean ks vs))
         (let-values (((ks vs) (hashtable-entries (terminfo-db-ext-numbers db))))
           (vector-map* convert-number ks vs))
         (let-values (((ks vs) (hashtable-entries (terminfo-db-ext-strings db))))
           (vector-map* convert-string ks vs)))))

(define (sexpr->terminfo-db sexpr)
  (let ((terminal-names (car sexpr))
        (booleans (make-vector (vector-length terminfo-booleans) #f))
        (numbers (make-vector (vector-length terminfo-numbers) -1))
        (strings (make-vector (vector-length terminfo-strings) #f))
        (ext-booleans (make-eq-hashtable))
        (ext-numbers (make-eq-hashtable))
        (ext-strings (make-eq-hashtable)))
    (define (vector-index key vec)
      (let lp ((i 0))
        (cond ((fx=? i (vector-length vec)) #f)
              ((eq? key (vector-ref vec i)) i)
              (else (lp (fx+ i 1))))))
    (define (set-key&value names values extended name value)
      (cond ((vector-index name names)
             => (lambda (i) (vector-set! values i value)))
            (else (hashtable-set! extended name value))))
    (for-each
     (lambda (cap)
       (let ((name (car cap)) (value (cdr cap)))
         (cond ((boolean? value)
                (set-key&value terminfo-booleans booleans ext-booleans name value))
               ((number? value)
                (set-key&value terminfo-numbers numbers ext-numbers name value))
               ((string? value)
                (set-key&value terminfo-strings strings ext-strings name (string->utf8 value)))
               ((bytevector? value)
                (set-key&value terminfo-strings strings ext-strings name value))
               (else
                (assertion-violation 'sexpr->terminfo-db "Bad value" name value)))))
     (cdr sexpr))
    (make-terminfo-db terminal-names
                      booleans
                      numbers
                      strings
                      ext-booleans
                      ext-numbers
                      ext-strings)))

(define (%ti-getter name type get get-ext default)
  (define ti-getcap
    (case-lambda
      ((capability ts)
       (cond ((pair? capability)
              (let ((captype (car capability)) (index (cdr capability)))
                (unless (eq? (car capability) type)
                  (assertion-violation name
                                       (string-append "Expected a "
                                                      (symbol->string (car capability))
                                                      " capability")
                                       captype index))
                (vector-ref (get (termstate-ti ts)) index)))
             (else
              (hashtable-ref (get-ext (termstate-ti ts)) capability default))))))
  ti-getcap)

;; Get a terminfo flag, number or string
(define tigetflag
  (%ti-getter 'tigetflag 'boolean terminfo-db-booleans terminfo-db-ext-booleans #f))

(define tigetnum
  (%ti-getter 'tigetnum 'number terminfo-db-numbers terminfo-db-ext-numbers -1))

(define tigetstr
  (%ti-getter 'tigetstr 'string terminfo-db-strings terminfo-db-ext-strings #f))

;; Get a terminfo procedure
(define tigetproc
  (case-lambda
    ((capability ts)
     (cond ((tigetstr capability ts) =>
            (lambda (str)
              (cond ((hashtable-ref (termstate-proc-cache ts) str #f))
                    (else
                     ;; Cache the compiled procedure
                     (let ((proc (terminfo-procedure str ts)))
                       (hashtable-set! (termstate-proc-cache ts) str proc)
                       proc)))))
           (else
            (lambda _ _))))))

;; Helper for easily calling a terminfo procedure
(define ticall
  (case-lambda
    ((capability ts)
     ((tigetproc capability ts)))
    ((capability ts p1)
     ((tigetproc capability ts) p1))
    ((capability ts p1 p2)
     ((tigetproc capability ts) p1 p2))
    ((capability ts p1 p2 . p*)
     (apply (tigetproc capability ts) p1 p2 p*))))

(define (decode-fail . _)
  ;; Returns key clen mlen. If key is true then a key was decoded and
  ;; clen indicates the number of bytes that the key used.
  (values #f 0 0))

(define (decode-utf8 buf offset _keys)
  (define (dec-utf8 n len lower-limit)
    (let ((len (fx+ (fx- offset 1) len)))
      (if (fx>? len (bytevector-length buf))
          (decode-fail)
          (let lp ((n n) (i offset))
            (if (fx=? i len)
                (if (or (fx<? n lower-limit) (fx<=? #xD800 n #xDFFF))
                    (decode-fail)
                    (values (integer->char n) len len))
                (let ((b (bytevector-u8-ref buf i)))
                  (cond ((eqv? (fxand b #b11000000) #b10000000)
                         (lp (fxior (fxarithmetic-shift-left n 6)
                                    (fxand b #b111111))
                             (fx+ i 1)))
                        (else
                         (decode-fail)))))))))
  (let ((b (bytevector-u8-ref buf (fx- offset 1))))
    (cond
      ((eqv? (fxand b #b11100000) #b11000000)
       (dec-utf8 (fxand b #b00011111) 2 #x80))
      ((eqv? (fxand b #b11110000) #b11100000)
       (dec-utf8 (fxand b #b00001111) 3 #x800))
      ((eqv? (fxand b #b11111000) #b11110000)
       (dec-utf8 (fxand b #b00000111) 4 #x10000))
      (else
       (decode-fail)))))

(define (get-int p)
  (let lp ((i #f))
    (if (or (port-eof? p) (not (char<=? #\0 (lookahead-char p) #\9)))
        (or i (eof-object))
        (lp (+ (if i (* i 10) 0)
               (fx- (char->integer (get-char p))
                    (char->integer #\0)))))))

;; Decode cursor position
(define (make-decode-user6 user6)
  (cond
    ((equal? user6 (string->utf8 "\x1b;[%i%d;%dR"))
     (lambda (buf offset _keys)
       (cond
         ((bytevector-u8-index buf (char->integer #\R) offset) =>
          (lambda (R-pos)
            (let ((p (open-string-input-port
                      (utf8->string (subbytevector buf offset R-pos)))))
              (let* ((y (get-int p)) (sep (get-char p)) (x (get-int p))
                     (term (get-char p)))
                (if (and (fixnum? x) (eqv? sep #\;) (fixnum? y) (eof-object? term)
                         (fx<=? 1 x 32767) (fx<=? 1 y 32767))
                    (values (make-cursor-report-event (fx- x 1) (fx- y 1))
                            (fx+ R-pos 1) (fx+ R-pos 1))
                    (decode-fail))))))
         (else
          (decode-fail)))))
    (else
     (lambda _ (decode-fail)))))

;; Decode Device Attributes (DA)
(define (make-decode-user8 user8)
  (define architectures
    '((1 . VT100)
      (6 . VT102)
      (12 . VT125)
      (62 . VT2xx)
      (63 . VT3xx)
      (64 . VT4xx)
      (65 . VT5xx)))
  (define attributes
    '((0 . no-options)
      (1 . columns-132)
      (2 . printer)
      (3 . regis)
      (4 . sixel)
      (6 . selective-erase)
      (7 . soft-character-sets)
      (8 . user-defined-keys)
      (9 . national-replacement-charsets)
      (13 . local-editing-mode)
      (14 . eight-bit)
      (15 . technical-character-set)
      (16 . mouse)
      (17 . terminal-state-reports)
      (18 . windowing)
      (19 . dual-sessions)
      (21 . horizontal-scrolling)
      (22 . ansi-color)
      (29 . ansi-text-location)
      (42 . latin-2)
      (44 . pcterm4)))
  (lambda (buf offset _keys)
    (cond
      ((bytevector-u8-index buf (char->integer #\c) offset) =>
       (lambda (c-pos)
         (let ((p (open-string-input-port
                   (utf8->string (subbytevector buf offset c-pos)))))
           (let lp ((num* '()))
             (let* ((num (get-int p))
                    (sep (get-char p)))
               (cond ((and (fixnum? num) (eof-object? sep))
                      (if (null? num*)
                          (decode-fail)
                          (let ((num* (reverse (cons num num*))))
                            (values
                              (make-device-attributes-event
                               (cond ((assq (car num*) architectures) => cdr)
                                     (else (car num*)))
                               (map (lambda (attr)
                                      (cond ((assq attr attributes) => cdr)
                                            (else attr)))
                                    (cdr num*)))
                              (fx+ c-pos 1) (fx+ c-pos 1)))))
                     ((eqv? sep #\;)
                      (lp (cons num num*)))
                     (else
                      (decode-fail))))))))
      (else
       (decode-fail)))))

(define (decode-esc buf offset keys)
  ;; The normal case is that a whole key escape sequence arrives in a
  ;; single read() syscall. So a single ESC is a true escape. (The
  ;; alternative is a bad user experience: either sleep for a while
  ;; and wait to see if a full escape sequence arrives or have unknown
  ;; escape sequences decode as nonsense)
  (cond ((fx=? (bytevector-length buf) offset)
         ;; ESC or Alt-ESC
         (values 'KEY_ESC offset offset))
        ((fx>? offset 1)
         ;; ESC ESC ESC is not going to anywhere
         (decode-fail))
        (else
         (let-values ([(partial? key bytes) (match-key buf offset keys)])
           (cond
             ((and (char? key)
                   (fx>? (bytevector-length buf)
                         (fx+ (bytevector-length bytes) offset)))
              ;; This is a heuristic to reject unknown escape
              ;; sequences.
              (values (make-unknown-event 'escape buf)
                      (or (bytevector-u8-index buf (char->integer #\esc) offset)
                          (bytevector-length buf))
                      offset))
             ((and (not partial?) key (not (unknown-event? key)))
              ;; If ESC is followed by a valid key then Alt was held.
              (let ((len (fx+ offset (bytevector-length bytes))))
                (values (cons 'alt key) len len)))
             (else
              ;; Not for us. Hope someone else decodes this as a valid
              ;; escape sequence.
              (decode-fail)))))))

(define (mouse-decode-helper bi xi yi release?)
  (define SHIFT 2)
  (define ALT 3)
  (define CTRL 4)
  (define MOVE 5)
  (define EXTENDED 6)
  (define BUTTON-RELEASED 3)
  (let* ((state (fxand bi (fxnot #b11)))
         ;; If btn is BUTTON-RELEASED then button is not valid
         (btn (fxand bi #b11))
         (button (if (fxbit-set? state EXTENDED) (fx+ btn 3) btn)))
    (let ((mkevent
           (cond
             ((or release? (eqv? btn BUTTON-RELEASED)) make-mouse-up-event)
             ((fxbit-set? state MOVE) make-mouse-move-event)
             ((memv button '(3 4))
              ;; Tries to give control over the scroll amount
              ;; using alt and ctrl.
              (let ((delta-y (if (eqv? button BUTTON-RELEASED) -1 1)))
                ;; XXX: Shift+scroll is eaten by the terminal
                (let-values ([(delta-mode factor)
                              (cond ((fxbit-set? state ALT) (values 'line 1))
                                    ((fxbit-set? state CTRL) (values 'page 1))
                                    (else (values 'page 1/5)))])
                  (lambda (mods buttons x y)
                    (make-wheel-event mods buttons x y
                                      0 (* delta-y factor) 0 delta-mode)))))
             (else make-mouse-down-event)))
          ;; FIXME: If this is a release then we don't have the
          ;; code...? So the code should have been saved?
          (buttons (if (eqv? button BUTTON-RELEASED)
                       0 (bitwise-arithmetic-shift-left 1 button)))
          (mods (list->modifier-set
                 `(,@(if (fxbit-set? state SHIFT) '(shift) '())
                   ,@(if (fxbit-set? state ALT) '(alt) '())
                   ,@(if (fxbit-set? state CTRL) '(ctrl) '())))))
      (mkevent mods buttons (fx- xi 1) (fx- yi 1)))))

(define (decode-mouse buf offset _keys)
  ;; The latin1 version is limited to 223 and the UTF-8 version to
  ;; 2015.
  (let lp ((code (utf8->string buf)) (encoding 'utf8))
    (cond
      ((fx<? (string-length code) (fx+ offset 3))
       (decode-fail))
      (else
       (let ((bc (string-ref code offset))
             (xc (string-ref code (fx+ offset 1)))
             (yc (string-ref code (fx+ offset 2))))
         (let ((bi (fx- (char->integer bc) (char->integer #\space)))
               (xi (fx- (char->integer xc) (char->integer #\space)))
               (yi (fx- (char->integer yc) (char->integer #\space)))
               (len (fx+ offset (if (eq? encoding 'utf8)
                                    (bytevector-length (string->utf8 (string bc xc yc)))
                                    3))))
           (cond
             ((or (char=? bc #\xFFFD) (char=? xc #\xFFFD) (char=? yc #\xFFFD))
              ;; The code is not valid UTF-8, so it is probably raw 8-bit.
              (lp (list->string (map integer->char (bytevector->u8-list buf))) 'latin1))
             ((or (fx<? xi 1) (fx<? yi 1))
              ;; Urxvt has been observed to send negative coordinates
              (values (make-unknown-event 'mouse (list xi yi)) len len))
             (else
              (values (mouse-decode-helper bi xi yi #f) len len)))))))))

(define (decode-sgr-mouse buf offset _keys)
  ;; XXX: Check if x,y can get into the surrogates and what happens then.
  (define SHIFT 2)
  (define ALT 3)
  (define CTRL 4)
  (define MOVE 5)
  (define EXTENDED 6)
  (define BUTTON-RELEASED 3)
  (define (decode code R-pos press?)
    (let ((p (open-string-input-port
              (utf8->string (subbytevector buf offset R-pos)))))
      (let* ((bi (get-int p)) (sep0 (get-char p))
             (xi (get-int p)) (sep1 (get-char p))
             (yi (get-int p)) (term (get-char p)))
        (if (and (fixnum? bi) (eqv? sep0 #\;)
                 (fixnum? xi) (eqv? sep1 #\;)
                 (fixnum? yi) (eof-object? term)
                 (fx<=? 1 xi 32767) (fx<=? 1 yi 32767))
            (values (mouse-decode-helper bi xi yi press?)
                    (fx+ R-pos 1) (fx+ R-pos 1))
            (decode-fail)))))
  (let ((code (utf8->string buf)))
    (cond
      ((fx<? (string-length code) (fx+ offset 3))
       (decode-fail))
      ;; XXX: #\M = press, #\m = release
      ((bytevector-u8-index buf (char->integer #\m) offset) =>
       (lambda (R-pos) (decode code R-pos 'release)))
      ((bytevector-u8-index buf (char->integer #\M) offset) =>
       (lambda (R-pos) (decode code R-pos #f)))
      (else (decode-fail)))))

(define (terminfo-keys->list db)
  ;; List of (#vu8(x y z) . binding). The binding can be a procedure,
  ;; which returns three values: key, consume-length and match-length.
  ;; If both key and consume-length are #f then there is no match. If
  ;; match-length is positive then the procedure found a partial
  ;; match. The longest bytes go first.
  ;; TODO: Maybe use a tree
  (define ret '())
  (define (add-key key-bytes key-name)
    (set! ret (cons (cons key-bytes key-name) ret)))
  ;; Ctrl-a, etc.
  (do ((i 0 (fx+ i 1))) ((fx=? i (char->integer #\space)))
    (add-key (u8-list->bytevector `(,i))
             `(ctrl . ,(char-downcase (integer->char (+ #x40 i))))))
  ;; Some of the control keys are produced by labeled keys
  (add-key #vu8(9) 'KEY_TAB)
  (add-key #vu8(13) 'KEY_ENTER)
  (add-key #vu8(27) decode-esc)
  ;; The printable ASCII
  (do ((i (char->integer #\space) (fx+ i 1))) ((fx=? i (char->integer #\delete)))
    (add-key (u8-list->bytevector `(,i)) (integer->char i)))
  ;; All valid leading UTF-8 bytes.
  (do ((i #xC2 (fx+ i 1))) ((fx=? i #xF5))
    (add-key (u8-list->bytevector `(,i)) decode-utf8))
  ;; Cursor position reporting.
  (cond
    ((vector-ref (terminfo-db-strings db) (cdr user6)) =>
     (lambda (u6)
       (cond
         ((bytevector-u8-index u6 (char->integer #\%)) =>
          (lambda (i)
            (add-key (subbytevector u6 0 i) (make-decode-user6 u6))))))))
  ;; Device Attributes (DA)
  (add-key (string->utf8 "\x1b;[?") (make-decode-user8 #f))
  ;; SGR mouse mode
  (add-key (string->utf8 "\x1b;[<") decode-sgr-mouse)
  ;; All pre-defined keys from the terminfo database
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (vector-length terminfo-strings)))
    (let ((bv (vector-ref (terminfo-db-strings db) i))
          (key (vector-ref terminfo-keys i)))
      (when (and bv key)
        (if (eq? key 'KEY_MOUSE)
            (add-key bv decode-mouse)
            (add-key bv key)))))
  ;; All extended keys from the terminfo database
  (let-values ([(names bvs) (hashtable-entries (terminfo-db-ext-strings db))])
    (vector-for-each
     (lambda (name bv)
       (when (bytevector? bv)
         (let* ((name (symbol->string name))
                (key (and (fx>? (string-length name) 0)
                          (char=? #\k (string-ref name 0))
                          ;; Extended key
                          (string->symbol
                           (string-append "xKEY_" (substring name 1 (string-length name)))))))
           (add-key bv key))))
     names bvs))
  ;; Sort by length
  (list-sort (lambda (x y)
               (fx>? (bytevector-length (car x)) (bytevector-length (car y))))
             ret))

;; Match the buffer against the key definitions. Returns three values:
;; partial?, key and bytes. If partial? is true then more input should
;; be read, although with a short delay, because there is a longer
;; sequence that could match the buffer.
(define (match-key buf offset all-keys)
  (define display (lambda _ _))
  (define write (lambda _ _))
  (define newline (lambda _ _))
  (let lp ((keys all-keys) (partial? #f) (mkey #f) (mbytes #f) (longest -1))
    (newline)
    (write (list 'keys keys)) (newline)
    (display (list 'offset offset 'partial? partial? 'mkey mkey 'mbytes mbytes 'longest longest)) (newline)
    (if (null? keys)
        (if mkey
            (values partial? mkey mbytes)
            (values partial? #f #f))
        (let ((mlen (bytevector-prefix-length buf (caar keys) offset)))
          (display (list 'mlen mlen)) (newline)
          (cond
            ((eqv? mlen 0)
             ;; Not even a partial match.
             (display "-> No match\n")
             (lp (cdr keys) partial? mkey mbytes longest))
            ((eqv? mlen (bytevector-length (caar keys)))
             ;; The full key code is at the start of the buffer.
             (display "-> Full key\n")
             (let lp* ((key (cdar keys)) (bytes (caar keys)) (mlen mlen))
               (display (list 'lp* key)) (newline)
               (cond
                 ((procedure? key)
                  ;; A procedure has been provided that will check how
                  ;; much of the buffer it wants to consume (if
                  ;; anything) and how much should be considered a
                  ;; match.
                  (let-values ([(key^ clen^ mlen^) (key buf (fx+ offset mlen) all-keys)])
                    (display (list 'proc=> key^ clen^ mlen^)) (newline)
                    (cond ((and key^ (fxpositive? clen^) (fx>? mlen^ longest))
                           ;; We have a new interpretation.
                           (display "-> Procedural match\n")
                           (lp* key^ (subbytevector buf offset clen^) mlen^))
                          ((and (fx>? mlen^ 0) (not (unknown-event? key^)))
                           ;; The procedure found a partial match.
                           (display "-> Procedural partial\n")
                           (lp (cdr keys) #t mkey mbytes longest))
                          (else
                           ;; The procedure rejected the buffer.
                           (display "-> Procedural reject\n")
                           (lp (cdr keys) partial? mkey mbytes longest)))))
                 ((fx>? mlen longest)
                  (display "-> Longest match\n")
                  (lp (cdr keys) partial? key bytes mlen))
                 (else
                  ;; The buffer is a prefix of the key code.
                  (display "-> Shorter match\n")
                  (lp (cdr keys) partial? mkey mbytes longest)))))
            ((and (eqv? mlen (bytevector-length buf)) (eqv? longest -1))
             ;; The buffer is a prefix of the key code. If we read
             ;; more data, maybe this very key will show up. (So
             ;; the caller needs to poll for more data.)
             (display "-> Buffer is a prefix\n")
             (lp (cdr keys) #t mkey mbytes longest))
            (else
             (display "-> Buffer is a shorter prefix\n")
             (lp (cdr keys) partial? mkey mbytes longest)))))))

(define list->modifier-set (enum-set-constructor (modifier-set)))

;; Should follow this, except with symbols instead of strings and
;; space is not special:
;; https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
(define keymapping
  '((KEY_ENTER #f Enter standard)
    (KEY_TAB #f Tab standard)
    (KEY_BTAB #f Tab standard shift)

    (KEY_DOWN #f ArrowDown standard)
    (xKEY_DN3 #f ArrowDown standard alt)
    (xKEY_DN5 #f ArrowDown standard ctrl)
    (xKEY_DN7 #f ArrowDown standard ctrl alt)
    (xKEY_DN #f ArrowDown standard shift)

    (KEY_LEFT #f ArrowLeft standard)
    (KEY_SLEFT #f ArrowLeft standard shift)
    (xKEY_LFT3 #f ArrowLeft standard alt)
    (xKEY_LFT5 #f ArrowLeft standard ctrl)
    (xKEY_LFT7 #f ArrowLeft standard ctrl alt)

    (KEY_RIGHT #f ArrowRight standard)
    (KEY_SRIGHT #f ArrowRight standard shift)
    (xKEY_RIT3 #f ArrowRight standard alt)
    (xKEY_RIT5 #f ArrowRight standard ctrl)
    (xKEY_RIT7 #f ArrowRight standard ctrl alt)

    (KEY_UP #f ArrowUp standard)
    (xKEY_UP3 #f ArrowUp standard alt)
    (xKEY_UP5 #f ArrowUp standard ctrl)
    (xKEY_UP7 #f ArrowUp standard ctrl alt)
    (xKEY_UP #f ArrowUp standard shift)

    (KEY_END #f End standard)
    (KEY_SEND #f End standard shift)
    (xKEY_END3 #f End standard alt)
    (xKEY_END5 #f End standard ctrl)
    (xKEY_END7 #f End standard ctrl alt)

    (KEY_HOME #f Home standard)
    (xKEY_HOM3 #f Home standard alt)
    (xKEY_HOM5 #f Home standard ctrl)
    (xKEY_HOM6 #f Home standard ctrl shift)
    (xKEY_HOM7 #f Home standard ctrl alt)
    (KEY_SHOME #f Home standard shift)

    (KEY_NPAGE #f PageDown standard)
    (KEY_SNEXT #f PageDown standard shift)
    (xKEY_NXT3 #f PageDown standard alt)
    (xKEY_NXT5 #f PageDown standard ctrl)
    (xKEY_NXT7 #f PageDown standard ctrl alt)

    (KEY_PPAGE #f PageUp standard)
    (KEY_SPREVIOUS #f PageUp standard shift)
    (xKEY_PRV3 #f PageUp standard alt)
    (xKEY_PRV5 #f PageUp standard ctrl)
    (xKEY_PRV7 #f PageUp standard ctrl alt)

    (KEY_BACKSPACE #f Backspace standard)

    (KEY_DC #f Delete standard)
    (KEY_SDC #f Delete standard shift)
    (xKEY_DC3 #f Delete standard alt)
    (xKEY_DC5 #f Delete standard ctrl)
    (xKEY_DC6 #f Delete standard ctrl shift)
    (xKEY_DC7 #f Delete standard ctrl alt)  ;!

    (KEY_IC #f Insert standard)
    (KEY_SIC #f Insert standard shift)
    (xKEY_IC3 #f Insert standard alt)
    (xKEY_IC5 #f Insert standard ctrl)
    (xKEY_IC6 #f Insert standard ctrl shift)
    (xKEY_IC7 #f Insert standard ctrl alt)

    (KEY_ESC #f Escape standard)
    (KEY_FND #f Find standard)

    (KEY_A1 #\7 #\7 numpad)
    (KEY_A3 #\9 #\9 numpad)
    (KEY_B2 #\5 #\5 numpad)
    (KEY_C1 #\1 #\1 numpad)
    (KEY_C3 #\3 #\3 numpad)

    ;; TODO: Variants with Ctrl exist in rxvt, but not in terminfo
    (KEY_F1 #f F1 standard)
    (KEY_F2 #f F2 standard)
    (KEY_F3 #f F3 standard)
    (KEY_F4 #f F4 standard)
    (KEY_F5 #f F5 standard)
    (KEY_F6 #f F6 standard)
    (KEY_F7 #f F7 standard)
    (KEY_F8 #f F8 standard)
    (KEY_F9 #f F9 standard)
    (KEY_F10 #f F10 standard)
    (KEY_F11 #f F11 standard)
    (KEY_F12 #f F12 standard)
    (KEY_F13 #f F13 standard)
    (KEY_F14 #f F14 standard)
    (KEY_F15 #f F15 standard)
    (KEY_F16 #f F16 standard)
    (KEY_F17 #f F17 standard)
    (KEY_F18 #f F18 standard)
    (KEY_F19 #f F19 standard)
    (KEY_F20 #f F20 standard)))

(define (key->event full-key)
  (if (event? full-key)
      full-key
      (let lp ((key full-key) (mods (modifier-set)))
        (cond
          ((pair? key)
           (case (car key)
             ((ctrl) (lp (cdr key) (enum-set-union mods (modifier-set ctrl))))
             ((alt) (lp (cdr key) (enum-set-union mods (modifier-set alt))))
             (else
              (error 'key->event "Unsupported symbol in key" full-key (car key)))))
          ((char? key)
           (let ((mods (if (char-upper-case? key)
                           (enum-set-union mods (modifier-set shift))
                           mods))
                 (char (cond ((enum-set-member? (modifier ctrl) mods)
                              #f)
                             ((enum-set-member? (modifier alt) mods)
                              #f)
                             (else key))))
             (make-key-press-event mods char key 'standard)))
          (else
           (cond ((assq key keymapping) =>
                  (lambda (mapping)
                    (let ((char (cadr mapping))
                          (keyname (caddr mapping))
                          (location (cadddr mapping))
                          (extra-mods (cddddr mapping)))
                      (make-key-press-event (enum-set-union mods (list->modifier-set
                                                                  extra-mods))
                                            char keyname location))))
                 (else
                  (make-unknown-event 'terminfo key))))))))

(define (termstate-translate-input state input)
  (cond ((eof-object? input)
         (values #f (make-input-closed-event) #vu8()))
        (else
         (termstate-keybuffer-set! state (call-with-bytevector-output-port
                                           (lambda (p)
                                             (put-bytevector p (termstate-keybuffer state))
                                             (put-bytevector p input))))
         (let ((buf (termstate-keybuffer state))
               (keys (termstate-keys state)))
           (match-key buf 0 keys)))))

(define (termstate-consume-input state len)
  (let ((buf (termstate-keybuffer state)))
    (termstate-keybuffer-set! state (subbytevector buf len))))

(define terminfo-procedure
  ;; FIXME: padding_baud_rate gives the lowest baud rate for padding,
  ;; pad_char is the padding char if not #\nul, xon_off disables
  ;; padding, but it should be used in flash
  (case-lambda
    ((term-string state)
     (let ((proc (terminfo-raw-procedure term-string))
           (dvar (termstate-dynamic-vars state))
           (svar (termstate-static-vars state)))
       ;; TODO: Affected line count is always 1, but should be passed
       ;; in somehow, maybe a parameter.
       (case-lambda
         (()
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                0 0 0 0 0 0 0 0 0))
         ((p1)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 0 0 0 0 0 0 0 0))
         ((p1 p2)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 0 0 0 0 0 0 0))
         ((p1 p2 p3)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 0 0 0 0 0 0))
         ((p1 p2 p3 p4)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 0 0 0 0 0))
         ((p1 p2 p3 p4 p5)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 p5 0 0 0 0))
         ((p1 p2 p3 p4 p5 p6)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 p5 p6 0 0 0))
         ((p1 p2 p3 p4 p5 p6 p7)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 p5 p6 p7 0 0))
         ((p1 p2 p3 p4 p5 p6 p7 p8)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 p5 p6 p7 p8 0))
         ((p1 p2 p3 p4 p5 p6 p7 p8 p9)
          (proc (termstate-out state) dvar svar (termstate-baudrate state) 1
                p1 p2 p3 p4 p5 p6 p7 p8 p9)))))))

(define terminfo-colors
  (vector Black Red Green Brown Blue Magenta Cyan Gray
          DarkGray LightRed LightGreen Yellow LightBlue
          LightMagenta LightCyan White))

;; Debug printing of the console state. No redisplay optimization.
(define (debug-print-console c state)
  (define term-cursor_normal (tigetproc cursor_normal state))
  (define term-cursor_invisible (tigetproc cursor_invisible state))
  (define term-setrgbb (tigetproc 'setrgbb state))
  (define term-setrgbf (tigetproc 'setrgbf state))
  (define term-Tc? (tigetflag 'Tc state))
  (define term-RGB? (tigetflag 'RGB state))
  (define term-setaf (tigetproc set_a_foreground state))
  (define term-setab (tigetproc set_a_background state))
  (define term-cup (tigetproc cursor_address state))
  (define term-cud1 (tigetproc cursor_down state))
  (define term-cr (tigetproc carriage_return state))
  (define term-sgr (tigetproc set_attributes state))
  (define num-colors (tigetnum max_colors state))
  (define (set-foreground-color color)
    (unless (color-d color)
      (cond (term-RGB?
             ;; XXX: term-setaf and term-setab in xterm-direct have a
             ;; weird special case for p1 < 8.
             (if (< (color-rgb color) 8)
                 (term-setaf 0)
                 (term-setaf (color-rgb color))))
            (term-Tc?
             (term-setrgbf (color-r color) (color-g color) (color-b color)))
            (else
             ;; XXX: This could do an approximation, but should also
             ;; support 88- and 256-color palettes.
             (do ((i 0 (fx+ i 1)))
                 ((fx=? i (vector-length terminfo-colors)))
               (when (eqv? (vector-ref terminfo-colors i) color)
                 (cond ((< num-colors 16)
                        (unless (< i 8)
                          (ticall enter_bold_mode state))
                        (term-setaf (fxmod i 8)))
                       (else
                        (term-setaf i)))))))))
  (define (set-background-color color)
    (unless (color-d color)
      (cond (term-RGB?
             (if (< (color-rgb color) 8)
                 (term-setab 0)
                 (term-setab (color-rgb color))))
            (term-Tc?
             (term-setrgbb (color-r color) (color-g color) (color-b color)))
            (else
             (do ((i 0 (fx+ i 1)))
                 ((fx=? i (vector-length terminfo-colors)))
               (when (eqv? (vector-ref terminfo-colors i) color)
                 (cond ((< num-colors 16)
                        (term-setab (fxmod i 8)))
                       (else
                        (term-setab i)))))))))
  ;; Redraw the dirty rows of the screen
  ;; XXX: This is temporary until the redisplay algorithm is in place
  (term-cup 0 0)
  (do ((y 0 (fx+ y 1))) ((fx=? y (console-full-rows c)))
    (let-values ([(buf mbuf fgbuf bgbuf abuf idx) (%index/nowin c 0 y)])
      (when (console-row-dirty? c y 'absolute)
        (let lp ((x 0) (fgcolor^ #f) (bgcolor^ #f) (attr^ #f))
          (unless (fx=? x (console-full-cols c))
            (let ((ch (text-ref buf (fx+ idx x))))
              (cond
                ((textcell-unused? ch)
                 (lp (fx+ x 1) fgcolor^ bgcolor^ attr^))
                (else
                 (let ((fgcolor (fg-ref fgbuf (fx+ idx x)))
                       (bgcolor (bg-ref bgbuf (fx+ idx x)))
                       (attr (attr-ref abuf (fx+ idx x))))
                   ;; Change the colors and attributes if any differ
                   (unless (and (eqv? fgcolor fgcolor^)
                                (eqv? bgcolor bgcolor^)
                                (equal? attr attr^))
                     (letrec ((get (lambda (name)
                                     (cond ((assq name attr) => cdr)
                                           (else #f))))
                              (flag (lambda (name)
                                      (if (get name) 1 0))))
                       (term-sgr (flag 'standout)
                                 (flag 'underline)
                                 (flag 'reverse)
                                 (flag 'blink)
                                 (flag 'dim)
                                 (flag 'bold)
                                 (flag 'invisible))
                       (when (get 'italic)
                         (ticall enter_italics_mode state)))
                     (set-foreground-color fgcolor)
                     (set-background-color bgcolor))
                   (put-bytevector (termstate-out state)
                                   (string->utf8
                                    (call-with-string-output-port
                                      (lambda (p)
                                        (put-char p ch)
                                        (put-string p (mark-ref mbuf (fx+ idx x)))))))
                   (lp (fx+ x 1) fgcolor bgcolor attr))))))))
      (unless (eqv? y (fx- (console-full-rows c) 1))
        (term-cr)
        (term-cud1))))
  (clear-console-dirty! c)
  ;; Move the cursor and its attributes
  (cond
    ((assq 'visible (console-cursor-attr c)) =>
     (lambda (v)
       (if (cdr v)
           (term-cursor_normal)
           (term-cursor_invisible)))))
  (term-cup (fx+ (console-y c) (console-y1 c))
            (fx+ (console-x c) (console-x1 c))))

;; A backend for use with (text-info console)
(define (make-terminfo-backend state)
  (define inited #f)
  (define probe-phase 'send)
  (define (mouse-tracking-type)
    (let ((names (terminfo-db-terminal-names (termstate-ti state))))
      (cond ((and (fx>=? (string-length names) 4)
                  (equal? "rxvt" (substring names 0 4)))
             'rxvt)
            (else #f))))
  (define (init)
    (termios-raw-mode 0)
    (ticall meta_off state)
    (ticall enter_ca_mode state)
    (ticall exit_alt_charset_mode state)
    ;; (ticall change_scroll_region state)
    (ticall exit_insert_mode state)
    (ticall enter_am_mode state)
    (ticall keypad_local state)
    ;; http://invisible-island.net/xterm/xterm.faq.html#xterm_pc_style
    ;; http://invisible-island.net/xterm/xterm.faq.html#xterm_arrows
    (ticall keypad_xmit state)
    (when (tigetstr key_mouse state)
      ;; FIXME: Are these really not in terminfo? "XM"/"xm"?
      (put-bytevector (termstate-out state)
                      (string->utf8
                       (case (mouse-tracking-type)
                         ((rxvt) "\x1b;[?1001s\x1b;[?1002h\x1b;[?1005h")
                         (else   "\x1b;[?1001s\x1b;[?1002h\x1b;[?1006h")))))
    (set! inited #t))
  (define cols 80)
  (define rows 24)
  (define width 0)
  (define height 0)
  (define (resize)
    (guard (exn ((error? exn) #f))
      (let-values ([(cols^ rows^ width^ height^) (termios-get-window-size 0)])
        (set! cols cols^)
        (set! rows rows^)
        (set! width width^)
        (set! height height^)
        (make-resize-event cols^ rows^ width^ height^))))
  (resize)
  (lambda (cmd arg)
    (case cmd
      [(get-size)
       (values cols rows width height)]
      [(init)
       (init)]
      [(restore)
       (ticall exit_ca_mode state)
       (ticall exit_attribute_mode state)
       (ticall cursor_normal state)
       (ticall keypad_local state)
       (when (tigetstr key_mouse state)
         (put-bytevector (termstate-out state)
                         (string->utf8
                          (case (mouse-tracking-type)
                            ((rxvt) "\x1b;[?1005l\x1b;[?1002l\x1b;[?1001r")
                            (else   "\x1b;[?1006l\x1b;[?1002l\x1b;[?1001r")))))
       (flush-output-port (termstate-out state))
       (termios-canonical-mode 0)
       (set! inited #f)]
      [(update)
       (unless inited (init))
       ;; TODO: go through the redisplay algorithm
       (debug-print-console arg state)
       (flush-output-port (termstate-out state))]
      [(redraw)
       (unless inited (init))
       (ticall clear_screen state)
       (debug-print-console arg state)
       (flush-output-port (termstate-out state))]
      [(resize)
       ;; Probe for the size (works even across serial lines)
       (set! probe-phase 'send)
       (resize)]
      [(read-event)
       ;; Fun hack to probe for the terminal size
       (when (and (eq? probe-phase 'send)
                  (tigetstr user7 state) (tigetstr user6 state))
         ;; Move the cursor to the bottom left and ask where it is
         (ticall cursor_address state 999 999)
         (ticall user7 state)
         ;; Get primary attributes
         (ticall user9 state)
         (set! probe-phase 'await)
         (flush-output-port (termstate-out state)))
       (let lp ((read-more #f))
         (let ((input (if (or (equal? (termstate-keybuffer state) #vu8()) read-more)
                          (get-bytevector-some (termstate-in state))
                          #vu8())))
           (let lp* ()
             (let-values ([(partial? key code) (termstate-translate-input state input)])
               (cond
                 ((and (eqv? probe-phase 'await) (cursor-report-event? key))
                  (termstate-consume-input state (bytevector-length code))
                  (set! probe-phase #f)
                  (set! cols (fx+ (cursor-report-event-x key) 1))
                  (set! rows (fx+ (cursor-report-event-y key) 1))
                  ;; TODO: Only report if the size has changed. Can
                  ;; also every now and then to make sure the cursor
                  ;; is where it should be.
                  (make-resize-event cols rows width height))
                 (key
                  (when partial?
                    ;; FIXME: poll/sleep and if data is available then do (lp).
                    ;; This particular API is synchronous anyway.
                    #f)
                  (termstate-consume-input state (bytevector-length code))
                  (key->event key))
                 ((not partial?)
                  ;; Buffer starts with nonsense; skip some of it.
                  (termstate-consume-input state 1)
                  (lp #f))
                 (else
                  (lp #t)))))))]
      [(event-ready?)
       ;; TODO: key-ready? should do select() and read
       ;; available data, but still return #f if no full
       ;; key has been decoded
       #f]
      [else
       (error 'terminfo-backend "Bad command" cmd)]))))
