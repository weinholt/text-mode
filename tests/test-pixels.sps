#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs)
  (text-mode console)
  (text-mode pixels))

(clrscr)
(cursor-off)
(do ((i 0 (+ i 1)))
    ((= i 161))
  (print "Hello world! "))
(gotoxy 0 0)

(set-window 3 2 39 20)
(text-background Red)
(clrscr)

(let ((pm (make-pixmap 32 32)))
  (do ((i 0 (+ i 1)))
      ((= i 32))
    (pixmap-set! pm i i (hsv-color (* 360 (/ i 32)) 1 1))
    (pixmap-set! pm i (- 31 i) (hsv-color (* 360 (mod (+ 1/2 (/ i 32)) 1)) 1 1)))

  (gotoxy 2 1)
  (pixmap-draw-half pm)
  (redraw-screen)
  (read-key)

  (text-background Red)
  (clrscr)
  (gotoxy 2 1)
  (pixmap-draw-quad pm)
  (update-screen)
  (read-key)

  (text-background Red)
  (clrscr)
  (text-background Default)
  (gotoxy 2 1)
  (pixmap-draw-braille pm)
  (update-screen)
  (read-key))

(restore-console)
