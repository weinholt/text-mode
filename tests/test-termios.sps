#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (text-mode termios))

(test-begin "termios-stdin")

(termios-raw-mode 0)

(display "\\\n\\\n\\\n> There should be a slope to the left\r\n")
(display "This test is interactive. Press keys and quit with Ctrl-C:")
(flush-output-port (current-output-port))
(let lp ()
  (display "\r\n")
  (let ((b (get-u8 (standard-input-port))))
    (display b)
    (flush-output-port (current-output-port))
    (unless (eqv? b (fxand 31 (char->integer #\C)))
      (lp))))

(termios-canonical-mode 0)

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
