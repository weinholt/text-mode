#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs)
  (text-mode console))

(clrscr)
(gotoxy 0 0)
(cursor-on)
(with-window 0 0 (window-maxx) 1
  (lambda ()
    (text-color Black)
    (text-background Gray)
    (clreol)
    (print " ")
    (for-each (lambda (x)
                (let* ((x (symbol->string x))
                       (c0 (string-ref x 0))
                       (c* (substring x 1 (string-length x))))
                  (text-color Red)
                  (print c0)
                  (text-color Black)
                  (print c*)
                  (print "  ")))
              '(File  Edit  Search  Run  Compile  Debug  Tools  Options  Window  Help))))

(with-window 0 (- (window-maxy) 1) (window-maxx) (window-maxy)
  (lambda ()
    (clreol)
    (text-color Black)
    (text-background Gray)
    (print " ")
    (text-color Red)
    (print "F1")
    (text-color Black)
    (print " Help  ")
    (text-color Red)
    (print "F2")
    (text-color Black)
    (print " Save  ")
    (text-color Red)
    (print "x")
    (text-color Black)
    (print " Exit")))

;; Background
(with-window 0 1 (window-maxx) (- (window-maxy) 1)
  (lambda ()
    (cond (#t
           (text-color Black)
           (text-background (rgb-color #x8e8eaa))
           ;; (text-color Default)
           ;; (text-background Default)
           (clrscr))
          (else
           (text-color Blue)
           (text-background Gray)
           (print (make-string (* (window-maxy) (window-maxx)) #\░))))))

;; Color test pattern
(gotoxy 0 2)
(do ((maxx (window-maxx)) (colnum 0 (fx+ colnum 1))) ((fx=? colnum maxx))
  (text-background (hsv-color (* 360 (/ colnum maxx)) 1 1))
  (text-color (hsv-color (* 360 (mod (+ 1/2 (/ colnum maxx)) 1)) 1 1))
  (print (if (even? colnum) #\/ #\\)))

(text-background Blue)
(text-color Yellow)

(with-window 2 5 12 19
  (lambda ()
    (clrscr)
    (for-each println
              `(,(string-normalize-nfd "åäö12345")
                "ひらがな"
                "aeioucsz"
                "áéíóúčšž"
                "台北1234"
                "abcdefgh"
                "ＱＲＳ12"
                "abcdefgh"
                "ｱｲｳ12345"
                "abcdefgh"
                "ВкусБорщ"
                "\x0;\a\b\t\n\v\f\r"))))

(with-window 14 5 30 19
  (lambda ()
    (clrscr)
    (do ((i 0 (+ i 1)))
        ((= i 143))
      (print i)
      (print " "))))

(with-window 32 5 48 19
  (lambda ()
    (text-color Default)
    (text-background Default)
    (clrscr)
    (for-each (lambda (attr)
                (text-attribute attr #t)
                (println attr)
                (text-attribute attr #f))
              `(standout underline reverse blink dim bold invisible italic))))

(gotoxy (window-maxx) (window-maxy))

(redraw-screen)
(let lp ()
  (unless (eqv? #\x (read-key))
    (lp)))

(restore-console)
