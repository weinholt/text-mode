#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (text-mode redisplay))

(test-begin "redisplay-trace")

(define-record-type matrix
  (fields m x y)
  (nongenerative matrix-v0-5f97f264-63b9-45b2-8903-58adb2a6a940))

;; Test the example from section 3 of "A Redisplay Algorithm".
(let ()
  (define (compute-trace/line-update A B)
    (define (Cd _i) 0)
    (define (Ci _j) 1)
    (define (Ct i j)
      (if (char=? (string-ref A i) (string-ref B j)) 0 +inf.0))
    (compute-trace (string-length A)
                   (string-length B)
                   Ct Cd Ci))
  (let-values ([(_W D)
                (compute-trace/line-update "abcbst" "bctsbt")])
    (test-equal (matrix-m D)
                '#(#(   0   +inf.0 +inf.0 +inf.0 +inf.0 +inf.0 +inf.0 +inf.0)
                   #(+inf.0    0      0      0      0      0      0      0)
                   #(+inf.0    1      1      0      0      0      0      0)
                   #(+inf.0    2      2      1      0      0      0      0)
                   #(+inf.0    3      3      2      1      1      1      0)
                   #(+inf.0    4      4      3      2      2      1      1)
                   #(+inf.0    5      5      4      3      2      2      2)
                   #(+inf.0    6      6      5      4      3      3      2)))))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
