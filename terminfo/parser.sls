;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2010-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Terminfo parser

;; Reads and works with compiled terminfo files in ncurses' format
;; (output from tic).

;; The latest terminal database is located at:
;; http://invisible-island.net/ncurses/

;; The format of compiled terminfo files is described here:
;; http://invisible-island.net/ncurses/man/term.5.html
;; Also useful is terminfo(5).

(library (text-mode terminfo parser)
  (export
    make-terminfo-db terminfo-db?
    terminfo-db-terminal-names
    terminfo-db-booleans
    terminfo-db-numbers
    terminfo-db-strings
    terminfo-db-ext-booleans
    terminfo-db-ext-numbers
    terminfo-db-ext-strings
    get-compiled-ncurses-terminfo
    parse-term-string                   ;string tokenizer
    terminfo-expression                 ;string compiler
    terminfo-raw-procedure)             ;string -> procedure
  (import
    (rnrs (6))
    (rnrs eval)
    (struct pack)
    (text-mode terminfo constants))

(define (trace . x)
  ;; (for-each display x)
  ;; (newline)
  #f)

(define-record-type terminfo-db
  (nongenerative terminfo-db-v0-a06490d4-9a4d-4438-a00b-ab50ead72d7b)
  (fields terminal-names
          booleans                     ;indicates supported features
          numbers                      ;screen size, delays, etc
          strings                      ;sequences that perform actions
          ext-booleans                 ;as above, except eq hashtables
          ext-numbers
          ext-strings))

;;; Helpers

(define (bytevector-append . bvs)
  (call-with-bytevector-output-port
    (lambda (p) (for-each (lambda (bv) (put-bytevector p bv)) bvs))))

(define (subbytevector bv start end)
  (let ((ret (make-bytevector (- end start))))
    (bytevector-copy! bv start
                      ret 0 (- end start))
    ret))

(define bytevector-u8-index
  (case-lambda
    ((bv c start end)
     (let lp ((i start))
       (cond ((fx=? end i) #f)
             ((fx=? (bytevector-u8-ref bv i) c) i)
             (else (lp (fx+ i 1))))))
    ((bv c start)
     (bytevector-u8-index bv c start (bytevector-length bv)))))

(define (zip* xs ys)
  (if (or (null? xs) (null? ys))
      '()
      (cons (list (car xs) (car ys))
            (zip* (cdr xs) (cdr ys)))))

(define (list->vector/len list len default)
  ;; XXX: hmm, why does "ansi" have more bool entries than Caps?
  (let ((v (make-vector (max (length list) len) default)))
    (do ((i 0 (+ i 1))
         (list list (cdr list)))
        ((null? list) v)
      (vector-set! v i (car list)))))

;;; File parsing

(define (port-align p)
  (when (odd? (port-position p))
    (get-u8 p)))

(define (copy-string buf offset)
  (and (>= offset 0)
       (subbytevector buf offset
                      (or (bytevector-u8-index buf #x00 offset)
                          (bytevector-length buf)))))

(define (get-booleans p n)
  (let ((buf (get-bytevector-n p n)))
    (map fxpositive? (bytevector->u8-list buf))))

(define (get-numbers p n wordsize)
  (let ((buf (get-bytevector-n p (fx* n wordsize))))
    (bytevector->sint-list buf (endianness little) wordsize)))

(define (get-strings p offsets buflen)
  (let ((buf (get-bytevector-n p buflen)))
    (map (lambda (offset) (copy-string buf offset)) offsets)))

(define (get-compiled-ncurses-terminfo* p wordsize)
  (define (get-terminals p n)
    (let ((buf (get-bytevector-n p (- n 1))))
      (get-u8 p)                      ;skip nul
      (utf8->string buf)))
  (let-values ([(tlen blen shorts offsets slen) (get-unpack p "<5s")])
    (let* ((terminals (get-terminals p tlen))
           (booleans (get-booleans p blen)))
      (port-align p)
      (let* ((numbers (get-numbers p shorts wordsize))
             (strings (get-strings p (get-numbers p offsets 2) slen)))
        (values terminals
                (list->vector/len booleans (vector-length terminfo-booleans) #f)
                (list->vector/len numbers (vector-length terminfo-numbers) -1)
                (list->vector/len strings (vector-length terminfo-strings) #f))))))

(define (get-extended-ncurses-terminfo* p wordsize)
  (define (find-name-start table string-offsets)
    ;; The table starts with strings and after these come the names
    ;; for everything. Returns the offset of the first name.
    (let ((last-offset (fold-left max -1 string-offsets)))
      (cond ((negative? last-offset) 0)
            ((bytevector-u8-index table #x00 last-offset)
             => (lambda (idx) (fx+ idx 1)))
            (else 0))))
  (port-align p)
  (let*-values ([(nbooleans nnumerics nstrings table-items table-bytes)
                 (get-unpack p "<5s")]
                [(booleans) (get-booleans p nbooleans)])
    (port-align p)
    (let* ((numbers (get-numbers p nnumerics wordsize))
           (string-offsets (get-numbers p nstrings 2))
           (name-offsets (get-numbers p (+ nbooleans nnumerics nstrings) 2)))
      (let* ((table (get-bytevector-n p table-bytes))
             (split (find-name-start table string-offsets))
             (strings (map (lambda (offset) (copy-string table offset))
                           string-offsets))
             (names (map (lambda (offset)
                           (string->symbol
                            (utf8->string (copy-string table offset))))
                         (map (lambda (offset) (+ offset split)) name-offsets))))
        ;; names is for booleans, numbers and strings (in that order).
        (let ((ext-booleans (make-eq-hashtable))
              (ext-numbers (make-eq-hashtable))
              (ext-strings (make-eq-hashtable)))
          (define (setter ht)
            (lambda (v) (lambda (name) (hashtable-set! ht name v))))
          (for-each (lambda (setter name) (setter name))
                    (append (map (setter ext-booleans) booleans)
                            (map (setter ext-numbers) numbers)
                            (map (setter ext-strings) strings))
                    names)
          (values ext-booleans ext-numbers ext-strings))))))

(define (get-compiled-ncurses-terminfo p)
  (let* ((magic (get-unpack p "<s"))
         (wordsize (case magic
                     ((#o432) 2)
                     ((#d542) 4)
                     (else #f))))
    (unless wordsize
      (error 'get-compiled-ncurses-terminfo "Not a terminfo file (bad magic)" magic))
    (let-values ([(terminals booleans numbers strings)
                  (get-compiled-ncurses-terminfo* p wordsize)])
      (cond ((port-eof? p)
             (make-terminfo-db terminals booleans numbers strings
                               (make-eq-hashtable) (make-eq-hashtable)
                               (make-eq-hashtable)))
            (else
             (let-values ([(ext-booleans ext-numbers ext-strings)
                           (get-extended-ncurses-terminfo* p wordsize)])
               (make-terminfo-db terminals booleans numbers strings
                                 ext-booleans ext-numbers ext-strings)))))))

;;; Parse the term strings

(define (parse-printf-format p)
  ;; Parse as much of %[[:]flags][width[.precision]][doxXs] as there
  ;; is. The flags, width and precision are consumed even if the
  ;; command is actually something other than a printf.
  (let ((right-pad #f)                ;padding is on the right
        (show-sign #f)                ;show sign with #\- or #\space
        (show-base #f)                ;show base with 0x or 0
        (pad-char #\space)            ;padding character
        (width #f)                    ;min width of output
        (precision #f)                ;min # of digits to print
        (format-char #\nul))          ;[cdoxXsu]
    (define (reconstruct-fmt)
      ;; Reconstruct the parsed format (for debugging).
      (call-with-string-output-port
        (lambda (p)
          (put-char p #\%)
          (when right-pad (put-char p #\-))
          (when show-sign (put-char p #\space))
          (when show-base (put-char p #\#))
          (unless (char=? pad-char #\space)
            (put-char p pad-char))
          (when width     (display width p))
          (when precision
            (when width   (put-char p #\.))
            (display precision p))
          (put-char p format-char))))
    (let ((allow-minus #f) (error #f) (value #f))
      (let loop ()
        (let ((u8 (lookahead-u8 p)))
          (unless (eof-object? u8)
            (case (integer->char u8)
              ;; XXX: [cu] will not be picked up as printf
              ((#\c #\d #\o #\x #\X #\s #\u)
               (set! format-char (integer->char u8)))
              ((#\:)
               (get-u8 p)
               (set! allow-minus #t)
               (loop))
              ((#\-)
               (when allow-minus
                 (get-u8 p)
                 (set! right-pad #t)
                 (loop)))
              ((#\#)
               (get-u8 p)
               ;; Print the base if non-zero and non-decimal (0x, 0)
               (set! show-base #t)
               (loop))
              ((#\space)
               ;; Always show the sign with either #\- or #\space
               (get-u8 p)
               (set! show-sign #t)
               (loop))
              ((#\.)
               (get-u8 p)
               (cond ((not width)
                      (set! width (or value 0))
                      (set! pad-char #\space)
                      (set! value #f))
                     (else
                      (set! error #t)))
               (loop))
              ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
               (get-u8 p)
               (when (and (eqv? u8 (char->integer #\0))
                          (not value)
                          (not width))
                 (set! pad-char #\0))
               (set! value (+ (* (or value 0) 10) (- u8 (char->integer #\0))))
               (when (> value 10000)
                 (set! error #t))
               (loop))
              ;; XXX: + is document but not implemented
              (else #f)))))
      (when error
        (set! pad-char #\space)
        (set! right-pad #f)
        (set! width #f)
        (set! show-base #f))
      (if width
          (set! precision (or value 0))
          (set! width value))
      (when (and right-pad (eqv? pad-char #\0))
        (set! pad-char #\space))
      (list (reconstruct-fmt) right-pad show-sign show-base pad-char
            width precision format-char))))

(define (parse-term-string bv)
  (define (get-until p . c)
    (let ((bytes (map char->integer c)))
      (call-with-bytevector-output-port
        (lambda (o)
          (let lp ()
            (let ((c (lookahead-u8 p)))
              (unless (or (eof-object? c) (memv c bytes))
                (put-u8 o (get-u8 p))
                (lp))))))))
  ;; Lexing of the stack machine operations
  (define (get-op p)
    (cond ((port-eof? p) (eof-object))
          ((= (lookahead-u8 p) (char->integer #\$))
           ;; XXX: An argument can be made for delaying this parsing
           ;; and instead doing it on the output from the program.
           (get-u8 p)                   ;eat $
           (cond ((not (eqv? (lookahead-u8 p) (char->integer #\<)))
                  (list 'print #vu8(#x24))) ;$
                 (else
                  (get-u8 p)                   ;eat <
                  (let ((delay (get-until p #\>)))
                    (get-u8 p)                 ;eat >
                    ;; FIXME: parse the suffixes #\* and #\/ and the time
                    (let ((*-with-affected #f)
                          (mandatory? #f))
                      (list 'msleep delay *-with-affected mandatory?))))))
          ((= (lookahead-u8 p) (char->integer #\%))
           ;; Command
           (get-u8 p)                   ;eat %
           (let* ((printf-args (parse-printf-format p))
                  (cmd (get-u8 p)))
             (trace "parsing command: " cmd ", " (and (fixnum? cmd) (integer->char cmd)))
             (case (and (fixnum? cmd) (integer->char cmd))
               ((#\%) `(print ,(string->utf8 "%")))
               ((#\d #\o #\x #\X #\s)
                (cons 'printf printf-args))
               ((#\c) '(print-char))
               ((#\u) '(print-utf8))
               ((#\P #\g)
                (let* ((var (integer->char (get-u8 p)))
                       (varname (string->symbol (string (char-downcase var)))))
                  (list (if (eqv? cmd (char->integer #\g)) 'get 'set!)
                        (if (char-upper-case? var) 'static 'dynamic)
                        varname)))
               ((#\p)
                (let ((i (get-u8 p)))
                  (if (fx<=? (char->integer #\1) i (char->integer #\9))
                      (list 'parameter (fx- i (char->integer #\0)))
                      ;; XXX: Warning?
                      '(push 0))))
               ((#\')
                (let ((c (get-u8 p)))
                  (get-u8 p)            ;eat '
                  (list 'push c)))
               ((#\{)
                (let lp ((n 0) (ch (get-u8 p)))
                  (if (or (eof-object? ch)
                          (not (<= (char->integer #\0) ch (char->integer #\9))))
                      `(push ,n)
                      (lp (+ (* n 10) (- ch (char->integer #\0)))
                          (get-u8 p)))))
               ((#\?) '(if))
               ((#\t) '(then))
               ((#\e) '(else))
               ((#\;) '(endif))
               ((#\l) '(string-length))
               ((#\+) '(+))
               ((#\-) '(-))
               ((#\*) '(*))
               ((#\/) '(quotient))
               ((#\m) '(remainder))
               ((#\|) '(bitwise-ior))
               ((#\&) '(bitwise-and))
               ((#\^) '(bitwise-xor))
               ((#\=) '(=))
               ((#\>) '(>))
               ((#\<) '(<))
               ((#\A) '(and))
               ((#\O) '(or))
               ((#\!) '(not))
               ((#\~) '(bitwise-not))
               ((#\i) '(inc-p1/p2))
               (else `(nop ,cmd)))))
          (else
           ;; Literal output
           (list 'print (get-until p #\% #\$)))))
  (let ((p (open-bytevector-input-port bv)))
    (let lp ((tokens '()))
      (let ((cmd (get-op p)))
        ;; (display "CMD: ")
        ;; (write cmd)
        ;; (newline)
        (if (eof-object? cmd)
            (reverse tokens)
            (lp (cons cmd tokens)))))))

;;; Compilation

(define (get-stack-depth tokens)
  (let lp ((tokens tokens) (mindepth 0) (maxdepth 0) (depth 0))
    (if (null? tokens)
        (values mindepth maxdepth depth)
        (let-values ([(d0 d1)
                      ;; Get stack deltas for (at most) two phases of
                      ;; the operation.
                      (case (caar tokens)
                        ((print) (values 0 0))
                        ((printf) (values 0 -1))
                        ((parameter get push) (values 0 1))
                        ((set!) (values 0 -1))
                        ((msleep nop) (values 9 0))
                        ((if else endif) (values 0 0))
                        ((then) (values 0 -1))
                        ((print-char print-utf8) (values 0 -1))
                        ((string-length) (values -1 1))
                        ((+ - * quotient remainder bitwise-ior bitwise-and
                            bitwise-xor = > < and or not bitwise-not)
                         (values -2 1))
                        ((inc-p1/p2) (values 0 0))
                        (else
                         (error 'get-stack-depth "Unimplemented op" (caar tokens))))])
          (let* ((depth0 (+ depth d0))
                 (depth1 (+ depth0 d1)))
            (lp (cdr tokens)
                (fxmin mindepth depth0 depth1)
                (fxmax maxdepth depth0 depth1)
                depth1))))))

(define (terminfo-expression* tokens)
  (define (no-stack-effect? tokens)
    ;; Nothing currently on the stack is used in "tokens" and they
    ;; don't leave anything on the stack.
    (let-values ([(mindepth _maxdepth depth) (get-stack-depth tokens)])
      (and (eqv? mindepth 0) (eqv? depth 0))))
  (define termcap-compat
    (let-values ([(mindepth maxdepth _depth) (get-stack-depth tokens)])
      ;; Activates compatibility with termcap strings.
      (and (zero? maxdepth) (- mindepth))))
  (define stack                         ;unique names for all stack slots
    (let-values ([(mindepth maxdepth _depth) (get-stack-depth tokens)])
      (do ((slots (if (zero? maxdepth) (- mindepth) maxdepth))
           (i 0 (fx+ i 1))
           (s* '() (cons (string->symbol (string-append "s" (number->string i))) s*)))
          ((fx=? i slots)
           (reverse s*)))))
  (define (params-mutated? tokens) (memq 'inc-p1/p2 tokens))
  (define parameters '#(#f p1^ p2^ p3 p4 p5 p6 p7 p8 p9))
  (define lambdaargs (if (params-mutated? tokens) `(p1 p2 ,@stack) stack))
  (define (wrap expr)
    ;; This is the start of the returned code. p is the output port,
    ;; p1-p9 is the parameters/arguments. dvar/svar are the
    ;; dynamic/static variables (as hashtables) sN/s* is the stack,
    ;; with part being explicit as variables.
    `(lambda (p dvar svar baudrate lines p1 p2 p3 p4 p5 p6 p7 p8 p9)
       (define (k . x) (if #f #f))
       (let ((p1^ p1) (p2^ p2)
             ,@(map (lambda (sN) `(,sN 0)) stack))
         (let (,@(if termcap-compat     ;push the arguments
                     (do ((arg* (cdr (vector->list parameters))
                                (cdr arg*))
                          (s* (reverse stack) (cdr s*))
                          (ret '()
                               (cons (list (car s*)
                                           (let ((v (car arg*)))
                                             `(if (string? ,v) 0 ,v)))
                                     ret)))
                         ((or (null? s*) (null? arg*)) ret))
                     '()))
           (,expr ,@lambdaargs)))
       (if #f #f)))
  (define (let-pop var . body*)
    `(let (,@(zip* (cons var stack) stack))
       (let (#;(,var (if (string? ,var) 0 ,var))
             )
         ,@body*)))
  (define (let-pop* var . body*)
    `(let (,@(zip* (cons var stack) stack))
       ,@body*))
  (define (let-push var . body*)
    `(let (,@(zip* stack (cons var stack)))
       ,@body*))
  (define (parse-if tokens)
    ;; Starting with an 'if/'else token, find the next 'else. Returns
    ;; three values: the 'then branch, the 'else branch, and the code
    ;; after the endif. Could likely be written in a simpler way.
    (define (remtail list tail)
      (if (equal? list tail)
          '()
          (cons (car list) (remtail (cdr list) tail))))
    (define (factor-tails t-code e-code)
      (let lp ((t-tail t-code))
        (cond ((null? t-tail)
               (values t-code e-code '()))
              (else
               (let lp* ((e-tail e-code))
                 (cond ((null? e-tail) (lp (cdr t-tail)))
                       ((equal? t-tail e-tail)
                        (values (remtail t-code t-tail)
                                (remtail e-code t-tail)
                                t-tail))
                       (else (lp* (cdr e-tail)))))))))
    (let lp ((tokens (cdr tokens)) (conseq '()) (level 0) (in-then? #f))
      (if (null? tokens)
          (factor-tails (reverse conseq) tokens)
          (let* ((token (car tokens))
                 (cmd (car token)))
            (cond
              ((and (eqv? level 0) (memq cmd '(else endif)))
               ;; tokens now holds the matching else clause. Let's
               ;; remove all the code that belongs to the else clause
               ;; and append it to conseq.
               (let lp* ((rest tokens) (level 0))
                 (if (null? rest)
                     (factor-tails (reverse conseq) tokens)
                     (let ((cmd (caar rest)))
                       (cond
                         ((eq? cmd 'endif)
                          (if (eqv? level 0)
                              (factor-tails (append (reverse conseq) (cdr rest)) tokens)
                              (lp* (cdr rest) (fx- level 1))))
                         ((eq? cmd 'if)
                          (lp* (cdr rest) (fx+ level 1)))
                         (else
                          (lp* (cdr rest) level)))))))
              ((eq? cmd 'endif)
               (lp (cdr tokens) (cons token conseq) (fx- level 1) in-then?))
              ((eq? cmd 'if)
               (lp (cdr tokens) (cons token conseq) (fx+ level 1) in-then?))
              ((and (eqv? level 0) (eq? cmd 'then))
               (lp (cdr tokens) conseq level #t))
              (else
               (lp (cdr tokens) (if in-then? (cons token conseq) conseq) level in-then?)))))))
  (wrap
   (let lp ((tokens tokens) (ifs '()))
     (if (null? tokens)
         'k
         (let* ((token (car tokens))
                (cmd (car token))
                (args (cdr token)))
           (case cmd
             ((parameter)
              (let ((idx (car args)))
                `(lambda ,lambdaargs
                   ,(let-push (vector-ref parameters idx)
                              `(,(lp (cdr tokens) ifs) ,@lambdaargs)))))
             ((push)
              (let ((v (car args)))
                `(lambda ,lambdaargs
                   ,(let-push v
                              `(,(lp (cdr tokens) ifs) ,@lambdaargs)))))
             ((print)
              `(lambda ,lambdaargs
                 ,(if (eqv? 1 (bytevector-length (car args)))
                      `(put-u8 p ,(bytevector-u8-ref (car args) 0))
                      `(put-bytevector p ,(car args)))
                 (,(lp (cdr tokens) ifs) ,@lambdaargs)))
             ((print-char)
              `(lambda ,lambdaargs
                 ,(let-pop 'v
                    `(put-u8 p (let ((n (bitwise-and v #xff)))
                                 ;; ncurses sends #x80, hoping
                                 ;; it will be interpreted as
                                 ;; #\nul anyway.
                                 #;(if (eqv? n 0) #x80 n)
                                 n))
                    `(,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((print-utf8)
              `(lambda ,lambdaargs
                 ,(let-pop 'v
                    `(when (and (fixnum? v)
                                (fx<=? #x0000 v #x10FFFF)
                                (not (fx<=? #xD800 v #xDFFF)))
                       (put-bytevector p (string->utf8
                                          (string (integer->char v)))))
                    `(,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((printf)
              `(lambda ,lambdaargs
                 ,(let-pop* 'v
                    `(ti-printf p ,(car args) v ,@(cdr args))
                    `(,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((get)
              `(lambda ,lambdaargs
                 ,(let-push `(hashtable-ref ,(case (car args)
                                               ((static) 'svar)
                                               (else 'dvar))
                                            ',(caddr token)
                                            0)
                            `(,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((set!)
              `(lambda ,lambdaargs
                 ,(let-pop* 'v
                    `(hashtable-set! ,(case (car args)
                                        ((static) 'svar)
                                        (else 'dvar))
                                     ',(caddr token)
                                     v)
                    `(,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((if)
              (lp (cdr tokens) (cons tokens ifs)))
             ((then)
              (cond
                ((pair? ifs)
                 (let-values ([(conseq altern tail) (parse-if (car ifs))])
                   (if (and (not (params-mutated? conseq))
                            (not (params-mutated? altern))
                            (no-stack-effect? conseq)
                            (no-stack-effect? altern))
                       `(lambda ,lambdaargs
                          ,(let-pop 'v
                             `(if (not (eqv? v 0))
                                  (,(lp conseq ifs) ,@lambdaargs)
                                  (,(lp altern ifs) ,@lambdaargs)))
                          (,(lp tail ifs) ,@lambdaargs))
                       `(lambda ,lambdaargs
                          (let ((k (lambda ,lambdaargs
                                     (,(lp tail ifs) ,@lambdaargs))))
                            ,(let-pop 'v
                               `(if (not (eqv? v 0))
                                    (,(lp conseq ifs) ,@lambdaargs)
                                    (,(lp altern ifs) ,@lambdaargs))))))))
                (else
                 ;; Bad syntax...
                 (trace "warning: bad if\n")
                 (lp (cdr tokens) ifs))))
             ((else)
              ;; Replace the current 'if with the 'else
              (cond ((pair? ifs)
                     (lp (cdr tokens) (cons tokens (cdr ifs))))
                    (else
                     (trace "warning: bad else\n")
                     (lp (cdr tokens) (cons tokens ifs)))))
             ((endif)
              (when (null? ifs)
                (trace "warning: bad endif\n"))
              (lp (cdr tokens) (if (pair? ifs) (cdr ifs) '())))
             ((> < = bitwise-ior bitwise-and bitwise-xor - * +
                 quotient remainder and or not bitwise-not)
              `(lambda ,lambdaargs
                 ,((if (memq cmd '(bitwise-not not)) (lambda (_id expr) expr) let-pop) 'v1
                   (let-pop 'v0
                     (cond
                       ((and (pair? ifs) (pair? tokens) (pair? (cdr tokens))
                             (equal? '(then) (cadr tokens))
                             (memq cmd '(> < =)))
                        ;; Direct compilation of test + then
                        (let-values ([(conseq altern tail) (parse-if (car ifs))])
                          (if (and (not (params-mutated? conseq))
                                   (not (params-mutated? altern))
                                   (no-stack-effect? conseq)
                                   (no-stack-effect? altern))
                              `(let ()
                                 (if (,cmd v0 v1)
                                     (,(lp conseq ifs) ,@lambdaargs)
                                     (,(lp altern ifs) ,@lambdaargs))
                                 (,(lp tail ifs) ,@lambdaargs))
                              `(let ((k (lambda ,lambdaargs
                                          (,(lp tail ifs) ,@lambdaargs))))
                                 (if (,cmd v0 v1)
                                     (,(lp conseq ifs) ,@lambdaargs)
                                     (,(lp altern ifs) ,@lambdaargs))))))
                       (else
                        ;; Regular pushing of the result
                        `(let ([v ,(case cmd
                                     ((> < =) `(if (,cmd v0 v1) 1 0))
                                     ((and) '(if (and (not (eqv? 0 v0)) (not (eqv? 0 v1))) 1 0))
                                     ((or) '(if (or (not (eqv? 0 v0)) (not (eqv? 0 v1))) 1 0))
                                     ;; XXX: do these match C?
                                     ((quotient) '(if (eqv? v1 0) 0 (quotient v0 v1)))
                                     ((remainder) '(if (eqv? v1 0) 0 (remainder v0 v1)))
                                     ((bitwise-not) '(bitwise-not v0))
                                     ((not) '(if (eqv? v0 0) 1 0))
                                     (else `(,cmd v0 v1)))])
                           ,(let-push 'v
                                      `(,(lp (cdr tokens) ifs) ,@lambdaargs)))))))))
             ((string-length)
              `(lambda ,lambdaargs
                 ,(let-pop* 'v
                    (let-push '(cond ((string? v)
                                      (bytevector-length (string->utf8 v)))
                                     ((bytevector? v)
                                      (bytevector-length v))
                                     (else 0))
                              `(,(lp (cdr tokens) ifs) ,@lambdaargs)))))
             ((inc-p1/p2)
              `(lambda ,lambdaargs
                 (let ((p1^ (if (string? p1) p1 (+ p1 1)))
                       (p2^ (if (string? p2) p2 (+ p2 1))))
                   (let (,@(if termcap-compat
                               '((s0 p2^) (s1 p1^))
                               '()))
                     (,(lp (cdr tokens) ifs) ,@lambdaargs)))))
             ((msleep)
              (let ((delay (car args))
                    (*-with-affected? (cadr args))
                    (mandatory? (caddr args)))
                `(lambda ,lambdaargs
                   (msleep p ,(if *-with-affected? '(* lines delay) delay) ,mandatory? baudrate)
                   (,(lp (cdr tokens) ifs) ,@lambdaargs))))
             ((nop) (lp (cdr tokens) ifs))
             (else
              (error 'terminfo-expression "Unimplemented op" token))))))))

(define (terminfo-expression term-string)
  (let ((bv (if (bytevector? term-string) term-string (string->utf8 term-string))))
    (terminfo-expression* (parse-term-string bv))))

;; Expose a parameterized string as a procedure.
(define terminfo-raw-procedure
  (let ((env #f))
    (lambda (term-string)
      (unless env
        (set! env (environment '(rnrs)
                               '(rnrs r5rs)
                               '(text-mode terminfo builtins))))
      (if term-string
          (eval (terminfo-expression term-string) env)
          (lambda _ #f))))))
