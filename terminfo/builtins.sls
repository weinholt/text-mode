;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Terminfo builtins available from terminfo parameterized strings

(library (text-mode terminfo builtins)
  (export
    ti-printf
    msleep)
  (import
    (rnrs (6)))

;; Prints the (single!) value v to the port p according to the
;; printf(3) style format string fmt. Returns the position of the
;; character that ends the format.
(define (ti-printf p fmt v right-pad show-sign show-base
                   pad-char width precision format-char)
  (define put
    (if (textual-port? p)
        (lambda (x) (display x p))
        (lambda (x) (put-bytevector p (string->utf8 x)))))
  (assert (memv format-char '(#\d #\o #\x #\X #\s)))
  (let* ((v (case format-char
              ((#\d) (if (string? v) 0 v))
              ((#\o #\x #\X) (if (string? v) 0 (bitwise-and v #xFFFFFFFF)))
              (else (if (string? v) v ""))))
         ;; Just (abs v) as a string
         (unpadded
          (if (and (eqv? v 0) (eqv? precision 0))
              ""
              (case format-char
                ((#\d) (number->string (abs v) 10))
                ((#\o) (number->string (abs v) 8))
                ((#\x) (string-downcase (number->string (abs v) 16)))
                ((#\X) (string-upcase (number->string (abs v) 16)))
                (else v))))
         ;; The minimum number of digits to show
         (unpadded
          (if (and precision (number? v))
              (let* ((unpadded-len (string-length unpadded))
                     (len (max 0 (- precision unpadded-len))))
                (string-append (make-string len #\0) unpadded))
              unpadded))
         ;; Make the base of the number explicit.
         (base-chars
          (if (and show-base (not (eqv? v 0)))
              (case format-char
                ((#\o)
                 (if (and (> (string-length unpadded) 0)
                          (char=? (string-ref unpadded 0) #\0))
                     ""
                     "0"))
                ((#\x) "0x")
                ((#\X) "0X")
                (else ""))
              ""))
         (is-negative (and (number? v) (negative? v)))
         ;; Put the sign left or right of the padding
         (sign-left (or (and is-negative (eqv? pad-char #\0))
                        (and show-sign (eqv? pad-char #\0))))
         (sign-right (and is-negative (eqv? pad-char #\space)))
         (padding
          ;; The width provided by the non-padding characters
          (let ((unpadded-len (+ (string-length unpadded)
                                 (string-length base-chars)
                                 (if (or sign-left sign-right)
                                     1
                                     0))))
            (if (and width (< unpadded-len width))
                (make-string (- width unpadded-len) pad-char)
                ""))))
    (when sign-left (put (if is-negative "-" " ")))
    (when (not right-pad)
      (put padding))
    (when sign-right (put (if is-negative "-" " ")))
    (put base-chars)
    (put unpadded)
    (when right-pad
      (put padding))))

(define (msleep p delay mandatory? baudrate)
  ;; TODO: Print #\nul to create the delay
  #f))
