;; -*- mode: scheme; coding: utf-8 -*-
;; ##############################################################################
;; # Copyright (c) 1998-2015,2016 Free Software Foundation, Inc.                #
;; #                                                                            #
;; # Permission is hereby granted, free of charge, to any person obtaining a    #
;; # copy of this software and associated documentation files (the "Software"), #
;; # to deal in the Software without restriction, including without limitation  #
;; # the rights to use, copy, modify, merge, publish, distribute, distribute    #
;; # with modifications, sublicense, and/or sell copies of the Software, and to #
;; # permit persons to whom the Software is furnished to do so, subject to the  #
;; # following conditions:                                                      #
;; #                                                                            #
;; # The above copyright notice and this permission notice shall be included in #
;; # all copies or substantial portions of the Software.                        #
;; #                                                                            #
;; # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
;; # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
;; # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
;; # THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      #
;; # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
;; # FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
;; # DEALINGS IN THE SOFTWARE.                                                  #
;; #                                                                            #
;; # Except as contained in this notice, the name(s) of the above copyright     #
;; # holders shall not be used in advertising or otherwise to promote the sale, #
;; # use or other dealings in this Software without prior written               #
;; # authorization.                                                             #
;; ##############################################################################
#!r6rs

#| Terminfo API constants extracted from
extern/Caps (copied from the ncurses distribution) by
tools/parse-caps.sps |#

(library (text-mode terminfo constants)
  (export
    terminfo-strings
    terminfo-booleans
    terminfo-numbers
    terminfo-keys
    auto_left_margin
    auto_right_margin
    no_esc_ctlc
    ceol_standout_glitch
    eat_newline_glitch
    erase_overstrike
    generic_type
    hard_copy
    has_meta_key
    has_status_line
    insert_null_glitch
    memory_above
    memory_below
    move_insert_mode
    move_standout_mode
    over_strike
    status_line_esc_ok
    dest_tabs_magic_smso
    tilde_glitch
    transparent_underline
    xon_xoff
    needs_xon_xoff
    prtr_silent
    hard_cursor
    non_rev_rmcup
    no_pad_char
    non_dest_scroll_region
    can_change
    back_color_erase
    hue_lightness_saturation
    col_addr_glitch
    cr_cancels_micro_mode
    has_print_wheel
    row_addr_glitch
    semi_auto_right_margin
    cpi_changes_res
    lpi_changes_res
    backspaces_with_bs
    crt_no_scrolling
    no_correctly_working_cr
    gnu_has_meta_key
    linefeed_is_newline
    has_hardware_tabs
    return_does_clr_eol
    columns
    init_tabs
    lines
    lines_of_memory
    magic_cookie_glitch
    padding_baud_rate
    virtual_terminal
    width_status_line
    num_labels
    label_height
    label_width
    max_attributes
    maximum_windows
    max_colors
    max_pairs
    no_color_video
    buffer_capacity
    dot_vert_spacing
    dot_horz_spacing
    max_micro_address
    max_micro_jump
    micro_col_size
    micro_line_size
    number_of_pins
    output_res_char
    output_res_line
    output_res_horz_inch
    output_res_vert_inch
    print_rate
    wide_char_size
    buttons
    bit_image_entwining
    bit_image_type
    magic_cookie_glitch_ul
    carriage_return_delay
    new_line_delay
    backspace_delay
    horizontal_tab_delay
    number_of_function_keys
    back_tab
    bell
    carriage_return
    change_scroll_region
    clear_all_tabs
    clear_screen
    clr_eol
    clr_eos
    column_address
    command_character
    cursor_address
    cursor_down
    cursor_home
    cursor_invisible
    cursor_left
    cursor_mem_address
    cursor_normal
    cursor_right
    cursor_to_ll
    cursor_up
    cursor_visible
    delete_character
    delete_line
    dis_status_line
    down_half_line
    enter_alt_charset_mode
    enter_blink_mode
    enter_bold_mode
    enter_ca_mode
    enter_delete_mode
    enter_dim_mode
    enter_insert_mode
    enter_secure_mode
    enter_protected_mode
    enter_reverse_mode
    enter_standout_mode
    enter_underline_mode
    erase_chars
    exit_alt_charset_mode
    exit_attribute_mode
    exit_ca_mode
    exit_delete_mode
    exit_insert_mode
    exit_standout_mode
    exit_underline_mode
    flash_screen
    form_feed
    from_status_line
    init_1string
    init_2string
    init_3string
    init_file
    insert_character
    insert_line
    insert_padding
    key_backspace
    key_catab
    key_clear
    key_ctab
    key_dc
    key_dl
    key_down
    key_eic
    key_eol
    key_eos
    key_f0
    key_f1
    key_f10
    key_f2
    key_f3
    key_f4
    key_f5
    key_f6
    key_f7
    key_f8
    key_f9
    key_home
    key_ic
    key_il
    key_left
    key_ll
    key_npage
    key_ppage
    key_right
    key_sf
    key_sr
    key_stab
    key_up
    keypad_local
    keypad_xmit
    lab_f0
    lab_f1
    lab_f10
    lab_f2
    lab_f3
    lab_f4
    lab_f5
    lab_f6
    lab_f7
    lab_f8
    lab_f9
    meta_off
    meta_on
    linefeed
    pad_char
    parm_dch
    parm_delete_line
    parm_down_cursor
    parm_ich
    parm_index
    parm_insert_line
    parm_left_cursor
    parm_right_cursor
    parm_rindex
    parm_up_cursor
    pkey_key
    pkey_local
    pkey_xmit
    print_screen
    prtr_off
    prtr_on
    repeat_char
    reset_1string
    reset_2string
    reset_3string
    reset_file
    restore_cursor
    row_address
    save_cursor
    scroll_forward
    scroll_reverse
    set_attributes
    set_tab
    set_window
    tab
    to_status_line
    underline_char
    up_half_line
    init_prog
    key_a1
    key_a3
    key_b2
    key_c1
    key_c3
    prtr_non
    char_padding
    acs_chars
    plab_norm
    key_btab
    enter_xon_mode
    exit_xon_mode
    enter_am_mode
    exit_am_mode
    xon_character
    xoff_character
    ena_acs
    label_on
    label_off
    key_beg
    key_cancel
    key_close
    key_command
    key_copy
    key_create
    key_end
    key_enter
    key_exit
    key_find
    key_help
    key_mark
    key_message
    key_move
    key_next
    key_open
    key_options
    key_previous
    key_print
    key_redo
    key_reference
    key_refresh
    key_replace
    key_restart
    key_resume
    key_save
    key_suspend
    key_undo
    key_sbeg
    key_scancel
    key_scommand
    key_scopy
    key_screate
    key_sdc
    key_sdl
    key_select
    key_send
    key_seol
    key_sexit
    key_sfind
    key_shelp
    key_shome
    key_sic
    key_sleft
    key_smessage
    key_smove
    key_snext
    key_soptions
    key_sprevious
    key_sprint
    key_sredo
    key_sreplace
    key_sright
    key_srsume
    key_ssave
    key_ssuspend
    key_sundo
    req_for_input
    key_f11
    key_f12
    key_f13
    key_f14
    key_f15
    key_f16
    key_f17
    key_f18
    key_f19
    key_f20
    key_f21
    key_f22
    key_f23
    key_f24
    key_f25
    key_f26
    key_f27
    key_f28
    key_f29
    key_f30
    key_f31
    key_f32
    key_f33
    key_f34
    key_f35
    key_f36
    key_f37
    key_f38
    key_f39
    key_f40
    key_f41
    key_f42
    key_f43
    key_f44
    key_f45
    key_f46
    key_f47
    key_f48
    key_f49
    key_f50
    key_f51
    key_f52
    key_f53
    key_f54
    key_f55
    key_f56
    key_f57
    key_f58
    key_f59
    key_f60
    key_f61
    key_f62
    key_f63
    clr_bol
    clear_margins
    set_left_margin
    set_right_margin
    label_format
    set_clock
    display_clock
    remove_clock
    create_window
    goto_window
    hangup
    dial_phone
    quick_dial
    tone
    pulse
    flash_hook
    fixed_pause
    wait_tone
    user0
    user1
    user2
    user3
    user4
    user5
    user6
    user7
    user8
    user9
    orig_pair
    orig_colors
    initialize_color
    initialize_pair
    set_color_pair
    set_foreground
    set_background
    change_char_pitch
    change_line_pitch
    change_res_horz
    change_res_vert
    define_char
    enter_doublewide_mode
    enter_draft_quality
    enter_italics_mode
    enter_leftward_mode
    enter_micro_mode
    enter_near_letter_quality
    enter_normal_quality
    enter_shadow_mode
    enter_subscript_mode
    enter_superscript_mode
    enter_upward_mode
    exit_doublewide_mode
    exit_italics_mode
    exit_leftward_mode
    exit_micro_mode
    exit_shadow_mode
    exit_subscript_mode
    exit_superscript_mode
    exit_upward_mode
    micro_column_address
    micro_down
    micro_left
    micro_right
    micro_row_address
    micro_up
    order_of_pins
    parm_down_micro
    parm_left_micro
    parm_right_micro
    parm_up_micro
    select_char_set
    set_bottom_margin
    set_bottom_margin_parm
    set_left_margin_parm
    set_right_margin_parm
    set_top_margin
    set_top_margin_parm
    start_bit_image
    start_char_set_def
    stop_bit_image
    stop_char_set_def
    subscript_characters
    superscript_characters
    these_cause_cr
    zero_motion
    char_set_names
    key_mouse
    mouse_info
    req_mouse_pos
    get_mouse
    set_a_foreground
    set_a_background
    pkey_plab
    device_type
    code_set_init
    set0_des_seq
    set1_des_seq
    set2_des_seq
    set3_des_seq
    set_lr_margin
    set_tb_margin
    bit_image_repeat
    bit_image_newline
    bit_image_carriage_return
    color_names
    define_bit_image_region
    end_bit_image_region
    set_color_band
    set_page_length
    display_pc_char
    enter_pc_charset_mode
    exit_pc_charset_mode
    enter_scancode_mode
    exit_scancode_mode
    pc_term_options
    scancode_escape
    alt_scancode_esc
    enter_horizontal_hl_mode
    enter_left_hl_mode
    enter_low_hl_mode
    enter_right_hl_mode
    enter_top_hl_mode
    enter_vertical_hl_mode
    set_a_attributes
    set_pglen_inch
    termcap_init2
    termcap_reset
    linefeed_if_not_lf
    backspace_if_not_bs
    other_non_function_keys
    arrow_key_map
    acs_ulcorner
    acs_llcorner
    acs_urcorner
    acs_lrcorner
    acs_ltee
    acs_rtee
    acs_btee
    acs_ttee
    acs_hline
    acs_vline
    acs_plus
    memory_lock
    memory_unlock
    box_chars_1)
  (import (only (rnrs) define quote))

(define terminfo-booleans
  '#(bw am xsb xhp xenl eo gn hc km hs in da db mir msgr os eslok
     xt hz ul xon nxon mc5i chts nrrmc npc ndscr ccc bce hls xhpa
     crxm daisy xvpa sam cpix lpix OTbs OTns OTnc OTMT OTNL OTpt
     OTxr))

(define terminfo-numbers
  '#(cols it lines lm xmc pb vt wsl nlab lh lw ma wnum colors
     pairs ncv bufsz spinv spinh maddr mjump mcs mls npins orc
     orl orhi orvi cps widcs btns bitwin bitype OTug OTdC OTdN
     OTdB OTdT OTkn))

(define terminfo-strings
  '#(cbt bel cr csr tbc clear el ed hpa cmdch cup cud1 home civis
     cub1 mrcup cnorm cuf1 ll cuu1 cvvis dch1 dl1 dsl hd smacs
     blink bold smcup smdc dim smir invis prot rev smso smul ech
     rmacs sgr0 rmcup rmdc rmir rmso rmul flash ff fsl is1 is2
     is3 if ich1 il1 ip kbs ktbc kclr kctab kdch1 kdl1 kcud1
     krmir kel ked kf0 kf1 kf10 kf2 kf3 kf4 kf5 kf6 kf7 kf8 kf9
     khome kich1 kil1 kcub1 kll knp kpp kcuf1 kind kri khts kcuu1
     rmkx smkx lf0 lf1 lf10 lf2 lf3 lf4 lf5 lf6 lf7 lf8 lf9 rmm
     smm nel pad dch dl cud ich indn il cub cuf rin cuu pfkey
     pfloc pfx mc0 mc4 mc5 rep rs1 rs2 rs3 rf rc vpa sc ind ri
     sgr hts wind ht tsl uc hu iprog ka1 ka3 kb2 kc1 kc3 mc5p rmp
     acsc pln kcbt smxon rmxon smam rmam xonc xoffc enacs smln
     rmln kbeg kcan kclo kcmd kcpy kcrt kend kent kext kfnd khlp
     kmrk kmsg kmov knxt kopn kopt kprv kprt krdo kref krfr krpl
     krst kres ksav kspd kund kBEG kCAN kCMD kCPY kCRT kDC kDL
     kslt kEND kEOL kEXT kFND kHLP kHOM kIC kLFT kMSG kMOV kNXT
     kOPT kPRV kPRT kRDO kRPL kRIT kRES kSAV kSPD kUND rfi kf11
     kf12 kf13 kf14 kf15 kf16 kf17 kf18 kf19 kf20 kf21 kf22 kf23
     kf24 kf25 kf26 kf27 kf28 kf29 kf30 kf31 kf32 kf33 kf34 kf35
     kf36 kf37 kf38 kf39 kf40 kf41 kf42 kf43 kf44 kf45 kf46 kf47
     kf48 kf49 kf50 kf51 kf52 kf53 kf54 kf55 kf56 kf57 kf58 kf59
     kf60 kf61 kf62 kf63 el1 mgc smgl smgr fln sclk dclk rmclk
     cwin wingo hup dial qdial tone pulse hook pause wait u0 u1
     u2 u3 u4 u5 u6 u7 u8 u9 op oc initc initp scp setf setb cpi
     lpi chr cvr defc swidm sdrfq sitm slm smicm snlq snrmq sshm
     ssubm ssupm sum rwidm ritm rlm rmicm rshm rsubm rsupm rum
     mhpa mcud1 mcub1 mcuf1 mvpa mcuu1 porder mcud mcub mcuf mcuu
     scs smgb smgbp smglp smgrp smgt smgtp sbim scsd rbim rcsd
     subcs supcs docr zerom csnm kmous minfo reqmp getm setaf
     setab pfxl devt csin s0ds s1ds s2ds s3ds smglr smgtb birep
     binel bicr colornm defbi endbi setcolor slines dispc smpch
     rmpch smsc rmsc pctrm scesc scesa ehhlm elhlm elohlm erhlm
     ethlm evhlm sgr1 slength OTi2 OTrs OTnl OTbc OTko OTma OTG2
     OTG3 OTG1 OTG4 OTGR OTGL OTGU OTGD OTGH OTGV OTGC meml memu
     box1))

(define terminfo-keys
  '#(#f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f KEY_BACKSPACE
     KEY_CATAB KEY_CLEAR KEY_CTAB KEY_DC KEY_DL KEY_DOWN KEY_EIC
     KEY_EOL KEY_EOS KEY_F0 KEY_F1 KEY_F10 KEY_F2 KEY_F3 KEY_F4
     KEY_F5 KEY_F6 KEY_F7 KEY_F8 KEY_F9 KEY_HOME KEY_IC KEY_IL
     KEY_LEFT KEY_LL KEY_NPAGE KEY_PPAGE KEY_RIGHT KEY_SF KEY_SR
     KEY_STAB KEY_UP #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f KEY_A1
     KEY_A3 KEY_B2 KEY_C1 KEY_C3 #f #f #f #f KEY_BTAB #f #f #f #f
     #f #f #f #f #f KEY_BEG KEY_CANCEL KEY_CLOSE KEY_COMMAND
     KEY_COPY KEY_CREATE KEY_END KEY_ENTER KEY_EXIT KEY_FIND
     KEY_HELP KEY_MARK KEY_MESSAGE KEY_MOVE KEY_NEXT KEY_OPEN
     KEY_OPTIONS KEY_PREVIOUS KEY_PRINT KEY_REDO KEY_REFERENCE
     KEY_REFRESH KEY_REPLACE KEY_RESTART KEY_RESUME KEY_SAVE
     KEY_SUSPEND KEY_UNDO KEY_SBEG KEY_SCANCEL KEY_SCOMMAND
     KEY_SCOPY KEY_SCREATE KEY_SDC KEY_SDL KEY_SELECT KEY_SEND
     KEY_SEOL KEY_SEXIT KEY_SFIND KEY_SHELP KEY_SHOME KEY_SIC
     KEY_SLEFT KEY_SMESSAGE KEY_SMOVE KEY_SNEXT KEY_SOPTIONS
     KEY_SPREVIOUS KEY_SPRINT KEY_SREDO KEY_SREPLACE KEY_SRIGHT
     KEY_SRSUME KEY_SSAVE KEY_SSUSPEND KEY_SUNDO #f KEY_F11
     KEY_F12 KEY_F13 KEY_F14 KEY_F15 KEY_F16 KEY_F17 KEY_F18
     KEY_F19 KEY_F20 KEY_F21 KEY_F22 KEY_F23 KEY_F24 KEY_F25
     KEY_F26 KEY_F27 KEY_F28 KEY_F29 KEY_F30 KEY_F31 KEY_F32
     KEY_F33 KEY_F34 KEY_F35 KEY_F36 KEY_F37 KEY_F38 KEY_F39
     KEY_F40 KEY_F41 KEY_F42 KEY_F43 KEY_F44 KEY_F45 KEY_F46
     KEY_F47 KEY_F48 KEY_F49 KEY_F50 KEY_F51 KEY_F52 KEY_F53
     KEY_F54 KEY_F55 KEY_F56 KEY_F57 KEY_F58 KEY_F59 KEY_F60
     KEY_F61 KEY_F62 KEY_F63 #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f KEY_MOUSE #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f
     #f #f #f #f #f #f #f #f #f #f #f #f #f #f #f))

;; bw: cub1 wraps from column 0 to last column
(define auto_left_margin '(boolean . 0))

;; am: terminal has automatic margins
(define auto_right_margin '(boolean . 1))

;; xsb: beehive (f1=escape, f2=ctrl C)
(define no_esc_ctlc '(boolean . 2))

;; xhp: standout not erased by overwriting (hp)
(define ceol_standout_glitch '(boolean . 3))

;; xenl: newline ignored after 80 cols (concept)
(define eat_newline_glitch '(boolean . 4))

;; eo: can erase overstrikes with a blank
(define erase_overstrike '(boolean . 5))

;; gn: generic line type
(define generic_type '(boolean . 6))

;; hc: hardcopy terminal
(define hard_copy '(boolean . 7))

;; km: Has a meta key (i.e., sets 8th-bit)
(define has_meta_key '(boolean . 8))

;; hs: has extra status line
(define has_status_line '(boolean . 9))

;; in: insert mode distinguishes nulls
(define insert_null_glitch '(boolean . 10))

;; da: display may be retained above the screen
(define memory_above '(boolean . 11))

;; db: display may be retained below the screen
(define memory_below '(boolean . 12))

;; mir: safe to move while in insert mode
(define move_insert_mode '(boolean . 13))

;; msgr: safe to move while in standout mode
(define move_standout_mode '(boolean . 14))

;; os: terminal can overstrike
(define over_strike '(boolean . 15))

;; eslok: escape can be used on the status line
(define status_line_esc_ok '(boolean . 16))

;; xt: tabs destructive, magic so char (t1061)
(define dest_tabs_magic_smso '(boolean . 17))

;; hz: cannot print ~'s (Hazeltine)
(define tilde_glitch '(boolean . 18))

;; ul: underline character overstrikes
(define transparent_underline '(boolean . 19))

;; xon: terminal uses xon/xoff handshaking
(define xon_xoff '(boolean . 20))

;; nxon: padding will not work, xon/xoff required
(define needs_xon_xoff '(boolean . 21))

;; mc5i: printer will not echo on screen
(define prtr_silent '(boolean . 22))

;; chts: cursor is hard to see
(define hard_cursor '(boolean . 23))

;; nrrmc: smcup does not reverse rmcup
(define non_rev_rmcup '(boolean . 24))

;; npc: pad character does not exist
(define no_pad_char '(boolean . 25))

;; ndscr: scrolling region is non-destructive
(define non_dest_scroll_region '(boolean . 26))

;; ccc: terminal can re-define existing colors
(define can_change '(boolean . 27))

;; bce: screen erased with background color
(define back_color_erase '(boolean . 28))

;; hls: terminal uses only HLS color notation (Tektronix)
(define hue_lightness_saturation '(boolean . 29))

;; xhpa: only positive motion for hpa/mhpa caps
(define col_addr_glitch '(boolean . 30))

;; crxm: using cr turns off micro mode
(define cr_cancels_micro_mode '(boolean . 31))

;; daisy: printer needs operator to change character set
(define has_print_wheel '(boolean . 32))

;; xvpa: only positive motion for vpa/mvpa caps
(define row_addr_glitch '(boolean . 33))

;; sam: printing in last column causes cr
(define semi_auto_right_margin '(boolean . 34))

;; cpix: changing character pitch changes resolution
(define cpi_changes_res '(boolean . 35))

;; lpix: changing line pitch changes resolution
(define lpi_changes_res '(boolean . 36))

;; OTbs: uses ^H to move left
(define backspaces_with_bs '(boolean . 37))

;; OTns: crt cannot scroll
(define crt_no_scrolling '(boolean . 38))

;; OTnc: no way to go to start of line
(define no_correctly_working_cr '(boolean . 39))

;; OTMT: has meta key
(define gnu_has_meta_key '(boolean . 40))

;; OTNL: move down with \n
(define linefeed_is_newline '(boolean . 41))

;; OTpt: has 8-char tabs invoked with ^I
(define has_hardware_tabs '(boolean . 42))

;; OTxr: return clears the line
(define return_does_clr_eol '(boolean . 43))

;; cols: number of columns in a line
(define columns '(number . 0))

;; it: tabs initially every # spaces
(define init_tabs '(number . 1))

;; lines: number of lines on screen or page
(define lines '(number . 2))

;; lm: lines of memory if > line. 0 means varies
(define lines_of_memory '(number . 3))

;; xmc: number of blank characters left by smso or rmso
(define magic_cookie_glitch '(number . 4))

;; pb: lowest baud rate where padding needed
(define padding_baud_rate '(number . 5))

;; vt: virtual terminal number (CB/unix)
(define virtual_terminal '(number . 6))

;; wsl: number of columns in status line
(define width_status_line '(number . 7))

;; nlab: number of labels on screen
(define num_labels '(number . 8))

;; lh: rows in each label
(define label_height '(number . 9))

;; lw: columns in each label
(define label_width '(number . 10))

;; ma: maximum combined attributes terminal can handle
(define max_attributes '(number . 11))

;; wnum: maximum number of definable windows
(define maximum_windows '(number . 12))

;; colors: maximum number of colors on screen
(define max_colors '(number . 13))

;; pairs: maximum number of color-pairs on the screen
(define max_pairs '(number . 14))

;; ncv: video attributes that cannot be used with colors
(define no_color_video '(number . 15))

;; bufsz: numbers of bytes buffered before printing
(define buffer_capacity '(number . 16))

;; spinv: spacing of pins vertically in pins per inch
(define dot_vert_spacing '(number . 17))

;; spinh: spacing of dots horizontally in dots per inch
(define dot_horz_spacing '(number . 18))

;; maddr: maximum value in micro_..._address
(define max_micro_address '(number . 19))

;; mjump: maximum value in parm_..._micro
(define max_micro_jump '(number . 20))

;; mcs: character step size when in micro mode
(define micro_col_size '(number . 21))

;; mls: line step size when in micro mode
(define micro_line_size '(number . 22))

;; npins: numbers of pins in print-head
(define number_of_pins '(number . 23))

;; orc: horizontal resolution in units per line
(define output_res_char '(number . 24))

;; orl: vertical resolution in units per line
(define output_res_line '(number . 25))

;; orhi: horizontal resolution in units per inch
(define output_res_horz_inch '(number . 26))

;; orvi: vertical resolution in units per inch
(define output_res_vert_inch '(number . 27))

;; cps: print rate in characters per second
(define print_rate '(number . 28))

;; widcs: character step size when in double wide mode
(define wide_char_size '(number . 29))

;; btns: number of buttons on mouse
(define buttons '(number . 30))

;; bitwin: number of passes for each bit-image row
(define bit_image_entwining '(number . 31))

;; bitype: type of bit-image device
(define bit_image_type '(number . 32))

;; OTug: number of blanks left by ul
(define magic_cookie_glitch_ul '(number . 33))

;; OTdC: pad needed for CR
(define carriage_return_delay '(number . 34))

;; OTdN: pad needed for LF
(define new_line_delay '(number . 35))

;; OTdB: padding required for ^H
(define backspace_delay '(number . 36))

;; OTdT: padding required for ^I
(define horizontal_tab_delay '(number . 37))

;; OTkn: count of function keys
(define number_of_function_keys '(number . 38))

;; cbt: back tab (P)
(define back_tab '(string . 0))

;; bel: audible signal (bell) (P)
(define bell '(string . 1))

;; cr: carriage return (P*) (P*)
(define carriage_return '(string . 2))

;; csr: change region to line #1 to line #2 (P)
(define change_scroll_region '(string . 3))

;; tbc: clear all tab stops (P)
(define clear_all_tabs '(string . 4))

;; clear: clear screen and home cursor (P*)
(define clear_screen '(string . 5))

;; el: clear to end of line (P)
(define clr_eol '(string . 6))

;; ed: clear to end of screen (P*)
(define clr_eos '(string . 7))

;; hpa: horizontal position #1, absolute (P)
(define column_address '(string . 8))

;; cmdch: terminal settable cmd character in prototype !?
(define command_character '(string . 9))

;; cup: move to row #1 columns #2
(define cursor_address '(string . 10))

;; cud1: down one line
(define cursor_down '(string . 11))

;; home: home cursor (if no cup)
(define cursor_home '(string . 12))

;; civis: make cursor invisible
(define cursor_invisible '(string . 13))

;; cub1: move left one space
(define cursor_left '(string . 14))

;; mrcup: memory relative cursor addressing, move to row #1 columns #2
(define cursor_mem_address '(string . 15))

;; cnorm: make cursor appear normal (undo civis/cvvis)
(define cursor_normal '(string . 16))

;; cuf1: non-destructive space (move right one space)
(define cursor_right '(string . 17))

;; ll: last line, first column (if no cup)
(define cursor_to_ll '(string . 18))

;; cuu1: up one line
(define cursor_up '(string . 19))

;; cvvis: make cursor very visible
(define cursor_visible '(string . 20))

;; dch1: delete character (P*)
(define delete_character '(string . 21))

;; dl1: delete line (P*)
(define delete_line '(string . 22))

;; dsl: disable status line
(define dis_status_line '(string . 23))

;; hd: half a line down
(define down_half_line '(string . 24))

;; smacs: start alternate character set (P)
(define enter_alt_charset_mode '(string . 25))

;; blink: turn on blinking
(define enter_blink_mode '(string . 26))

;; bold: turn on bold (extra bright) mode
(define enter_bold_mode '(string . 27))

;; smcup: string to start programs using cup
(define enter_ca_mode '(string . 28))

;; smdc: enter delete mode
(define enter_delete_mode '(string . 29))

;; dim: turn on half-bright mode
(define enter_dim_mode '(string . 30))

;; smir: enter insert mode
(define enter_insert_mode '(string . 31))

;; invis: turn on blank mode (characters invisible)
(define enter_secure_mode '(string . 32))

;; prot: turn on protected mode
(define enter_protected_mode '(string . 33))

;; rev: turn on reverse video mode
(define enter_reverse_mode '(string . 34))

;; smso: begin standout mode
(define enter_standout_mode '(string . 35))

;; smul: begin underline mode
(define enter_underline_mode '(string . 36))

;; ech: erase #1 characters (P)
(define erase_chars '(string . 37))

;; rmacs: end alternate character set (P)
(define exit_alt_charset_mode '(string . 38))

;; sgr0: turn off all attributes
(define exit_attribute_mode '(string . 39))

;; rmcup: strings to end programs using cup
(define exit_ca_mode '(string . 40))

;; rmdc: end delete mode
(define exit_delete_mode '(string . 41))

;; rmir: exit insert mode
(define exit_insert_mode '(string . 42))

;; rmso: exit standout mode
(define exit_standout_mode '(string . 43))

;; rmul: exit underline mode
(define exit_underline_mode '(string . 44))

;; flash: visible bell (may not move cursor)
(define flash_screen '(string . 45))

;; ff: hardcopy terminal page eject (P*)
(define form_feed '(string . 46))

;; fsl: return from status line
(define from_status_line '(string . 47))

;; is1: initialization string
(define init_1string '(string . 48))

;; is2: initialization string
(define init_2string '(string . 49))

;; is3: initialization string
(define init_3string '(string . 50))

;; if: name of initialization file
(define init_file '(string . 51))

;; ich1: insert character (P)
(define insert_character '(string . 52))

;; il1: insert line (P*)
(define insert_line '(string . 53))

;; ip: insert padding after inserted character
(define insert_padding '(string . 54))

;; kbs: backspace key
(define key_backspace '(string . 55))

;; ktbc: clear-all-tabs key
(define key_catab '(string . 56))

;; kclr: clear-screen or erase key
(define key_clear '(string . 57))

;; kctab: clear-tab key
(define key_ctab '(string . 58))

;; kdch1: delete-character key
(define key_dc '(string . 59))

;; kdl1: delete-line key
(define key_dl '(string . 60))

;; kcud1: down-arrow key
(define key_down '(string . 61))

;; krmir: sent by rmir or smir in insert mode
(define key_eic '(string . 62))

;; kel: clear-to-end-of-line key
(define key_eol '(string . 63))

;; ked: clear-to-end-of-screen key
(define key_eos '(string . 64))

;; kf0: F0 function key
(define key_f0 '(string . 65))

;; kf1: F1 function key
(define key_f1 '(string . 66))

;; kf10: F10 function key
(define key_f10 '(string . 67))

;; kf2: F2 function key
(define key_f2 '(string . 68))

;; kf3: F3 function key
(define key_f3 '(string . 69))

;; kf4: F4 function key
(define key_f4 '(string . 70))

;; kf5: F5 function key
(define key_f5 '(string . 71))

;; kf6: F6 function key
(define key_f6 '(string . 72))

;; kf7: F7 function key
(define key_f7 '(string . 73))

;; kf8: F8 function key
(define key_f8 '(string . 74))

;; kf9: F9 function key
(define key_f9 '(string . 75))

;; khome: home key
(define key_home '(string . 76))

;; kich1: insert-character key
(define key_ic '(string . 77))

;; kil1: insert-line key
(define key_il '(string . 78))

;; kcub1: left-arrow key
(define key_left '(string . 79))

;; kll: lower-left key (home down)
(define key_ll '(string . 80))

;; knp: next-page key
(define key_npage '(string . 81))

;; kpp: previous-page key
(define key_ppage '(string . 82))

;; kcuf1: right-arrow key
(define key_right '(string . 83))

;; kind: scroll-forward key
(define key_sf '(string . 84))

;; kri: scroll-backward key
(define key_sr '(string . 85))

;; khts: set-tab key
(define key_stab '(string . 86))

;; kcuu1: up-arrow key
(define key_up '(string . 87))

;; rmkx: leave 'keyboard_transmit' mode
(define keypad_local '(string . 88))

;; smkx: enter 'keyboard_transmit' mode
(define keypad_xmit '(string . 89))

;; lf0: label on function key f0 if not f0
(define lab_f0 '(string . 90))

;; lf1: label on function key f1 if not f1
(define lab_f1 '(string . 91))

;; lf10: label on function key f10 if not f10
(define lab_f10 '(string . 92))

;; lf2: label on function key f2 if not f2
(define lab_f2 '(string . 93))

;; lf3: label on function key f3 if not f3
(define lab_f3 '(string . 94))

;; lf4: label on function key f4 if not f4
(define lab_f4 '(string . 95))

;; lf5: label on function key f5 if not f5
(define lab_f5 '(string . 96))

;; lf6: label on function key f6 if not f6
(define lab_f6 '(string . 97))

;; lf7: label on function key f7 if not f7
(define lab_f7 '(string . 98))

;; lf8: label on function key f8 if not f8
(define lab_f8 '(string . 99))

;; lf9: label on function key f9 if not f9
(define lab_f9 '(string . 100))

;; rmm: turn off meta mode
(define meta_off '(string . 101))

;; smm: turn on meta mode (8th-bit on)
(define meta_on '(string . 102))

;; nel: newline (behave like cr followed by lf)
(define linefeed '(string . 103))

;; pad: padding char (instead of null)
(define pad_char '(string . 104))

;; dch: delete #1 characters (P*)
(define parm_dch '(string . 105))

;; dl: delete #1 lines (P*)
(define parm_delete_line '(string . 106))

;; cud: down #1 lines (P*)
(define parm_down_cursor '(string . 107))

;; ich: insert #1 characters (P*)
(define parm_ich '(string . 108))

;; indn: scroll forward #1 lines (P)
(define parm_index '(string . 109))

;; il: insert #1 lines (P*)
(define parm_insert_line '(string . 110))

;; cub: move #1 characters to the left (P)
(define parm_left_cursor '(string . 111))

;; cuf: move #1 characters to the right (P*)
(define parm_right_cursor '(string . 112))

;; rin: scroll back #1 lines (P)
(define parm_rindex '(string . 113))

;; cuu: up #1 lines (P*)
(define parm_up_cursor '(string . 114))

;; pfkey: program function key #1 to type string #2
(define pkey_key '(string . 115))

;; pfloc: program function key #1 to execute string #2
(define pkey_local '(string . 116))

;; pfx: program function key #1 to transmit string #2
(define pkey_xmit '(string . 117))

;; mc0: print contents of screen
(define print_screen '(string . 118))

;; mc4: turn off printer
(define prtr_off '(string . 119))

;; mc5: turn on printer
(define prtr_on '(string . 120))

;; rep: repeat char #1 #2 times (P*)
(define repeat_char '(string . 121))

;; rs1: reset string
(define reset_1string '(string . 122))

;; rs2: reset string
(define reset_2string '(string . 123))

;; rs3: reset string
(define reset_3string '(string . 124))

;; rf: name of reset file
(define reset_file '(string . 125))

;; rc: restore cursor to position of last save_cursor
(define restore_cursor '(string . 126))

;; vpa: vertical position #1 absolute (P)
(define row_address '(string . 127))

;; sc: save current cursor position (P)
(define save_cursor '(string . 128))

;; ind: scroll text up (P)
(define scroll_forward '(string . 129))

;; ri: scroll text down (P)
(define scroll_reverse '(string . 130))

;; sgr: define video attributes #1-#9 (PG9)
(define set_attributes '(string . 131))

;; hts: set a tab in every row, current columns
(define set_tab '(string . 132))

;; wind: current window is lines #1-#2 cols #3-#4
(define set_window '(string . 133))

;; ht: tab to next 8-space hardware tab stop
(define tab '(string . 134))

;; tsl: move to status line, column #1
(define to_status_line '(string . 135))

;; uc: underline char and move past it
(define underline_char '(string . 136))

;; hu: half a line up
(define up_half_line '(string . 137))

;; iprog: path name of program for initialization
(define init_prog '(string . 138))

;; ka1: upper left of keypad
(define key_a1 '(string . 139))

;; ka3: upper right of keypad
(define key_a3 '(string . 140))

;; kb2: center of keypad
(define key_b2 '(string . 141))

;; kc1: lower left of keypad
(define key_c1 '(string . 142))

;; kc3: lower right of keypad
(define key_c3 '(string . 143))

;; mc5p: turn on printer for #1 bytes
(define prtr_non '(string . 144))

;; rmp: like ip but when in insert mode
(define char_padding '(string . 145))

;; acsc: graphics charset pairs, based on vt100
(define acs_chars '(string . 146))

;; pln: program label #1 to show string #2
(define plab_norm '(string . 147))

;; kcbt: back-tab key
(define key_btab '(string . 148))

;; smxon: turn on xon/xoff handshaking
(define enter_xon_mode '(string . 149))

;; rmxon: turn off xon/xoff handshaking
(define exit_xon_mode '(string . 150))

;; smam: turn on automatic margins
(define enter_am_mode '(string . 151))

;; rmam: turn off automatic margins
(define exit_am_mode '(string . 152))

;; xonc: XON character
(define xon_character '(string . 153))

;; xoffc: XOFF character
(define xoff_character '(string . 154))

;; enacs: enable alternate char set
(define ena_acs '(string . 155))

;; smln: turn on soft labels
(define label_on '(string . 156))

;; rmln: turn off soft labels
(define label_off '(string . 157))

;; kbeg: begin key
(define key_beg '(string . 158))

;; kcan: cancel key
(define key_cancel '(string . 159))

;; kclo: close key
(define key_close '(string . 160))

;; kcmd: command key
(define key_command '(string . 161))

;; kcpy: copy key
(define key_copy '(string . 162))

;; kcrt: create key
(define key_create '(string . 163))

;; kend: end key
(define key_end '(string . 164))

;; kent: enter/send key
(define key_enter '(string . 165))

;; kext: exit key
(define key_exit '(string . 166))

;; kfnd: find key
(define key_find '(string . 167))

;; khlp: help key
(define key_help '(string . 168))

;; kmrk: mark key
(define key_mark '(string . 169))

;; kmsg: message key
(define key_message '(string . 170))

;; kmov: move key
(define key_move '(string . 171))

;; knxt: next key
(define key_next '(string . 172))

;; kopn: open key
(define key_open '(string . 173))

;; kopt: options key
(define key_options '(string . 174))

;; kprv: previous key
(define key_previous '(string . 175))

;; kprt: print key
(define key_print '(string . 176))

;; krdo: redo key
(define key_redo '(string . 177))

;; kref: reference key
(define key_reference '(string . 178))

;; krfr: refresh key
(define key_refresh '(string . 179))

;; krpl: replace key
(define key_replace '(string . 180))

;; krst: restart key
(define key_restart '(string . 181))

;; kres: resume key
(define key_resume '(string . 182))

;; ksav: save key
(define key_save '(string . 183))

;; kspd: suspend key
(define key_suspend '(string . 184))

;; kund: undo key
(define key_undo '(string . 185))

;; kBEG: shifted begin key
(define key_sbeg '(string . 186))

;; kCAN: shifted cancel key
(define key_scancel '(string . 187))

;; kCMD: shifted command key
(define key_scommand '(string . 188))

;; kCPY: shifted copy key
(define key_scopy '(string . 189))

;; kCRT: shifted create key
(define key_screate '(string . 190))

;; kDC: shifted delete-character key
(define key_sdc '(string . 191))

;; kDL: shifted delete-line key
(define key_sdl '(string . 192))

;; kslt: select key
(define key_select '(string . 193))

;; kEND: shifted end key
(define key_send '(string . 194))

;; kEOL: shifted clear-to-end-of-line key
(define key_seol '(string . 195))

;; kEXT: shifted exit key
(define key_sexit '(string . 196))

;; kFND: shifted find key
(define key_sfind '(string . 197))

;; kHLP: shifted help key
(define key_shelp '(string . 198))

;; kHOM: shifted home key
(define key_shome '(string . 199))

;; kIC: shifted insert-character key
(define key_sic '(string . 200))

;; kLFT: shifted left-arrow key
(define key_sleft '(string . 201))

;; kMSG: shifted message key
(define key_smessage '(string . 202))

;; kMOV: shifted move key
(define key_smove '(string . 203))

;; kNXT: shifted next key
(define key_snext '(string . 204))

;; kOPT: shifted options key
(define key_soptions '(string . 205))

;; kPRV: shifted previous key
(define key_sprevious '(string . 206))

;; kPRT: shifted print key
(define key_sprint '(string . 207))

;; kRDO: shifted redo key
(define key_sredo '(string . 208))

;; kRPL: shifted replace key
(define key_sreplace '(string . 209))

;; kRIT: shifted right-arrow key
(define key_sright '(string . 210))

;; kRES: shifted resume key
(define key_srsume '(string . 211))

;; kSAV: shifted save key
(define key_ssave '(string . 212))

;; kSPD: shifted suspend key
(define key_ssuspend '(string . 213))

;; kUND: shifted undo key
(define key_sundo '(string . 214))

;; rfi: send next input char (for ptys)
(define req_for_input '(string . 215))

;; kf11: F11 function key
(define key_f11 '(string . 216))

;; kf12: F12 function key
(define key_f12 '(string . 217))

;; kf13: F13 function key
(define key_f13 '(string . 218))

;; kf14: F14 function key
(define key_f14 '(string . 219))

;; kf15: F15 function key
(define key_f15 '(string . 220))

;; kf16: F16 function key
(define key_f16 '(string . 221))

;; kf17: F17 function key
(define key_f17 '(string . 222))

;; kf18: F18 function key
(define key_f18 '(string . 223))

;; kf19: F19 function key
(define key_f19 '(string . 224))

;; kf20: F20 function key
(define key_f20 '(string . 225))

;; kf21: F21 function key
(define key_f21 '(string . 226))

;; kf22: F22 function key
(define key_f22 '(string . 227))

;; kf23: F23 function key
(define key_f23 '(string . 228))

;; kf24: F24 function key
(define key_f24 '(string . 229))

;; kf25: F25 function key
(define key_f25 '(string . 230))

;; kf26: F26 function key
(define key_f26 '(string . 231))

;; kf27: F27 function key
(define key_f27 '(string . 232))

;; kf28: F28 function key
(define key_f28 '(string . 233))

;; kf29: F29 function key
(define key_f29 '(string . 234))

;; kf30: F30 function key
(define key_f30 '(string . 235))

;; kf31: F31 function key
(define key_f31 '(string . 236))

;; kf32: F32 function key
(define key_f32 '(string . 237))

;; kf33: F33 function key
(define key_f33 '(string . 238))

;; kf34: F34 function key
(define key_f34 '(string . 239))

;; kf35: F35 function key
(define key_f35 '(string . 240))

;; kf36: F36 function key
(define key_f36 '(string . 241))

;; kf37: F37 function key
(define key_f37 '(string . 242))

;; kf38: F38 function key
(define key_f38 '(string . 243))

;; kf39: F39 function key
(define key_f39 '(string . 244))

;; kf40: F40 function key
(define key_f40 '(string . 245))

;; kf41: F41 function key
(define key_f41 '(string . 246))

;; kf42: F42 function key
(define key_f42 '(string . 247))

;; kf43: F43 function key
(define key_f43 '(string . 248))

;; kf44: F44 function key
(define key_f44 '(string . 249))

;; kf45: F45 function key
(define key_f45 '(string . 250))

;; kf46: F46 function key
(define key_f46 '(string . 251))

;; kf47: F47 function key
(define key_f47 '(string . 252))

;; kf48: F48 function key
(define key_f48 '(string . 253))

;; kf49: F49 function key
(define key_f49 '(string . 254))

;; kf50: F50 function key
(define key_f50 '(string . 255))

;; kf51: F51 function key
(define key_f51 '(string . 256))

;; kf52: F52 function key
(define key_f52 '(string . 257))

;; kf53: F53 function key
(define key_f53 '(string . 258))

;; kf54: F54 function key
(define key_f54 '(string . 259))

;; kf55: F55 function key
(define key_f55 '(string . 260))

;; kf56: F56 function key
(define key_f56 '(string . 261))

;; kf57: F57 function key
(define key_f57 '(string . 262))

;; kf58: F58 function key
(define key_f58 '(string . 263))

;; kf59: F59 function key
(define key_f59 '(string . 264))

;; kf60: F60 function key
(define key_f60 '(string . 265))

;; kf61: F61 function key
(define key_f61 '(string . 266))

;; kf62: F62 function key
(define key_f62 '(string . 267))

;; kf63: F63 function key
(define key_f63 '(string . 268))

;; el1: Clear to beginning of line
(define clr_bol '(string . 269))

;; mgc: clear right and left soft margins
(define clear_margins '(string . 270))

;; smgl: set left soft margin at current column. See smgl. (ML is not in BSD termcap).
(define set_left_margin '(string . 271))

;; smgr: set right soft margin at current column
(define set_right_margin '(string . 272))

;; fln: label format
(define label_format '(string . 273))

;; sclk: set clock, #1 hrs #2 mins #3 secs
(define set_clock '(string . 274))

;; dclk: display clock
(define display_clock '(string . 275))

;; rmclk: remove clock
(define remove_clock '(string . 276))

;; cwin: define a window #1 from #2,#3 to #4,#5
(define create_window '(string . 277))

;; wingo: go to window #1
(define goto_window '(string . 278))

;; hup: hang-up phone
(define hangup '(string . 279))

;; dial: dial number #1
(define dial_phone '(string . 280))

;; qdial: dial number #1 without checking
(define quick_dial '(string . 281))

;; tone: select touch tone dialing
(define tone '(string . 282))

;; pulse: select pulse dialing
(define pulse '(string . 283))

;; hook: flash switch hook
(define flash_hook '(string . 284))

;; pause: pause for 2-3 seconds
(define fixed_pause '(string . 285))

;; wait: wait for dial-tone
(define wait_tone '(string . 286))

;; u0: User string #0
(define user0 '(string . 287))

;; u1: User string #1
(define user1 '(string . 288))

;; u2: User string #2
(define user2 '(string . 289))

;; u3: User string #3
(define user3 '(string . 290))

;; u4: User string #4
(define user4 '(string . 291))

;; u5: User string #5
(define user5 '(string . 292))

;; u6: User string #6
(define user6 '(string . 293))

;; u7: User string #7
(define user7 '(string . 294))

;; u8: User string #8
(define user8 '(string . 295))

;; u9: User string #9
(define user9 '(string . 296))

;; op: Set default pair to its original value
(define orig_pair '(string . 297))

;; oc: Set all color pairs to the original ones
(define orig_colors '(string . 298))

;; initc: initialize color #1 to (#2,#3,#4)
(define initialize_color '(string . 299))

;; initp: Initialize color pair #1 to fg=(#2,#3,#4), bg=(#5,#6,#7)
(define initialize_pair '(string . 300))

;; scp: Set current color pair to #1
(define set_color_pair '(string . 301))

;; setf: Set foreground color #1
(define set_foreground '(string . 302))

;; setb: Set background color #1
(define set_background '(string . 303))

;; cpi: Change number of characters per inch to #1
(define change_char_pitch '(string . 304))

;; lpi: Change number of lines per inch to #1
(define change_line_pitch '(string . 305))

;; chr: Change horizontal resolution to #1
(define change_res_horz '(string . 306))

;; cvr: Change vertical resolution to #1
(define change_res_vert '(string . 307))

;; defc: Define a character #1, #2 dots wide, descender #3
(define define_char '(string . 308))

;; swidm: Enter double-wide mode
(define enter_doublewide_mode '(string . 309))

;; sdrfq: Enter draft-quality mode
(define enter_draft_quality '(string . 310))

;; sitm: Enter italic mode
(define enter_italics_mode '(string . 311))

;; slm: Start leftward carriage motion
(define enter_leftward_mode '(string . 312))

;; smicm: Start micro-motion mode
(define enter_micro_mode '(string . 313))

;; snlq: Enter NLQ mode
(define enter_near_letter_quality '(string . 314))

;; snrmq: Enter normal-quality mode
(define enter_normal_quality '(string . 315))

;; sshm: Enter shadow-print mode
(define enter_shadow_mode '(string . 316))

;; ssubm: Enter subscript mode
(define enter_subscript_mode '(string . 317))

;; ssupm: Enter superscript mode
(define enter_superscript_mode '(string . 318))

;; sum: Start upward carriage motion
(define enter_upward_mode '(string . 319))

;; rwidm: End double-wide mode
(define exit_doublewide_mode '(string . 320))

;; ritm: End italic mode
(define exit_italics_mode '(string . 321))

;; rlm: End left-motion mode
(define exit_leftward_mode '(string . 322))

;; rmicm: End micro-motion mode
(define exit_micro_mode '(string . 323))

;; rshm: End shadow-print mode
(define exit_shadow_mode '(string . 324))

;; rsubm: End subscript mode
(define exit_subscript_mode '(string . 325))

;; rsupm: End superscript mode
(define exit_superscript_mode '(string . 326))

;; rum: End reverse character motion
(define exit_upward_mode '(string . 327))

;; mhpa: Like column_address in micro mode
(define micro_column_address '(string . 328))

;; mcud1: Like cursor_down in micro mode
(define micro_down '(string . 329))

;; mcub1: Like cursor_left in micro mode
(define micro_left '(string . 330))

;; mcuf1: Like cursor_right in micro mode
(define micro_right '(string . 331))

;; mvpa: Like row_address #1 in micro mode
(define micro_row_address '(string . 332))

;; mcuu1: Like cursor_up in micro mode
(define micro_up '(string . 333))

;; porder: Match software bits to print-head pins
(define order_of_pins '(string . 334))

;; mcud: Like parm_down_cursor in micro mode
(define parm_down_micro '(string . 335))

;; mcub: Like parm_left_cursor in micro mode
(define parm_left_micro '(string . 336))

;; mcuf: Like parm_right_cursor in micro mode
(define parm_right_micro '(string . 337))

;; mcuu: Like parm_up_cursor in micro mode
(define parm_up_micro '(string . 338))

;; scs: Select character set, #1
(define select_char_set '(string . 339))

;; smgb: Set bottom margin at current line
(define set_bottom_margin '(string . 340))

;; smgbp: Set bottom margin at line #1 or (if smgtp is not given) #2 lines from bottom
(define set_bottom_margin_parm '(string . 341))

;; smglp: Set left (right) margin at column #1
(define set_left_margin_parm '(string . 342))

;; smgrp: Set right margin at column #1
(define set_right_margin_parm '(string . 343))

;; smgt: Set top margin at current line
(define set_top_margin '(string . 344))

;; smgtp: Set top (bottom) margin at row #1
(define set_top_margin_parm '(string . 345))

;; sbim: Start printing bit image graphics
(define start_bit_image '(string . 346))

;; scsd: Start character set definition #1, with #2 characters in the set
(define start_char_set_def '(string . 347))

;; rbim: Stop printing bit image graphics
(define stop_bit_image '(string . 348))

;; rcsd: End definition of character set #1
(define stop_char_set_def '(string . 349))

;; subcs: List of subscriptable characters
(define subscript_characters '(string . 350))

;; supcs: List of superscriptable characters
(define superscript_characters '(string . 351))

;; docr: Printing any of these characters causes CR
(define these_cause_cr '(string . 352))

;; zerom: No motion for subsequent character
(define zero_motion '(string . 353))

;; csnm: Produce #1'th item from list of character set names
(define char_set_names '(string . 354))

;; kmous: Mouse event has occurred
(define key_mouse '(string . 355))

;; minfo: Mouse status information
(define mouse_info '(string . 356))

;; reqmp: Request mouse position
(define req_mouse_pos '(string . 357))

;; getm: Curses should get button events, parameter #1 not documented.
(define get_mouse '(string . 358))

;; setaf: Set foreground color to #1, using ANSI escape
(define set_a_foreground '(string . 359))

;; setab: Set background color to #1, using ANSI escape
(define set_a_background '(string . 360))

;; pfxl: Program function key #1 to type string #2 and show string #3
(define pkey_plab '(string . 361))

;; devt: Indicate language/codeset support
(define device_type '(string . 362))

;; csin: Init sequence for multiple codesets
(define code_set_init '(string . 363))

;; s0ds: Shift to codeset 0 (EUC set 0, ASCII)
(define set0_des_seq '(string . 364))

;; s1ds: Shift to codeset 1
(define set1_des_seq '(string . 365))

;; s2ds: Shift to codeset 2
(define set2_des_seq '(string . 366))

;; s3ds: Shift to codeset 3
(define set3_des_seq '(string . 367))

;; smglr: Set both left and right margins to #1, #2.  (ML is not in BSD termcap).
(define set_lr_margin '(string . 368))

;; smgtb: Sets both top and bottom margins to #1, #2
(define set_tb_margin '(string . 369))

;; birep: Repeat bit image cell #1 #2 times
(define bit_image_repeat '(string . 370))

;; binel: Move to next row of the bit image
(define bit_image_newline '(string . 371))

;; bicr: Move to beginning of same row
(define bit_image_carriage_return '(string . 372))

;; colornm: Give name for color #1
(define color_names '(string . 373))

;; defbi: Define rectangular bit image region
(define define_bit_image_region '(string . 374))

;; endbi: End a bit-image region
(define end_bit_image_region '(string . 375))

;; setcolor: Change to ribbon color #1
(define set_color_band '(string . 376))

;; slines: Set page length to #1 lines
(define set_page_length '(string . 377))

;; dispc: Display PC character #1
(define display_pc_char '(string . 378))

;; smpch: Enter PC character display mode
(define enter_pc_charset_mode '(string . 379))

;; rmpch: Exit PC character display mode
(define exit_pc_charset_mode '(string . 380))

;; smsc: Enter PC scancode mode
(define enter_scancode_mode '(string . 381))

;; rmsc: Exit PC scancode mode
(define exit_scancode_mode '(string . 382))

;; pctrm: PC terminal options
(define pc_term_options '(string . 383))

;; scesc: Escape for scancode emulation
(define scancode_escape '(string . 384))

;; scesa: Alternate escape for scancode emulation
(define alt_scancode_esc '(string . 385))

;; ehhlm: Enter horizontal highlight mode
(define enter_horizontal_hl_mode '(string . 386))

;; elhlm: Enter left highlight mode
(define enter_left_hl_mode '(string . 387))

;; elohlm: Enter low highlight mode
(define enter_low_hl_mode '(string . 388))

;; erhlm: Enter right highlight mode
(define enter_right_hl_mode '(string . 389))

;; ethlm: Enter top highlight mode
(define enter_top_hl_mode '(string . 390))

;; evhlm: Enter vertical highlight mode
(define enter_vertical_hl_mode '(string . 391))

;; sgr1: Define second set of video attributes #1-#6
(define set_a_attributes '(string . 392))

;; slength: Set page length to #1 hundredth of an inch (some implementations use sL for termcap).
(define set_pglen_inch '(string . 393))

;; OTi2: secondary initialization string
(define termcap_init2 '(string . 394))

;; OTrs: terminal reset string
(define termcap_reset '(string . 395))

;; OTnl: use to move down
(define linefeed_if_not_lf '(string . 396))

;; OTbc: move left, if not ^H
(define backspace_if_not_bs '(string . 397))

;; OTko: list of self-mapped keycaps
(define other_non_function_keys '(string . 398))

;; OTma: map arrow keys rogue(1) motion keys
(define arrow_key_map '(string . 399))

;; OTG2: single upper left
(define acs_ulcorner '(string . 400))

;; OTG3: single lower left
(define acs_llcorner '(string . 401))

;; OTG1: single upper right
(define acs_urcorner '(string . 402))

;; OTG4: single lower right
(define acs_lrcorner '(string . 403))

;; OTGR: tee pointing right
(define acs_ltee '(string . 404))

;; OTGL: tee pointing left
(define acs_rtee '(string . 405))

;; OTGU: tee pointing up
(define acs_btee '(string . 406))

;; OTGD: tee pointing down
(define acs_ttee '(string . 407))

;; OTGH: single horizontal line
(define acs_hline '(string . 408))

;; OTGV: single vertical line
(define acs_vline '(string . 409))

;; OTGC: single intersection
(define acs_plus '(string . 410))

;; meml: lock memory above cursor
(define memory_lock '(string . 411))

;; memu: unlock memory
(define memory_unlock '(string . 412))

;; box1: box characters primary set
(define box_chars_1 '(string . 413)))
