#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Generate (text-mode terminfo constants) from extern/Caps from
;;; the ncurses distribution

(import
  (rnrs (6))
  (only (srfi :1 lists) split-at)
  (only (srfi :13 strings) string-contains string-tokenize
        string-prefix? string-join string-trim-right string-delete)
  (srfi :14 char-sets)
  (srfi :48 intermediate-format-strings))

(define-record-type capline
  (fields varname capname type tcname
          keyname keyvalue termcap description))

(define (capline-scheme-name cap)
  (let ((sym (string->symbol (capline-varname cap))))
    (if (eq? sym 'newline)
        'linefeed
        sym)))

(define (capline-scheme-type cap)
  (case (string->symbol (capline-type cap))
    ((bool) 'boolean)
    ((num) 'number)
    ((str) 'string)
    (else
     (error 'cpaline-scheme-type "Unknown type" (capline-type cap)))))

;; Parse each line and give numbers to each capability. The numbers
;; are used in compiled terminfo files.
(define (parse-caps filename)
  (call-with-input-file filename
    (lambda (p)
      (let lp ((bool* '()) (num* '()) (str* '())
               (bool 0) (num 0) (str 0))
        (let lp* ()
          (let ((line (get-line p)))
            (cond
              ((eof-object? line)
               (append (reverse bool*) (reverse num*) (reverse str*)))
              ((or (string-prefix? "#" line)
                   (string-prefix? "capalias" line)
                   (string-prefix? "infoalias" line))
               (lp*))
              (else
               (let ((columns (string-tokenize line (char-set-complement (char-set #\tab)))))
                 (cond
                   ((>= (length columns) 8)
                    (let*-values ([(columns description) (split-at columns 7)]
                                  [(columns) (append columns (list (string-join description "")))])
                      (let ((cap (apply make-capline columns)))
                        (case (string->symbol (capline-type cap))
                          ((bool)
                           (lp (cons (cons bool cap) bool*) num* str*
                               (+ bool 1) num str))
                          ((num)
                           (lp bool* (cons (cons num cap) num*) str*
                               bool (+ num 1) str))
                          ((str)
                           (lp bool* num* (cons (cons str cap) str*)
                               bool num (+ str 1)))
                          (else
                           (error 'parse-caps "Bad type" line (capline-type cap)))))))
                   (else
                    (error 'parse-caps "Bad line" line (length columns)))))))))))))

(define (get-copyright filename)
  (call-with-input-file filename
    (lambda (p)
      (let lp ((lines '()))
    (let ((line (get-line p)))
      (if (string=? line "#")
          (reverse lines)
          (lp (cons line lines))))))))

(define (make-table caps type)
  (let* ((caps (filter (lambda (idx/cap)
             (equal? type (capline-type (cdr idx/cap))))
               caps))
     (table (make-vector (+ 1 (fold-left max 0 (map car caps))) #f)))
    (for-each (lambda (idx/cap)
        (let ((idx (car idx/cap)) (cap (cdr idx/cap)))
          (vector-set! table idx (string->symbol (capline-capname cap)))))
          caps)
    table))

(define (make-key-table caps len)
  (let* ((caps (filter (lambda (idx/cap)
             (let ((cap (cdr idx/cap)))
               (and (equal? "str" (capline-type cap))
                (not (equal? "-" (capline-keyname cap))))))
               caps))
     (table (make-vector len #f)))
    (for-each (lambda (idx/cap)
        (let ((idx (car idx/cap)) (cap (cdr idx/cap)))
          (vector-set! table idx
                   (string->symbol
                (string-delete (char-set #\( #\))
                           (capline-keyname cap))))))
          caps)
    table))

(define caps (parse-caps "extern/Caps"))
(define terminfo-booleans (make-table caps "bool"))
(define terminfo-numbers (make-table caps "num"))
(define terminfo-strings (make-table caps "str"))
(define terminfo-keys (make-key-table caps (vector-length terminfo-strings)))

(define (pretty-print/nonl x)
  (display (string-trim-right (format #f "~y" x))))

(define (main)
  (display ";; -*- mode: scheme; coding: utf-8 -*-\n")
  (for-each (lambda (line)
          (display ";; ")
          (display line)
          (display "\n"))
        (get-copyright "extern/Caps"))
  (display "#!r6rs\n")
  (display "\n")
  (display "#| Terminfo API constants extracted from
extern/Caps (copied from the ncurses distribution) by
tools/parse-caps.sps |#\n")
  (display "\n")
  (display "(library (text-mode terminfo constants)\n")
  (display "  (export")
  (display "\n    terminfo-strings")
  (display "\n    terminfo-booleans")
  (display "\n    terminfo-numbers")
  (display "\n    terminfo-keys")
  (for-each (lambda (idx/cap)
              (let ((idx (car idx/cap)) (cap (cdr idx/cap)))
                (display "\n    ")
                (write (capline-scheme-name cap))))
            caps)
  (display ")\n  ")
  (pretty-print/nonl '(import (only (rnrs) define quote)))
  (display "\n\n")
  (pretty-print/nonl `(define terminfo-booleans
                        ',terminfo-booleans))
  (display "\n\n")
  (pretty-print/nonl `(define terminfo-numbers
                        ',terminfo-numbers))
  (display "\n\n")
  (pretty-print/nonl `(define terminfo-strings
                        ',terminfo-strings))
  (display "\n\n")
  (pretty-print/nonl `(define terminfo-keys
                        ',terminfo-keys))
  (for-each (lambda (idx/cap)
              (let ((idx (car idx/cap)) (cap (cdr idx/cap)))
                (display "\n\n;; ")
                (display (capline-capname cap))
                (display ": ")
                (display (capline-description cap))
                (display "\n")
                (pretty-print/nonl `(define ,(capline-scheme-name cap)
                                      '(,(capline-scheme-type cap) . ,idx)))))
            caps)
  (display ")\n"))

(main)
