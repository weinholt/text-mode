;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Utilities for drawing with pixels on the text screen.

(library (text-mode pixels)
  (export
    make-pixmap
    pixmap-width pixmap-height
    pixmap-raw
    pixmap-fill!
    pixmap-set!
    pixmap-ref
    pixmap-draw-half
    pixmap-draw-quad
    pixmap-draw-braille)
  (import
    (rnrs)
    (text-mode console))

(define-record-type pixmap
  (fields width height raw)
  (protocol
   (lambda (p)
     (case-lambda
       ((width height)
        (let ((pm (p width height (make-bytevector (* width height 4) 0))))
          (pixmap-fill! pm Default)
          pm))
       ((width height raw)
        (assert (fx>=? (* width height 4) (bytevector-length raw)))
        (p width height raw))))))

(define (pixmap-fill! pm c)
  (let ((raw (pixmap-raw pm)))
    (do ((i 0 (fx+ i 4)))
        ((fx>=? i (bytevector-length raw)))
      (bytevector-u32-native-set! raw i c))))

(define (pixmap-set! pm x y c)
  (bytevector-u32-native-set! (pixmap-raw pm)
                              (fx* 4 (fx+ x (fx* (pixmap-width pm) y)))
                              c))

(define (pixmap-ref pm x y)
  (let ((idx (fx* 4 (fx+ x (fx* (pixmap-width pm) y)))))
    (if (fx<? idx (bytevector-length (pixmap-raw pm)))
        (bytevector-u32-native-ref (pixmap-raw pm) idx)
        0)))

(define color<? <)            ;TODO: better to use intensity, probably

(define (color>? x y)
  (color<? y x))

(define color-min min)

(define (%print-half-pixel c0 c1 c)
  (cond
    ((= c0 c1)
     (cond ((= c0 Default)
            (text-color c0 c)
            (text-background c1 c)
            (print " ")
            ;; Maybe use #\x2800;
            #; (gotoxy (+ (wherex) 1) (wherey)))
           (else
            (text-color c1 c)
            (text-background c0 c)
            (print "█" c))))
    ((or (and (> c0 c1) (not (= c0 Default))) (= c1 Default))
     (text-color c0 c)
     (text-background c1 c)
     (print "▀" c))
    ((or (< c0 c1) (= c0 Default))
     (text-color c1 c)
     (text-background c0 c)
     (print "▄" c))
    (else
     (text-color c1 c)
     (text-background c0 c)
     (print " " c))))

;; Draw the pixmap on the screen. The top left of the pixmap will be
;; at the current cursor position. The pixmap is clipped to the window
;; boundary. You can optionally specify the part of the pixmap that
;; will be drawn.
(define pixmap-draw-half
  (case-lambda
    ((pm)
     (pixmap-draw-half pm (current-console)))
    ((pm c)
     (pixmap-draw-half pm 0 0 (pixmap-width pm) (pixmap-height pm) c))
    ((pm x0 y0 x1 y1 c)
     (let ((cx0 (wherex c))
           (cy0 (wherey c)))
       (do ((x1 (fxmin x1 (fx- (window-width c) cx0)))
            (y1 (fxmin y1 (fx* (fx- (window-height c) cy0) 2)))
            (cy cy0 (fx+ cy 1))
            (y y0 (fx+ y 2)))
           ((fx>=? y y1))
         (gotoxy cx0 cy)
         (do ((x x0 (fx+ x 1)))
             ((fx=? x x1))
           (let ((c0 (pixmap-ref pm x y))
                 (c1 (pixmap-ref pm x (fx+ y 1))))
             (%print-half-pixel c0 c1 c))))))))

;; Draw the pixmap on the screen by using quadrants. Note that the
;; pixels will not be square in most fonts. There are some limitations
;; on the choice of colors because each character cell still only has
;; foreground and background. If any of the pixels has the default
;; color then the default color will be the background. The foreground
;; color will be the first pixel that doesn't match the background out
;; of upper-left, bottom-left, upper-right and bottom-right. The
;; algorithm driving this procedure is open to refinements.
(define pixmap-draw-quad
  (case-lambda
    ((pm)
     (pixmap-draw-quad pm (current-console)))
    ((pm c)
     (pixmap-draw-quad pm 0 0 (pixmap-width pm) (pixmap-height pm) c))
    ((pm x0 y0 x1 y1 c)
     ;; 1 4
     ;; 2 8
     (define alphabet '#(" " "▘" "▖" "▌"   ;00 01 02 03
                         "▝" "▀" "▞" "▛"   ;04 05 06 07
                         "▗" "▚" "▄" "▙"   ;10 11 12 13
                         "▐" "▜" "▟" "█")) ;14 15 16 17
     (let ((cx0 (wherex c))
           (cy0 (wherey c)))
       ;; TODO: Verify that the limits are sane
       (do ((x1 (fxmin x1 (fx* (fx- (window-width c) cx0) 2)))
            (y1 (fxmin y1 (fx* (fx- (window-height c) cy0) 2)))
            (cy cy0 (fx+ cy 1))
            (y y0 (fx+ y 2)))
           ((fx>=? y y1))
         (gotoxy cx0 cy)
         (do ((x x0 (fx+ x 2)))
             ((fx>=? x x1))
           (let ((cUL (pixmap-ref pm x y))
                 (cUR (pixmap-ref pm (fx+ x 1) y))
                 (cBL (pixmap-ref pm x (fx+ y 1)))
                 (cBR (pixmap-ref pm (fx+ x 1) (fx+ y 1))))
             (cond ((and (= cUL cUR) (= cBL cBR))
                    (%print-half-pixel cUL cBL c))
                   (else
                    ;; This is not very clever, but it tries to select
                    ;; the darkest color (or default) as the
                    ;; background. This will make more sense when the
                    ;; pixmap is viewed on a B&W terminal.
                    (let* ((cbg (if (or (eqv? cUL Default)
                                        (eqv? cBL Default)
                                        (eqv? cUR Default)
                                        (eqv? cBR Default))
                                    Default
                                    (color-min cUL cBL cUR cBR)))
                           (cfg (cond ((not (eqv? cUL cbg)) cUL)
                                      ((not (eqv? cBL cbg)) cBL)
                                      ((not (eqv? cUR cbg)) cUR)
                                      (else                 cBR))))
                      (text-color cfg c)
                      (text-background cbg c)
                      (let ((i (fxior (if (eqv? cbg cUL) 0 1)
                                      (if (eqv? cbg cBL) 0 2)
                                      (if (eqv? cbg cUR) 0 4)
                                      (if (eqv? cbg cBR) 0 8))))
                        (print (vector-ref alphabet i)))))))))))))

;; Draw pixels using Braille symbols. The control over color is even
;; less exact than with quadrants since eight pixels will share the
;; same foreground and background color. The dots are not contiguous
;; and some fonts may not have an even space distribution between the
;; dots. But at least the pixels are square. This procedure uses the
;; current background color.
(define pixmap-draw-braille
  (case-lambda
    ((pm)
     (pixmap-draw-braille pm (current-console)))
    ((pm c)
     (pixmap-draw-braille pm 0 0 (pixmap-width pm) (pixmap-height pm) c))
    ((pm x0 y0 x1 y1 c)
     ;; Layout in hex of the dots in BRAILLE PATTERN:
     ;; 1  8
     ;; 2  10
     ;; 4  20
     ;; 40 80
     (let ((cx0 (wherex c))
           (cy0 (wherey c)))
       (do ((x1 (fxmin x1 (fx* (fx- (window-width c) cx0) 2)))
            (y1 (fxmin y1 (fx* (fx- (window-height c) cy0) 4)))
            (cy cy0 (fx+ cy 1))
            (y y0 (fx+ y 4)))
           ((fx>=? y y1))
         (gotoxy cx0 cy)
         (do ((x x0 (fx+ x 2)))
             ((fx>=? x x1))
           (let ((c1UL (pixmap-ref pm x         y))
                 (c1UR (pixmap-ref pm (fx+ x 1) y))
                 (c1BL (pixmap-ref pm x         (fx+ y 1)))
                 (c1BR (pixmap-ref pm (fx+ x 1) (fx+ y 1)))
                 (c2UL (pixmap-ref pm x         (fx+ y 2)))
                 (c2UR (pixmap-ref pm (fx+ x 1) (fx+ y 2)))
                 (c2BL (pixmap-ref pm x         (fx+ y 3)))
                 (c2BR (pixmap-ref pm (fx+ x 1) (fx+ y 3))))
             (let* ((cbg (if (or (eqv? c1UL Default)
                                 (eqv? c1BL Default)
                                 (eqv? c1UR Default)
                                 (eqv? c1BR Default)
                                 (eqv? c2UL Default)
                                 (eqv? c2BL Default)
                                 (eqv? c2UR Default)
                                 (eqv? c2BR Default))
                             Default
                             (color-min c1UL c1BL c1UR c1BR
                                        c2UL c2BL c2UR c2BR)))
                    (cfg (cond ((not (eqv? c1UL cbg)) c1UL)
                               ((not (eqv? c1BL cbg)) c1BL)
                               ((not (eqv? c1UR cbg)) c1UR)
                               ((not (eqv? c1BR cbg)) c1BR)
                               ((not (eqv? c2UL cbg)) c2UL)
                               ((not (eqv? c2BL cbg)) c2BL)
                               ((not (eqv? c2UR cbg)) c2UR)
                               (else                  c2BR))))
               (text-color cfg c)
               (let ((i (fxior (if (eqv? cbg c1UL) 0 #x01)
                               (if (eqv? cbg c1BL) 0 #x02)
                               (if (eqv? cbg c1UR) 0 #x08)
                               (if (eqv? cbg c1BR) 0 #x10)
                               (if (eqv? cbg c2UL) 0 #x04)
                               (if (eqv? cbg c2BL) 0 #x40)
                               (if (eqv? cbg c2UR) 0 #x20)
                               (if (eqv? cbg c2BR) 0 #x80))))
                 (print (string (integer->char (+ #x2800 i))))))))))))))
