#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018-2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Prints debug info for a compiled terminfo file.

(import
  (rnrs (6))
  (only (srfi :13 strings) string-prefix?)
  (srfi :48 intermediate-format-strings)
  (only (text-mode terminfo constants)
        terminfo-booleans terminfo-numbers
        terminfo-strings terminfo-keys)
  (text-mode terminfo)
  (text-mode terminfo parser)
  (only (chezscheme) expand/optimize print-gensym))

(define (main db)
  (define (print-string name str key)
    (write (utf8->string str))
    (newline)
    (cond [key
           (format #t "    Key: ~s~%" key)]
          [(member name '(u6 u8 acsc))
           ;; user8 is the response to user9 and user6 is the
           ;; response to user7. acs_chars is the bytes for
           ;; using the alternate character set.
           #f]
          [else
           (format #t "~y~%" (expand/optimize (terminfo-expression str)))]))
  (format #t "Terminal names:~%  ~w~%" (terminfo-db-terminal-names db))
  (display "\nBooleans:\n")
  (do ((i 0 (+ i 1)))
      ((= i (vector-length terminfo-booleans)))
    (format #t "  ~s: ~w~%" (vector-ref terminfo-booleans i)
            (vector-ref (terminfo-db-booleans db) i)))
  (display "\nNumbers:\n")
  (do ((i 0 (+ i 1)))
      ((= i (vector-length terminfo-numbers)))
    (format #t "  ~s: ~w~%" (vector-ref terminfo-numbers i)
            (vector-ref (terminfo-db-numbers db) i)))
  (display "\nStrings:\n")
  (do ((i 0 (+ i 1)))
      ((= i (vector-length terminfo-strings)))
    (let ((name (vector-ref terminfo-strings i)))
      (format #t "  ~s: " name)
      (cond
        [(vector-ref (terminfo-db-strings db) i) =>
         (lambda (str)
           (print-string name str (vector-ref terminfo-keys i)))]
        [else (newline)])))
  (let ((show-ht (lambda (ht wrt)
                   (vector-for-each
                    (lambda (name)
                      (format #t "  ~s: " name)
                      (wrt name (hashtable-ref ht name #f)))
                    (hashtable-keys ht)))))
    (display "\nExtended booleans:\n")
    (show-ht (terminfo-db-ext-booleans db) (lambda (name str) (write str) (newline)))
    (display "\nExtended numbers:\n")
    (show-ht (terminfo-db-ext-numbers db) (lambda (name str) (write str) (newline)))
    (display "\nExtended strings:\n")
    (show-ht (terminfo-db-ext-strings db)
             (lambda (name str)
               (if str
                   (let* ((name (symbol->string name))
                          (key (and (string-prefix? "k" name)
                                    (string->symbol
                                     (string-append "KEY_" (substring name 1 (string-length name)))))))
                     (print-string name str key))
                   (newline))))))

(print-gensym #f)

(main (if (= (length (command-line)) 2)
          (call-with-port (open-file-input-port (cadr (command-line)))
            get-compiled-ncurses-terminfo)
          (get-terminfo-db)))
